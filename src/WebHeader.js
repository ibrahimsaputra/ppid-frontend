import React, { useEffect } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretRight,faWrench } from '@fortawesome/free-solid-svg-icons';
import Clock from './component/Clock';
import logo from './img/logo-ppid-kkp.png';
import CarouselText from './component/CarouselText';
import MegaMenu from './component/MegaMenu';
import { useSelector, useDispatch } from 'react-redux';
import { menuActions } from './_actions';

const getSelectMenu = (menu,idMenuUtama) => {
    let selectMenu = [];
    
    const menus = menu.find(
      (element) => element.id === parseInt(idMenuUtama)
    );
    if (menus) {
      menus.id_parent = null;
      selectMenu = [...selectMenu, menus];
  
      const subMenus = (subMen, id_parent) => {
        subMen.forEach((element) => {
          element.id_parent = id_parent;
          selectMenu = [...selectMenu, element];
          if (element.subMenus) {
            subMenus(element.subMenus, element.id);
          }
        });
      };
  
      subMenus(menus.subMenus, menus.id);
    }
    return selectMenu;
  };
  
function WebHeader(){
    const dispatch = useDispatch();
    const menus = useSelector((state)=>state.menu.menu);
    let sMenus=[]; 
    menus.forEach(e => [...sMenus,getSelectMenu(menus,e.id)]) 
    useEffect(() => {
        dispatch(menuActions.getMenu())
    }, sMenus) 
    
    return (<div>
            <div className="infobar">
                <div className="info">
                <div className="infologo"> Info <i><FontAwesomeIcon icon={faCaretRight}/> </i></div> 
                <div className="infocontent"><CarouselText text={["Selamat Datang Di Website Pejabat Pengelola Informasi dan Dokumentasi KKP Padang","Silahkan Gunakan Website Untuk Mengakses Informasi Yang Anda Inginkan"]}> </CarouselText></div> 
                </div>
                <div className="d-flex align-items-center">
                    <Clock/>
                    <a href="/login" className="btn btn-sm" style={{color:"#fff"}} ><i><FontAwesomeIcon icon={faWrench}/></i></a>
                </div>
            </div>
            <nav className="menubar topbar">
                <a href="/" className="text-center logobar my-lg-4"><img className="logobar" src={logo} alt=""/></a> 
                <div className="vertical-line"></div>
                <ul className="menulist">
                    {menus.map((menu,i)=> <MegaMenu key={i} menu={menu}/>)} 
                </ul>
            </nav>
        </div>
    );
}
export default WebHeader;