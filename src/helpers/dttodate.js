export function dttodate(datetime){
      const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
      const date = new Date(datetime);
      return String(date.getDate()).padStart(2,'0')+"-"+monthNames[date.getMonth()]+"-"+date.getFullYear();
}