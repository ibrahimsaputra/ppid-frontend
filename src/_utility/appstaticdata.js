export const tujuanPermohonan = (i) => {
  const data = ["PPID KKP Padang"];
  if (i) return data[i];
  return data;
};

export const tipePemohon = (i) => {
  const data = ["WNI", "Badan Hukum"];
  if (i || i === 0) return data[i];
  return data;
};
export const detailWNI = (i) => {
  const data = ["Perorangan", "Kelompok Orang"];
  if (i || i === 0) return data[i];
  return data;
};

export const kategoriInformasi = (i) => {
  const data = [
    "Regulasi",
    "RKA",
    "Lakip",
    "Laporan Keuangan",
    "Perjanjian",
    "ProfilBadanPublik",
    "Kegiatan",
    "LHKPN",
    "RENSTRA",
    "Lainnya",
  ];
  if (i || i === 0) return data[i];
  return data;
};

export const caraPeroleh = (i) => {
  const data = ["Email", "Kirim Sesuai Alamat", "Kirim Langusung"];
  if (i || i === 0) return data[i];
  return data;
};

export const bentukInformasi = (i) => {
  const data = ["Tidak Tersedia", "Tersedia"];
  if (i || i === 0) return data[i];
  return data;
};

export const jenisInformasi = (i) => {
  const data = ["Tidak", "Ya"];
  if (i || i === 0) return data[i];
  return data;
};

export const alasanKeberatan = (i) => {
  const data = [
    "Penolakan atas Permintaan Informasi Berdasarkan Alasan Pengecualian Sebagaimana dimaksud dalam Pasal 17",
    "Tidak disediakannya Informasi Berkala Sebagaimana dimaksud dalam Pasal 9",
    "Permintaan Informasi ditanggapi Tidak Sebagaimana Mestinya",
    "Pengenaan Biaya yang Tidak Wajar",
    "Penyampaian Informasi yang Melebihi Waktu yang diatur dalam Undang-Undang ini",
    "Lainnya",
  ];

  if (i || i === 0) return data[i];

  return data;
};

export const bentukFormat = (i) => {
  const data = ["Softcopy", "Hardcopy"];
  if (i || i === 0) return data[i];
  return data;
};

export const tipeMenu = (i) => {
  const data = ["post", "list pdf"];
  if (i || i === 0) return data[i];
  return data;
};

export const statusSuratMasuk = (i) => {
  const data = ["belum dibaca", "dikirim", "diabaikan"];
  if (i || i === 0) return data[i];
  return data;
};

export const menuWajib = (i) => {
  const data = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
  ];
  if (i || i === 0) return data[i];
  return data;
};
