import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import WebHeader from "./WebHeader";
import WebFooter from "./WebFooter";
import {
  Dashboard,
  Beranda,
  Login,
  NotFoundPage,
  RegulasiPostAddPage,
  PermohonanListPage,
  PermohonanAddPage,
  KeberatanPage,
  DIPPage,
  KeberatanListPage,
  KeberatanAddPage,
  DipListPage,
  InfopsmPostListPage,
  DipAddPage,
  ProfilAddPage,
  RegulasiPostListPage,
  InfopbPostListPage,
  InfopbPostAddPage,
  InfopsmPostAddPage,
  InfoptssPostListPage,
  InfoptssPostAddPage,
  StlayananPostListPage,
  StlayananPostAddPage,
  LaplayananListPage,
  LaplayananAddPage,
  InfopbPostPage,
  InfoptssPostPage,
  InfopsmPostPage,
  RegulasiPostSubmenuAddPage,
  InfopbPostSubmenuAddPage,
  InfopsmPostSubmenuAddPage,
  InfoptssPostSubmenuAddPage,
  StlayananPostSubmenuAddPage,
} from "./pages";
import { PrivateRoute, RoutePage, AdminHeader, AdminFooter } from "./component";
// import {history} from './helpers';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { RegulasiPostSubmenuListPage } from "./pages/regulasiPost.submenu.list.page";
import { InfopbPostSubmenuListPage } from "./pages/infopbPost.submenu.list.page";
import { InfopsmPostSubmenuListPage } from "./pages/infopsmPost.submenu.list.page";
import { InfoptssPostSubmenuListPage } from "./pages/infoptssPost.submenu.list.page";
import { StlayananPostSubmenuListPage } from "./pages/stlayananPost.submenu.list.page";
import { profilActions, menuActions } from "./_actions";

const getSelectMenu = (menu,idMenuUtama) => {
  let selectMenu = [];
  
  const menus = menu.find(
    (element) => element.id === parseInt(idMenuUtama)
  );
  if (menus) {
    menus.id_parent = null;
    selectMenu = [...selectMenu, menus];

    const subMenus = (subMen, id_parent) => {
      subMen.forEach((element) => {
        element.id_parent = id_parent;
        selectMenu = [...selectMenu, element];
        if (element.subMenus) {
          subMenus(element.subMenus, element.id);
        }
      });
    };

    subMenus(menus.subMenus, menus.id);
  }
  return selectMenu;
};

function App() {
  const dispatch = useDispatch();
  let menus = useSelector((state) => state.menu.menu);
  let sMenus=[]; 
  menus.forEach(e => [...sMenus,getSelectMenu(menus,e.id)]) 
  useEffect(() => {
    dispatch(menuActions.getMenu())
  }, sMenus) 
  const { isAdmin } = useSelector((state) => state.authentication);
  // ;
  return (
    <Router>
      
      <div className="App">
        {isAdmin ? <AdminHeader navId="admin-side-nav" /> : <WebHeader />}
        <Switch>
          {/* Website */}
          <Route exact path="/" component={Beranda} />
          <Route path="/login" component={Login} />
          <Route exact path="/permohonan" component={PermohonanAddPage} />
          <Route exact path="/keberatan" component={KeberatanAddPage} />
          <Route exact path="/e-dip" component={DIPPage} />

          <Route
            path={"/3/4"}
            render={(routerProps) => (
              <InfopbPostPage
                {...routerProps}
                id_menu={4}
                link={[{ nama: "Informasi Publik Berkala", link: "/3/4" }]}
              />
            )}
          />
          <Route
            path={"/3/5"}
            render={(routerProps) => (
              <InfoptssPostPage
                {...routerProps}
                id_menu={5}
                link={[{ nama: "Informasi Publik Berkala", link: "/3/5" }]}
              />
            )}
          />
          <Route
            path={"/3/6"}
            render={(routerProps) => (
              <InfopsmPostPage
                {...routerProps}
                id_menu={6}
                link={[{ nama: "Informasi Publik Serta Merta", link: "/3/6" }]}
              />
            )}
          />

          {menus.map((menu, i) => {
            // console.log(menu)
            return (
              <RoutePage key={i} path={"/" + menu.id} component={Beranda} />
            );
            // return <Route key={menu.id} path={"/"+menu.id} render={(props)=>{
            //   return <RegulasiPage {...props}/>
            // }}/>
          })}

          {/* Admin */}
          <PrivateRoute exact path="/admin/dashboard" component={Dashboard} />
          <PrivateRoute
            exact
            path="/admin/permohonan"
            component={PermohonanListPage}
          />
          <PrivateRoute
            exact
            path="/admin/permohonan/ubah/:id"
            component={PermohonanAddPage}
          />
          <PrivateRoute exact path="/admin/keberatan" component={KeberatanListPage} />
          <PrivateRoute
            exact
            path="/admin/keberatan/ubah/:id"
            component={KeberatanAddPage}
          />
          <PrivateRoute exact path="/admin/dip" component={DipListPage} />
          <PrivateRoute exact path="/admin/dip/tambah" component={DipAddPage} />
          <PrivateRoute exact path="/admin/dip/ubah/:id" component={DipAddPage} />
          <PrivateRoute exact path="/admin/profil" component={ProfilAddPage} />
          <PrivateRoute
            exact
            path="/admin/regulasi"
            component={RegulasiPostListPage}
          />
          <PrivateRoute
            exact
            path="/admin/regulasi/sub"
            component={RegulasiPostSubmenuListPage}
          />
          <PrivateRoute
            exact
            path="/admin/regulasi/sub/tambah"
            component={RegulasiPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/regulasi/sub/ubah/:id"
            component={RegulasiPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/regulasi/tambah"
            component={RegulasiPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/regulasi/ubah/:id"
            component={RegulasiPostAddPage}
          />
          <PrivateRoute exact path="/admin/infopb" component={InfopbPostListPage} />
          <PrivateRoute
            exact
            path="/admin/infopb/sub"
            component={InfopbPostSubmenuListPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopb/sub/tambah"
            component={InfopbPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopb/sub/ubah/:id"
            component={InfopbPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopb/tambah"
            component={InfopbPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopb/ubah/:id"
            component={InfopbPostAddPage}
          />
          <PrivateRoute exact path="/admin/infopsm" component={InfopsmPostListPage} />
          <PrivateRoute
            exact
            path="/admin/infopsm/sub"
            component={InfopsmPostSubmenuListPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopsm/sub/tambah"
            component={InfopsmPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopsm/sub/ubah/:id"
            component={InfopsmPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopsm/tambah"
            component={InfopsmPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infopsm/ubah/:id"
            component={InfopsmPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infoptss"
            component={InfoptssPostListPage}
          />
          <PrivateRoute
            exact
            path="/admin/infoptss/sub"
            component={InfoptssPostSubmenuListPage}
          />
          <PrivateRoute
            exact
            path="/admin/infoptss/sub/tambah"
            component={InfoptssPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infoptss/sub/ubah/:id"
            component={InfoptssPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infoptss/tambah"
            component={InfoptssPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/infoptss/ubah/:id"
            component={InfoptssPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/stlayanan"
            component={StlayananPostListPage}
          />
          <PrivateRoute
            exact
            path="/admin/stlayanan/sub"
            component={StlayananPostSubmenuListPage}
          />
          <PrivateRoute
            exact
            path="/admin/stlayanan/sub/tambah"
            component={StlayananPostSubmenuAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/stlayanan/sub/ubah/:id"
            component={StlayananPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/stlayanan/tambah"
            component={StlayananPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/stlayanan/ubah/:id"
            component={StlayananPostAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/laplayanan"
            component={LaplayananListPage}
          />
          <PrivateRoute
            exact
            path="/admin/laplayanan/ubah/:id"
            component={LaplayananAddPage}
          />
          <PrivateRoute
            exact
            path="/admin/laplayanan/tambah"
            component={LaplayananAddPage}
          />

          <Route path="/404" component={NotFoundPage} />
          <Redirect to="/404" />
        </Switch>
        {isAdmin ? <AdminFooter /> : <WebFooter />}
      </div>
    </Router>
  );
}

export { App };
