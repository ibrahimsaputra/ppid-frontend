import React from 'react';
import logo from './img/logo-ppid-kkp-white.png';
import tweeter from './img/tweeter-white.png';
import facebook from './img/facebook-white.png';
import instagram from './img/instagram-white.png';

function WebFooter(){
    return (
        <div className="footer">
            <div className="row footerbar">
                <img className="col-lg-3 logobar" src={logo} alt=""/>
                <div className="vertical-line"></div>
                <div className="col-lg-4 infocontact">
                    <h6>Kantor Kesehatan Pelabuhan Kelas II Padang</h6>
                    <p>Jl. Sutan Syahrir No.339 Kel. Rawang Kec.Padang Selatan Kota Padang Prov. Sumatera Barat</p>
                    <h6>Telp</h6>
                    <p>0751-61637 - Fax: 0751-61637</p>
                    <h6>Email</h6>
                    <p>kkppadang2@yahoo.co.id</p>
                </div>
                <div className="vertical-line"></div>
                <div className="col-lg-3 sosmedbar">
                    <h6>Contact Us</h6>
                    <a href="#tweeter"><img src={tweeter} alt="#tweeter"/></a>
                    <a href="#facebook"><img src={facebook} alt="#facebook"/></a>
                    <a href="#instagram"><img src={instagram} alt="#instagram"/></a>
                </div>
            </div>
            <div className="copyrightbar">
                <p> © 2020 Kantor Kesehatan Pelabuhan Kelas II Padang   Supported by PAM-TECHNO</p>
            </div>
        </div>
    );
}
export default WebFooter;