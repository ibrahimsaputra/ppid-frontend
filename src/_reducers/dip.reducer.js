const initialState = {
	dip:[
		
	],
	id: "",
	rincian: "",
	pejabat: "",
	pngjwb: "",
	wktbuatinfo: "",
	tmptbuatinfo: "",
	bfi_soft: "",
	bfi_hard: "",
	ji_bk: "",
	ji_ts: "",
	ji_sm: "",
	jk_wkt_simpan: "",
	keterangan: "",
	created_at: "",
	updated_at: "",
	enabled: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countDip:0,
}

export function dip(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_DIP':
			return {
				...state,
				dip: action.dip
			};
		case 'DIP_DETAIL':
			return {
				 ...state,
				id: action.id,
				rincian: action.rincian,
				pejabat: action.pejabat,
				pngjwb: action.pngjwb,
				wktbuatinfo: action.wktbuatinfo,
				tmptbuatinfo: action.tmptbuatinfo,
				bfi_soft: action.bfi_soft,
				bfi_hard: action.bfi_hard,
				ji_bk: action.ji_bk,
				ji_ts: action.ji_ts,
				ji_sm: action.ji_sm,
				jk_wkt_simpan: action.jk_wkt_simpan,
				keterangan: action.keterangan,
				created_at: action.created_at,
				updated_at: action.updated_at,
				enabled: action.enabled,
			};
		case 'DIP_UPDATED':
			return state;
		case 'HANDLE_DIP_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

