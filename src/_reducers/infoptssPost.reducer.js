import image1 from '../img/pelabuhan.jpg';
import image2 from '../img/pelabuhan2.jpg';
import pdf1 from '../img/pdf_1.pdf';
const initialState = {
	infoptssPost:[
		
	],
	id: "",
	id_menu: "",
	judul: "",
	body: "",
	thumbnail: undefined,
	link_pdf: undefined,
	created_at: "",
	updated_at: "",
	enabled: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countInfoptssPost:0,
}

export function infoptssPost(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_INFOPTSSPOST':
			return {
				...state,
				infoptssPost: action.infoptssPost
			};
		case 'INFOPTSSPOST_DETAIL':
			return {
				 ...state,
				id: action.id,
				id_menu: action.id_menu,
				judul: action.judul,
				body: action.body,
				thumbnail: action.thumbnail,
				link_pdf: action.link_pdf,
				created_at: action.created_at,
				updated_at: action.updated_at,
				enabled: action.enabled,
			};
		case 'INFOPTSSPOST_UPDATED':
			return state;
		case 'HANDLE_INFOPTSSPOST_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

