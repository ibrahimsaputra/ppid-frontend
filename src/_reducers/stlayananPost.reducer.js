const initialState = {
	stlayananPost:[
	],
	id: "",
	id_menu: "",
	judul: "",
	body: "",
	thumbnail: undefined,
	created_at: "",
	updated_at: "",
	enabled: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countStlayananPost:0,
}

export function stlayananPost(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_STLAYANANPOST':
			return {
				...state,
				stlayananPost: action.stlayananPost
			};
		case 'STLAYANANPOST_DETAIL':
			return {
				 ...state,
				id: action.id,
				id_menu: action.id_menu,
				judul: action.judul,
				body: action.body,
				thumbnail: action.thumbnail,
				created_at: action.created_at,
				updated_at: action.updated_at,
				enabled: action.enabled,
			};
		case 'STLAYANANPOST_UPDATED':
			return state;
		case 'HANDLE_STLAYANANPOST_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

