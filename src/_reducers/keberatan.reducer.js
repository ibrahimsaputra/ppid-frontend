const initialState = {
	keberatan:[
		// {
		// 	"id":1,
		// 	"nama_pemohon":"Nama",
		// 	"email":"Email",
		// 	"noktp":"NoKTP",
		// 	"alamat":"Alamat",
		// 	"nohp":"12345678",
		// 	"pil_keberatan":"regulasi",
		// 	"als_keberatan":"Penolakan atas Permintaan Informasi Berdasarkan Alasan Pengecualian Sebagaimana dimaksud dalam Pasal 17",
		// 	"als_lain":"",
		// 	"status":"belum dibaca ",
		// 	"created_at":"2020-07-22 04:57:17"
		// },
		// {
		// 	"id":2,
		// 	"nama_pemohon":"Nama2",
		// 	"email":"Email2",
		// 	"noktp":"NoKTP2",
		// 	"alamat":"Alamat2",
		// 	"nohp":"123456782",
		// 	"pil_keberatan":"lakip",
		// 	"als_keberatan":"Lainnya",
		// 	"als_lain":"Alasan Lainnya",
		// 	"status":"diabaikan",
		// 	"created_at":"2020-07-22 04:57:17"
		// }
	],
	id: "",
	nama_pemohon: "",
	email: "",
	alamat: "",
	nohp: "",
	pil_keberatan: "",
	als_keberatan: "",
	als_lain: "",
	status: "",
	created_at: "",
	updated_at: "",
	enabled: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countKeberatan:0,
}

export function keberatan(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_KEBERATAN':
			return {
				...state,
				keberatan: action.keberatan
			};
		case 'KEBERATAN_DETAIL':
			return {
				 ...state,
				id: action.id,
				nama_pemohon: action.nama_pemohon,
				email: action.email,
				alamat: action.alamat,
				nohp: action.nohp,
				pil_keberatan: action.pil_keberatan,
				als_keberatan: action.als_keberatan,
				als_lain: action.als_lain,
				status: action.status,
				created_at: action.created_at,
				updated_at: action.updated_at,
				enabled: action.enabled,
			};
		case 'KEBERATAN_UPDATED':
			return state;
		case 'HANDLE_KEBERATAN_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

