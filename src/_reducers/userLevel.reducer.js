const initialState = {
	userLevel:[],
	id: "",
	level_name: "",
	created_at: "",
	updated_at: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countUserLevel:0,
}

export function userLevel(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_USERLEVEL':
			return {
				...state,
				userLevel: action.userLevel
			};
		case 'USERLEVEL_DETAIL':
			return {
				 ...state,
				id: action.id,
				level_name: action.level_name,
				created_at: action.created_at,
				updated_at: action.updated_at,
			};
		case 'USERLEVEL_UPDATED':
			return state;
		case 'HANDLE_USERLEVEL_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

