const initialState = {
	user:[],
	id: "",
	id_level: "",
	username: "",
	password: "",
	created_at: "",
	updated_at: "",
	enabled: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countUser:0,
}

export function userReducer(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_USER':
			return {
				...state,
				user: action.user
			};
		case 'USER_DETAIL':
			return {
				 ...state,
				id: action.id,
				id_level: action.id_level,
				username: action.username,
				password: action.password,
				created_at: action.created_at,
				updated_at: action.updated_at,
				enabled: action.enabled,
			};
		case 'USER_UPDATED':
			return state;
		case 'HANDLE_USER_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

