import image1 from '../img/str_org.jpeg'
const initialState = {
	profil:[
	],
	id: "",
	profil_singkat: "<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
	tugas_fungsi: "<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
	link_struktur: image1,
	visi_misi: "<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
	created_at: "",
	updated_at: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countProfil:0,
}

export function profil(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_PROFIL':
			return {
				...state,
				profil: action.profil
			};
		case 'PROFIL_DETAIL':
			return {
				 ...state,
				id: action.id,
				profil_singkat: action.profil_singkat,
				tugas_fungsi: action.tugas_fungsi,
				link_struktur: action.link_struktur,
				visi_misi: action.visi_misi,
				created_at: action.created_at,
				updated_at: action.updated_at,
			};
		case 'PROFIL_UPDATED':
			return state;
		case 'HANDLE_PROFIL_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

