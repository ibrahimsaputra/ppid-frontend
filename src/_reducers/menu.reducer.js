const initialState = {
	menu:[
		{
			"id":1,
			"subMenus":[
                {
					"id":19,
					"subMenus":[],
					"nama_menu":"Profil Singkat",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":20,
					"subMenus":[],
					"nama_menu":"Tugas Dan Fungsi",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":21,
					"subMenus":[],
					"nama_menu":"Struktur Organisasi",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":22,
					"subMenus":[],
					"nama_menu":"Visi Misi",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				}
            ],
			"nama_menu":"Profil PPID",
			"enabled":"1",
			"tipe":"post"
		},
		{
			"id":2,
			"subMenus":[
				{
					"id":9,
					"subMenus":[],
					"nama_menu":"Regulasi Keterbukaan Informasi Publik",
					"enabled":"1",
					"tipe":"list pdf",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":10,
					"subMenus":[],
					"nama_menu":"Regulasi Kemenkes",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":11,
					"subMenus":[],
					"nama_menu":"Rancangan Regulasi Kemenkes",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":12,
					"subMenus":[],
					"nama_menu":"SOP Keterbukaan Informasi",
					"enabled":"1",
					"tipe":"list pdf",
					"created_at":"2020-07-22 04:57:17",
				},
			],
			"nama_menu":"Regulasi",
			"enabled":"1",
			"tipe":"post",
			"created_at":"2020-07-22 04:57:17",
		},
		{
			"id":3,
			"subMenus":[
				{
					"id":4,
					"subMenus":[],
					"nama_menu":"Informasi Publik Berkala",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":5,
					"subMenus":[],
					"nama_menu":" Informasi Publik Tersedia Setiap Saat",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":6,
					"subMenus":[],
					"nama_menu":" Informasi Publik Serta Merta",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
			],
			"nama_menu":"Informasi Publik",
			"enabled":"1",
			"tipe":"post",
			"created_at":"2020-07-22 04:57:17",
		},
		{
			"id":7,
			"subMenus":[
				{
					"id":13,
					"subMenus":[],
					"nama_menu":"Prosedur Permintaan Informasi",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":14,
					"subMenus":[],
					"nama_menu":"Maklumat Layanan",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":15,
					"subMenus":[],
					"nama_menu":"Prosedur Pengajuan Keberatan",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":16,
					"subMenus":[],
					"nama_menu":"Prosedur Permohonan Penyelesaian Sengketa Informasi Publik",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":17,
					"subMenus":[],
					"nama_menu":"Waktu Pelayanan Informasi",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				{
					"id":18,
					"subMenus":[],
					"nama_menu":"Standar Biaya Perolehan Informasi",
					"enabled":"1",
					"tipe":"post",
					"created_at":"2020-07-22 04:57:17",
				},
				
			],
			"nama_menu":"Standar Layanan",
			"enabled":"1",
			"tipe":"post",
			"created_at":"2020-07-22 04:57:17",
		},
		{
			"id":8,
			"subMenus":[],
			"nama_menu":"Laporan Layanan",
			"enabled":"1",
			"tipe":"list pdf",
			"created_at":"2020-07-22 04:57:17",
		},
	],
	id: "",
	id_parent: "",
	nama_menu: "",
	link_icon: undefined,
	tipe:"",
	enabled: "",
	created_at: "",
	updated_at: "",
}

export function menu(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_MENU':
			return {
				...state,
				menu: action.menu
			};
		case 'MENU_DETAIL':
			return {
				 ...state,
				id: action.id,
				id_parent: action.id_parent,
				nama_menu: action.nama_menu,
				link_icon: action.link_icon,
				tipe:action.tipe,
				enabled: action.enabled,
				created_at: action.created_at,
				updated_at: action.updated_at,
			};
		case 'MENU_UPDATED':
			return state;
		case 'HANDLE_MENU_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

