import pdf1 from '../img/pdf_1.pdf';
const initialState = {
	regulasiPost:[
	// 	{   "id":1,
    //     "id_menu":1,
    //     "judul":"Data Regulasi 1",
    //     "body":"<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
    //     "link_pdf":pdf1,
    //     "thumbnail":"",
    //     "created_at":"2020-07-22 04:57:17"
    // },
    // {
    //     "id":2,
    //     "id_menu":1,
    //     "judul":"Data Regulasi 1 2",
    //     "body":"<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
    //     "link_pdf":pdf1,
    //     "thumbnail":"",
    //     "created_at":"2020-07-22 10:57:17"
    // },
	],
	id: "",
	id_menu: "",
	judul: "",
	body: "",
	thumbnail: undefined,
	created_at: "",
	updated_at: "",
	enabled: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countRegulasiPost:0,
}

export function regulasiPost(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_REGULASIPOST':
			return {
				...state,
				regulasiPost: action.regulasiPost
			};
		case 'REGULASIPOST_DETAIL':
			return {
				 ...state,
				id: action.id,
				id_menu: action.id_menu,
				judul: action.judul,
				body: action.body,
				thumbnail: action.thumbnail,
				created_at: action.created_at,
				updated_at: action.updated_at,
				enabled: action.enabled,
			};
		case 'REGULASIPOST_UPDATED':
			return state;
		case 'HANDLE_REGULASIPOST_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

