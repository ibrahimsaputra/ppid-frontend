import {userReducer} from './user.reducer';
import {authReducer} from './auth.reducer';
import {dip} from './dip.reducer';
import {infopbPost} from './infopbPost.reducer';
import {infopsmPost} from './infopsmPost.reducer';
import {infoptssPost} from './infoptssPost.reducer';
import {keberatan} from './keberatan.reducer';
import {laplayanan} from './laplayanan.reducer';
import {menu} from './menu.reducer';
import {modul} from './modul.reducer';
import {modulAkses} from './modulAkses.reducer';
import {permohonan} from './permohonan.reducer';
import {profil} from './profil.reducer';
import {regulasiPost} from './regulasiPost.reducer';
import {stlayananPost} from './stlayananPost.reducer';
import {userLevel} from './userLevel.reducer';

import {combineReducers} from 'redux';

const allReducers = combineReducers({
    user: userReducer, 
    authentication:authReducer,
    dip,infopbPost,infopsmPost,infoptssPost,
    keberatan,laplayanan,menu,modul,modulAkses,permohonan,
    profil,regulasiPost,stlayananPost,userLevel
});

export default allReducers; 
