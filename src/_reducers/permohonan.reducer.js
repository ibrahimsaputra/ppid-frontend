import ktp from '../img/contoh_ktp.jpeg';
const initialState = {
	permohonan:[
        // {
        //     "id":1,
        //     "nama_pemohon":"Nama",
        //     "email":"Email",
        //     "noktp":"NoKTP",
        //     "alamat":"Alamat",
        //     "nohp":"12345678",
        //     "tujuan":"ppid kkp padang",
        //     "tipe_pemohon":"wni",
        //     "detail_wni":"perorangan",
        //     "uplaod_ktp":ktp,
        //     "info_kategori":"regulasi",
        //     "info_diminta":"Informasi Diminta",
        //     "cara":"diemail",
        //     "bentuk":"softcopy",
        //     "alasan":"Alasan",
        //     "kegunaan":"Keegunaan",
        //     "status":"belum dibaca ",
        //     "created_at":"2020-07-22 04:57:17"
        // },
        // {
        //     "id":2,
        //     "nama_pemohon":"Nama2",
        //     "email":"Email2",
        //     "noktp":"NoKTP2",
        //     "alamat":"Alamat2",
        //     "nohp":"123456782",
        //     "tujuan":"ppid kkp padang",
        //     "tipe_pemohon":"badan hukum",
        //     "detail_wni":"kelompok orang",
        //     "uplaod_ktp":ktp,
        //     "info_kategori":"rka",
        //     "info_diminta":"Informasi Diminta2",
        //     "cara":"dikirim langsung",
        //     "bentuk":"harcopy",
        //     "alasan":"Alasan2",
        //     "kegunaan":"Kegunaan2",
        //     "status":"dikirim",
        //     "created_at":"2020-07-22 04:57:17"
        // }
    ],
	id: "",
	nama_pemohon: "",
	email: "",
	noktp: "",
	alamat: "",
	nohp: "",
	tujuan: "",
	tipe_pemohon: "",
	detail_wni: "",
	upload_ktp: undefined,
	info_kategori: "",
	info_diminta: "",
	cara: "",
	bentuk: "",
	alasan: "",
	kegunaan: "",
	status: "",
	created_at: "",
	updated_at: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countPermohonan:0,
}

export function permohonan(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_PERMOHONAN':
			return {
				...state,
				permohonan: action.permohonan
			};
		case 'PERMOHONAN_DETAIL':
			return {
				 ...state,
				id: action.id,
				nama_pemohon: action.nama_pemohon,
				email: action.email,
				noktp: action.noktp,
				alamat: action.alamat,
				nohp: action.nohp,
				tujuan: action.tujuan,
				tipe_pemohon: action.tipe_pemohon,
				detail_wni: action.detail_wni,
				upload_ktp: action.upload_ktp,
				info_kategori: action.info_kategori,
				info_diminta: action.info_diminta,
				cara: action.cara,
				bentuk: action.bentuk,
				alasan: action.alasan,
				kegunaan: action.kegunaan,
				status: action.status,
				created_at: action.created_at,
				updated_at: action.updated_at,
			};
		case 'PERMOHONAN_UPDATED':
			return state;
		case 'HANDLE_PERMOHONAN_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

