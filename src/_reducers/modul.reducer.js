const initialState = {
	modul:[],
	id: "",
	kode: "",
	nama: "",
	created_at: "",
	enabled: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countModul:0,
}

export function modul(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_MODUL':
			return {
				...state,
				modul: action.modul
			};
		case 'MODUL_DETAIL':
			return {
				 ...state,
				id: action.id,
				kode: action.kode,
				nama: action.nama,
				created_at: action.created_at,
				enabled: action.enabled,
			};
		case 'MODUL_UPDATED':
			return state;
		case 'HANDLE_MODUL_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

