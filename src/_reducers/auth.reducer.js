let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? {loggedIn:true,isAdmin:true}:{resMessage:'',isAdmin:false};

export function authReducer(state=initialState,action){
    switch(action.type){
        case 'LOGIN_SUCCESS':
            return{
                logginIn:true,
                isAdmin:true
            };
        case 'LOGIN_FAILURE':
            return{
                resMessage:action.resMessage,
                isAdmin:false
            }
        case 'LOGOUT_SUCCESS':
            return  {
                loggedIn:false,
                isAdmin:false
            }
        case 'ADMIN':
            if (!state.isAdmin){
                return{
                    isAdmin:true
                }
            }return state;
        case 'WEB':
            if (state.isAdmin){
                return{
                    isAdmin:false
                }
            }
            return state;
        default:
            return state;

    }
}