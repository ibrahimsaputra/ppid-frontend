const initialState = {
	modulAkses:[],
	id_level: "",
	id_modul: "",
	l: "",
	d: "",
	t: "",
	u: "",
	h: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countModulAkses:0,
}

export function modulAkses(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_MODULAKSES':
			return {
				...state,
				modulAkses: action.modulAkses
			};
		case 'MODULAKSES_DETAIL':
			return {
				 ...state,
				id_level: action.id_level,
				id_modul: action.id_modul,
				l: action.l,
				d: action.d,
				t: action.t,
				u: action.u,
				h: action.h,
			};
		case 'MODULAKSES_UPDATED':
			return state;
		case 'HANDLE_MODULAKSES_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

