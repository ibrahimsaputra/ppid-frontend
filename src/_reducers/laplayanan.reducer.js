import pdf1 from '../img/pdf_1.pdf';
const initialState = {
	laplayanan:[{
        "id":1,
        "tahun":"2001",
        "enabled":"1",
        "link":pdf1,
        "created_at":"2020-07-22 04:57:17",
    },
    {
        "id":2,
        "tahun":"2002",
        "enabled":"0",
        "link":pdf1,
        "created_at":"2020-07-22 04:57:17",
    },],
	id: "",
	tahun: "",
	link: undefined,
	enabled: "",
	created_at: "",
	updated_at: "",
	selectedFiles:undefined,
	currentFile:undefined,
	countLaplayanan:0,
}

export function laplayanan(state=initialState,action) {
	switch (action.type){
		case 'GET_ALL_LAPLAYANAN':
			return {
				...state,
				laplayanan: action.laplayanan
			};
		case 'LAPLAYANAN_DETAIL':
			return {
				 ...state,
				id: action.id,
				tahun: action.tahun,
				link: action.link,
				enabled: action.enabled,
				created_at: action.created_at,
				updated_at: action.updated_at,
			};
		case 'LAPLAYANAN_UPDATED':
			return state;
		case 'HANDLE_LAPLAYANAN_ON_CHANGE':
			return {
				...state,
				[action.props]: action.value
			};
		default:
			return state;

	}
}

