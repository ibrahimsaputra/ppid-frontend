const post =[
    {
        "id":1,
        "id_menu":1,
        "judul":"Judul 1",
        "body":"<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
        "link_pdf":"http://localhost:3000/logo512.png",
        "thumbnail":"http://localhost:3000/logo512.png",
        "created_at":"2020-07-22 04:57:17"
    },
    {
        "id":2,
        "id_menu":1,
        "judul":"Judul 2",
        "body":"<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
        "link_pdf":"http://localhost:3000/logo512.png",
        "thumbnail":"http://localhost:3000/logo512.png",
        "created_at":"2020-07-22 10:57:17"
    },
];
const table_post =[
    {   "id":1,
        "id_menu":1,
        "judul":"Data Regulasi 1",
        "body":"<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
        "link_pdf":"http://localhost:3000/logo512.png",
        "thumbnail":"",
        "created_at":"2020-07-22 04:57:17"
    },
    {
        "id":2,
        "id_menu":1,
        "judul":"Data Regulasi 1 2",
        "body":"<h3>Test</h3> <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia adipisci odit repudiandae eum labore doloremque voluptatem cupiditate nesciunt laborum, nobis fugit eaque veritatis magnam! Pariatur suscipit vel illo itaque consequatur!</p>",
        "link_pdf":"http://localhost:3000/logo512.png",
        "thumbnail":"",
        "created_at":"2020-07-22 10:57:17"
    },
];
const dl_permohonan=[
    {
        "id":1,
        "nama_pemohon":"Nama",
        "email":"Email",
        "noktp":"NoKTP",
        "alamat":"Alamat",
        "nohp":"12345678",
        "tujuan":"ppid kkp padang",
        "tipe_pemohon":"wni",
        "detail_wni":"perorangan",
        "uplaod_ktp":"http://localhost:3000/logo512.png",
        "info_kategori":"regulasi",
        "info_diminta":"Informasi Diminta",
        "cara":"diemail",
        "bentuk":"softcopy",
        "alasan":"Alasan",
        "kegunaan":"Keegunaan",
        "status":"belum dibaca ",
        "created_at":"2020-07-22 04:57:17"
    },
    {
        "id":2,
        "nama_pemohon":"Nama2",
        "email":"Email2",
        "noktp":"NoKTP2",
        "alamat":"Alamat2",
        "nohp":"123456782",
        "tujuan":"ppid kkp padang",
        "tipe_pemohon":"badan hukum",
        "detail_wni":"kelompok orang",
        "uplaod_ktp":"http://localhost:3000/logo512.png",
        "info_kategori":"rka",
        "info_diminta":"Informasi Diminta2",
        "cara":"dikirim langsung",
        "bentuk":"harcopy",
        "alasan":"Alasan2",
        "kegunaan":"Keegunaan2",
        "status":"dikirim",
        "created_at":"2020-07-22 04:57:17"
    }
];
const dl_keberatan=[
    {
        "id":1,
        "nama_pemohon":"Nama",
        "email":"Email",
        "noktp":"NoKTP",
        "alamat":"Alamat",
        "nohp":"12345678",
        "pil_keberatan":"regulasi",
        "als_keberatan":"Penolakan atas Permintaan Informasi Berdasarkan Alasan Pengecualian Sebagaimana dimaksud dalam Pasal 17",
        "als_lain":"",
        "status":"belum dibaca ",
        "created_at":"2020-07-22 04:57:17"
    },
    {
        "id":2,
        "nama_pemohon":"Nama2",
        "email":"Email2",
        "noktp":"NoKTP2",
        "alamat":"Alamat2",
        "nohp":"123456782",
        "pil_keberatan":"lakip",
        "als_keberatan":"Lainnya",
        "als_lain":"Alasan Lainnya",
        "status":"diabaikan",
        "created_at":"2020-07-22 04:57:17"
    },
];
const dl_dip=[
    {
        "id":1,
        "rincian":"Rincian",
        "pejabat":"Pejabat",
        "pngjwb":"PenanggungJawab",
        "wktbuatinfo":"Waktu Buat Info",
        "tmtpbuatinfo":"Tempat Buat Info",
        "bfi_soft":"1",
        "bfi_hard":"0",
        "ji_bk":"1",
        "ji_ts":"0",
        "ji_sm":"1",
        "jk_wkt_simpan":"Waktu Simpan",
        "keterangan":"Keterangan",
        "created_at":"2020-07-22 04:57:17",
        "enabled":"1",        
    },
    {
        "id":2,
        "rincian":"Rincian2",
        "pejabat":"Pejabat2",
        "pngjwb":"PenanggungJawab2",
        "wktbuatinfo":"Waktu Buat Info2",
        "tmtpbuatinfo":"Tempat Buat Info2",
        "bfi_soft":"0",
        "bfi_hard":"1",
        "ji_bk":"0",
        "ji_ts":"1",
        "ji_sm":"0",
        "jk_wkt_simpan":"Waktu Simpan2",
        "keterangan":"Keterangan2",
        "created_at":"2020-07-22 04:57:17",
        "enabled":"1",        
    },
];
const dl_profil=[
    {
        "id":1,
        "profil_singkat":"<h1>Test</h1>",
        "tugas_fungsi":"<h1>Test</h1>",
        "link_struktur":"http://localhost:3000/logo512.png",
        "visi_misi":"<h1>Test</h1>",
        "created_at":"2020-07-22 04:57:17",
    },
];
const dl_laplayanan=[
    {
        "id":1,
        "tahun":"2001",
        "enabled":"1",
        "link":"http://localhost:3000/logo512.png",
        "created_at":"2020-07-22 04:57:17",
    },
    {
        "id":2,
        "tahun":"2002",
        "enabled":"0",
        "link":"http://localhost:3000/logo512.png",
        "created_at":"2020-07-22 04:57:17",
    },
];
const dl_menu=[
    {
        "id":1,
        "id_parent":null,
        "nama_menu":"Profil PPID",
        "enabled":"1",
        "link":"http://localhost:3000/logo512.png",
        "created_at":"2020-07-22 04:57:17",
    },
    {
        "id":2,
        "id_parent":null,
        "nama_menu":"Regulasi",
        "enabled":"1",
        "link":"http://localhost:3000/logo512.png",
        "created_at":"2020-07-22 04:57:17",
    },
    {
        "id":3,
        "id_parent":null,
        "nama_menu":"Informasi",
        "enabled":"1",
        "link":"http://localhost:3000/logo512.png",
        "created_at":"2020-07-22 04:57:17",
    },
];

const bar_menus=[
    {
        "id":1,
        "subMenus":[],
        "nama_menu":"Profil PPID",
        "enabled":"1",
    },
    {
        "id":2,
        "subMenus":[
            {
                "id":9,
                "subMenus":[],
                "nama_menu":"Regulasi Keterbukaan Informasi Publik",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":10,
                "subMenus":[],
                "nama_menu":"Regulasi Kemenkes",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":11,
                "subMenus":[],
                "nama_menu":"Rancangan Regulasi Kemenkes",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":12,
                "subMenus":[],
                "nama_menu":"SOP Keterbukaan Informasi",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
        ],
        "nama_menu":"Regulasi",
        "enabled":"1",
        "created_at":"2020-07-22 04:57:17",
    },
    {
        "id":3,
        "subMenus":[
            {
                "id":4,
                "subMenus":[],
                "nama_menu":"Informasi Publik Berkala",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":5,
                "subMenus":[],
                "nama_menu":" Informasi Publik Tersedia Setiap Saat",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":6,
                "subMenus":[],
                "nama_menu":" Informasi Publik Serta Merta",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
        ],
        "nama_menu":"Informasi Publik",
        "enabled":"1",
        "created_at":"2020-07-22 04:57:17",
    },
    {
        "id":7,
        "subMenus":[
            {
                "id":13,
                "subMenus":[],
                "nama_menu":"Prosedur Permintaan Informasi",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":14,
                "subMenus":[],
                "nama_menu":"Maklumat Layanan",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":15,
                "subMenus":[],
                "nama_menu":"Prosedur Pengajuan Keberatan",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":16,
                "subMenus":[],
                "nama_menu":"Prosedur Permohonan Penyelesaian Sengketa Informasi Publik",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":17,
                "subMenus":[],
                "nama_menu":"Waktu Pelayanan Informasi",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            {
                "id":18,
                "subMenus":[],
                "nama_menu":"Standar Biaya Perolehan Informasi",
                "enabled":"1",
                "created_at":"2020-07-22 04:57:17",
            },
            
        ],
        "nama_menu":"Standar Layanan",
        "enabled":"1",
        "created_at":"2020-07-22 04:57:17",
    },
    {
        "id":8,
        "subMenus":[],
        "nama_menu":"Laporan Layanan",
        "enabled":"1",
        "created_at":"2020-07-22 04:57:17",
    },
]

