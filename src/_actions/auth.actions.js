import {authService} from '../_services';
import {history} from '../helpers';

export const authActions = {
    login,
    logout
}

function login (username,password){
    return dispatch => {
        authService.login(username,password)
        .then(()=>{
            const user = JSON.parse(localStorage.getItem('user'))
            if(user){
                dispatch(loginSuccess());
                history.push("/admin/dashboard");
            }else{
                dispatch(loginFailure("Wrong username or password "))
            }
            // if(user && user.accessToken){
            //     dispatch(loginSuccess());
            //     history.push("/admin/dashboard");
            // }
        },error =>{
            const resMessage = 
            (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();
            dispatch(loginFailure(resMessage));
        });
    }
    
}

function logout(){
    return dispatch =>{
        localStorage.removeItem('user');
        dispatch(logoutUser());
        history.push('/')
    }
}


export function loginSuccess(){
    return{
        type:"LOGIN_SUCCESS"
    }
}
export function loginFailure(resMessage){
    return {
        type:"LOGIN_FAILURE",
        resMessage:resMessage
    }
}

export function logoutUser(){
    return{
        type:"LOGOUT_SUCCESS"
    }
}

export function admin(){
    return {
        type:'ADMIN'
    }
}

export function web(){
    return {
        type:'WEB'
    }
}