import {profilService} from '../_services';
import {history} from '../helpers';

export const profilActions = {
	getProfil,
	getProfilById,
	onChangeProfilProps,
	editProfilInfo,
	deleteProfilById
};

function getProfil(){
	return dispatch => {
		let apiEndPoint = "";
		profilService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeProfilsList(response.data));
			console.log(response)
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getProfilById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		profilService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editProfilDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function onChangeProfilProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeProfilProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeProfilProps(props, event));
		}else{
			dispatch(handleOnChangeProfilProps(props, event.target.value));
		}
	}
}

function editProfilInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		profilService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedProfilInfo());
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteProfilById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		profilService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteProfilDetails());
			dispatch(profilActions.getProfil());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeProfilsList(profil){
	return{
		type: "GET_ALL_PROFIL",
		profil: profil
	}
}

export function handleOnChangeProfilProps(props, value){
	return{
		type: "HANDLE_PROFIL_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editProfilDetails(profil){
	return{
		type: "PROFIL_DETAIL",
		id: profil.id,
		profil_singkat: profil.profil_singkat,
		tugas_fungsi: profil.tugas_fungsi,
		link_struktur: profil.link_struktur,
		visi_misi: profil.visi_misi,
		created_at: profil.created_at,
		updated_at: profil.updated_at,
	}
}

export function updatedProfilInfo(){
	return{
		type: "PROFIL_UPDATED"
	}
}

export function createProfilInfo(){
	return{
		type: "PROFIL_CREATED_SUCCESSFULLY"
	}
}

export function deleteProfilDetails(){
	return{
		type: "DELETED_PROFIL_DETAILS"
	}
}

