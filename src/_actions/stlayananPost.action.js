import {stlayananPostService} from '../_services';
import {history} from '../helpers';

export const stlayananPostActions = {
	getStlayananPost,
	getStlayananPostById,
	getStlayananPostByMenuId,
	getStlayananPostCount,
	onChangeStlayananPostProps,
	createStlayananPost,
	editStlayananPostInfo,
	deleteStlayananPostById
};

function getStlayananPost(){
	return dispatch => {
		let apiEndPoint = "";
		stlayananPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeStlayananPostsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getStlayananPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		stlayananPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editStlayananPostDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getStlayananPostByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		stlayananPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeStlayananPostsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getStlayananPostCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		stlayananPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeStlayananPostProps("countStlayananPost", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createStlayananPost(payload){
	return dispatch => {
		stlayananPostService.post(payload)
		.then((response)=>{
			dispatch(createStlayananPostInfo());
			history.push('/admin/stlayananpost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeStlayananPostProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeStlayananPostProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeStlayananPostProps(props, event));
		}else{
			dispatch(handleOnChangeStlayananPostProps(props, event.target.value));
		}
	}
}

function editStlayananPostInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		stlayananPostService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedStlayananPostInfo());
			history.push('/admin/stlayananpost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteStlayananPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		stlayananPostService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteStlayananPostDetails());
			dispatch(stlayananPostActions.getStlayananPost());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeStlayananPostsList(stlayananPost){
	return{
		type: "GET_ALL_STLAYANANPOST",
		stlayananPost: stlayananPost
	}
}

export function handleOnChangeStlayananPostProps(props, value){
	return{
		type: "HANDLE_STLAYANANPOST_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editStlayananPostDetails(stlayananPost){
	return{
		type: "STLAYANANPOST_DETAIL",
		id: stlayananPost.id,
		id_menu: stlayananPost.id_menu,
		judul: stlayananPost.judul,
		body: stlayananPost.body,
		thumbnail: stlayananPost.thumbnail,
		created_at: stlayananPost.created_at,
		updated_at: stlayananPost.updated_at,
		enabled: stlayananPost.enabled,
	}
}

export function updatedStlayananPostInfo(){
	return{
		type: "STLAYANANPOST_UPDATED"
	}
}

export function createStlayananPostInfo(){
	return{
		type: "STLAYANANPOST_CREATED_SUCCESSFULLY"
	}
}

export function deleteStlayananPostDetails(){
	return{
		type: "DELETED_STLAYANANPOST_DETAILS"
	}
}

