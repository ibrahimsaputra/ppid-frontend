import {infopbPostService} from '../_services';
import {history} from '../helpers';

export const infopbPostActions = {
	getInfopbPost,
	getInfopbPostById,
	getInfopbPostByMenuId,
	getInfopbPostCount,
	onChangeInfopbPostProps,
	createInfopbPost,
	editInfopbPostInfo,
	deleteInfopbPostById
};

function getInfopbPost(){
	return dispatch => {
		let apiEndPoint = "";
		infopbPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeInfopbPostsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getInfopbPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infopbPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editInfopbPostDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getInfopbPostByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		infopbPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeInfopbPostsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getInfopbPostCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		infopbPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeInfopbPostProps("countInfopbPost", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createInfopbPost(payload){
	return dispatch => {
		infopbPostService.post(payload)
		.then((response)=>{
			dispatch(createInfopbPostInfo());
			history.push('/admin/infopbpost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeInfopbPostProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeInfopbPostProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeInfopbPostProps(props, event));
		}else{
			dispatch(handleOnChangeInfopbPostProps(props, event.target.value));
		}
	}
}

function editInfopbPostInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infopbPostService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedInfopbPostInfo());
			history.push('/admin/infopbpost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteInfopbPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infopbPostService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteInfopbPostDetails());
			dispatch(infopbPostActions.getInfopbPost());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeInfopbPostsList(infopbPost){
	return{
		type: "GET_ALL_INFOPBPOST",
		infopbPost: infopbPost
	}
}

export function handleOnChangeInfopbPostProps(props, value){
	return{
		type: "HANDLE_INFOPBPOST_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editInfopbPostDetails(infopbPost){
	return{
		type: "INFOPBPOST_DETAIL",
		id: infopbPost.id,
		id_menu: infopbPost.id_menu,
		judul: infopbPost.judul,
		body: infopbPost.body,
		thumbnail: infopbPost.thumbnail,
		link_pdf: infopbPost.link_pdf,
		created_at: infopbPost.created_at,
		updated_at: infopbPost.updated_at,
		enabled: infopbPost.enabled,
	}
}

export function updatedInfopbPostInfo(){
	return{
		type: "INFOPBPOST_UPDATED"
	}
}

export function createInfopbPostInfo(){
	return{
		type: "INFOPBPOST_CREATED_SUCCESSFULLY"
	}
}

export function deleteInfopbPostDetails(){
	return{
		type: "DELETED_INFOPBPOST_DETAILS"
	}
}

