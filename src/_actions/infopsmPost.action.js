import {infopsmPostService} from '../_services';
import {history} from '../helpers';

export const infopsmPostActions = {
	getInfopsmPost,
	getInfopsmPostById,
	getInfopsmPostByMenuId,
	getInfopsmPostCount,
	onChangeInfopsmPostProps,
	createInfopsmPost,
	editInfopsmPostInfo,
	deleteInfopsmPostById
};

function getInfopsmPost(){
	return dispatch => {
		let apiEndPoint = "";
		infopsmPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeInfopsmPostsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getInfopsmPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infopsmPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editInfopsmPostDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getInfopsmPostByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		infopsmPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeInfopsmPostsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getInfopsmPostCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		infopsmPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeInfopsmPostProps("countInfopsmPost", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createInfopsmPost(payload){
	return dispatch => {
		infopsmPostService.post(payload)
		.then((response)=>{
			dispatch(createInfopsmPostInfo());
			history.push('/admin/infopsmpost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeInfopsmPostProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeInfopsmPostProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeInfopsmPostProps(props, event));
		}else{
			dispatch(handleOnChangeInfopsmPostProps(props, event.target.value));
		}
	}
}

function editInfopsmPostInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infopsmPostService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedInfopsmPostInfo());
			history.push('/admin/infopsmpost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteInfopsmPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infopsmPostService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteInfopsmPostDetails());
			dispatch(infopsmPostActions.getInfopsmPost());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeInfopsmPostsList(infopsmPost){
	return{
		type: "GET_ALL_INFOPSMPOST",
		infopsmPost: infopsmPost
	}
}

export function handleOnChangeInfopsmPostProps(props, value){
	return{
		type: "HANDLE_INFOPSMPOST_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editInfopsmPostDetails(infopsmPost){
	return{
		type: "INFOPSMPOST_DETAIL",
		id: infopsmPost.id,
		id_menu: infopsmPost.id_menu,
		judul: infopsmPost.judul,
		body: infopsmPost.body,
		thumbnail: infopsmPost.thumbnail,
		link_pdf: infopsmPost.link_pdf,
		created_at: infopsmPost.created_at,
		updated_at: infopsmPost.updated_at,
		enabled: infopsmPost.enabled,
	}
}

export function updatedInfopsmPostInfo(){
	return{
		type: "INFOPSMPOST_UPDATED"
	}
}

export function createInfopsmPostInfo(){
	return{
		type: "INFOPSMPOST_CREATED_SUCCESSFULLY"
	}
}

export function deleteInfopsmPostDetails(){
	return{
		type: "DELETED_INFOPSMPOST_DETAILS"
	}
}

