import {permohonanService} from '../_services';
import {history} from '../helpers';

export const permohonanActions = {
	getPermohonan,
	getPermohonanById,
	getPermohonanByMenuId,
	getPermohonanCount,
	onChangePermohonanProps,
	createPermohonan,
	editPermohonanInfo,
	deletePermohonanById
};

function getPermohonan(){
	return dispatch => {
		let apiEndPoint = "";
		permohonanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changePermohonansList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getPermohonanById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		permohonanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editPermohonanDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getPermohonanByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		permohonanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changePermohonansList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getPermohonanCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		permohonanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangePermohonanProps("countPermohonan", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createPermohonan(payload){
	return dispatch => {
		permohonanService.post(payload)
		.then((response)=>{
			dispatch(createPermohonanInfo());
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangePermohonanProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangePermohonanProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangePermohonanProps(props, event));
		}else{
			dispatch(handleOnChangePermohonanProps(props, event.target.value));
		}
	}
}

function editPermohonanInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		permohonanService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedPermohonanInfo());
			history.push('/admin/permohonan');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deletePermohonanById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		permohonanService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deletePermohonanDetails());
			dispatch(permohonanActions.getPermohonan());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changePermohonansList(permohonan){
	return{
		type: "GET_ALL_PERMOHONAN",
		permohonan: permohonan
	}
}

export function handleOnChangePermohonanProps(props, value){
	return{
		type: "HANDLE_PERMOHONAN_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editPermohonanDetails(permohonan){
	return{
		type: "PERMOHONAN_DETAIL",
		id: permohonan.id,
		nama_pemohon: permohonan.nama_pemohon,
		email: permohonan.email,
		noktp: permohonan.noktp,
		alamat: permohonan.alamat,
		nohp: permohonan.nohp,
		tujuan: permohonan.tujuan,
		tipe_pemohon: permohonan.tipe_pemohon,
		detail_wni: permohonan.detail_wni,
		upload_ktp: permohonan.upload_ktp,
		info_kategori: permohonan.info_kategori,
		info_diminta: permohonan.info_diminta,
		cara: permohonan.cara,
		bentuk: permohonan.bentuk,
		alasan: permohonan.alasan,
		kegunaan: permohonan.kegunaan,
		status: permohonan.status,
		created_at: permohonan.created_at,
		updated_at: permohonan.updated_at,
	}
}

export function updatedPermohonanInfo(){
	return{
		type: "PERMOHONAN_UPDATED"
	}
}

export function createPermohonanInfo(){
	return{
		type: "PERMOHONAN_CREATED_SUCCESSFULLY"
	}
}

export function deletePermohonanDetails(){
	return{
		type: "DELETED_PERMOHONAN_DETAILS"
	}
}

