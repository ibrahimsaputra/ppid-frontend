import {userService} from '../_services';
import {history} from '../helpers';

export const userActions = {
    getUser,
    getUserById,
    onChangeUserProps,
    createUser,
    editUserInfo,
    deleteUserById
};

function getUser(){
    return dispatch => {
        userService.get()
        .then((response)=>{
            dispatch(changeUsersList(response.data.data));
        }
        ).catch((err)=>{
            console.log(err);
        })
    }
}

function getUserById(id){
    return dispatch => {
        let apiEndPoint = "/"+id;
        userService.get(apiEndPoint)
        .then((response)=>{
            dispatch(editUserDetails(response.data.data));
        })
    };
}

function createUser(payload){

    return dispatch => {
        userService.post(payload)
        .then((response)=>{
            dispatch(createUserInfo());
            history.push('/user');
        })
    }
}

function onChangeUserProps(props, event){
    return dispatch =>{
        dispatch(handleOnChangeUserProps(props, event.target.value));
    }
}

function editUserInfo(id,payload){
    return dispatch => {
        let apiEndPoint = "/"+id;
        userService.put(apiEndPoint,payload)
        .then((response)=>{
            dispatch(updatedUserInfo());
            history.push('/user');
        })
    }
}


function deleteUserById(id){
    return dispatch => {
        let apiEndPoint = "/"+id;
        userService.deleteById(apiEndPoint)
        .then((response)=>{
             dispatch(deleteUserDetails());
             dispatch(userActions.getUser());
        })
    };
}

export function changeUsersList(user){
    return{
        type: "GET_ALL_USER",
        user: user
    }
}

export function handleOnChangeUserProps(props, value){
    return{
        type: "HANDLE_USER_ON_CHANGE",
        props: props,
        value: value
    }
}
export function editUserDetails(user){
    return{
        type: "USER_DETAIL",
        id: user.id,
        userLevel:user.userLevel,
        username: user.username,
        password:user.password,
    }
}

export function updatedUserInfo(){
    return{
        type: "USER_UPDATED"
    }
}

export function createUserInfo(){
    return{
        type: "USER_CREATED_SUCCESSFULLY"
    }
}

export function deleteUserDetails(){
    return{
        type: "DELETED_USER_DETAILS"
    }
}
