import {modulAksesService} from '../_services';
import {history} from '../helpers';

export const modulAksesActions = {
	getModulAkses,
	getModulAksesById,
	getModulAksesByMenuId,
	getModulAksesCount,
	onChangeModulAksesProps,
	createModulAkses,
	editModulAksesInfo,
	deleteModulAksesById
};

function getModulAkses(){
	return dispatch => {
		let apiEndPoint = "";
		modulAksesService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeModulAksessList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getModulAksesById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		modulAksesService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editModulAksesDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getModulAksesByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		modulAksesService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeModulAksessList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getModulAksesCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		modulAksesService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeModulAksesProps("countModulAkses", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createModulAkses(payload){
	return dispatch => {
		modulAksesService.post(payload)
		.then((response)=>{
			dispatch(createModulAksesInfo());
			history.push('/admin/modulakses');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeModulAksesProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeModulAksesProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeModulAksesProps(props, event));
		}else{
			dispatch(handleOnChangeModulAksesProps(props, event.target.value));
		}
	}
}

function editModulAksesInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		modulAksesService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedModulAksesInfo());
			history.push('/admin/modulakses');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteModulAksesById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		modulAksesService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteModulAksesDetails());
			dispatch(modulAksesActions.getModulAkses());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeModulAksessList(modulAkses){
	return{
		type: "GET_ALL_MODULAKSES",
		modulAkses: modulAkses
	}
}

export function handleOnChangeModulAksesProps(props, value){
	return{
		type: "HANDLE_MODULAKSES_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editModulAksesDetails(modulAkses){
	return{
		type: "MODULAKSES_DETAIL",
		id_level: modulAkses.id_level,
		id_modul: modulAkses.id_modul,
		l: modulAkses.l,
		d: modulAkses.d,
		t: modulAkses.t,
		u: modulAkses.u,
		h: modulAkses.h,
	}
}

export function updatedModulAksesInfo(){
	return{
		type: "MODULAKSES_UPDATED"
	}
}

export function createModulAksesInfo(){
	return{
		type: "MODULAKSES_CREATED_SUCCESSFULLY"
	}
}

export function deleteModulAksesDetails(){
	return{
		type: "DELETED_MODULAKSES_DETAILS"
	}
}

