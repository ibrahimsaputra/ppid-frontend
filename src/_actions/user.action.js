import {userService} from '../_services';
import {history} from '../helpers';

export const userActions = {
	getUser,
	getUserById,
	getUserByMenuId,
	getUserCount,
	onChangeUserProps,
	createUser,
	editUserInfo,
	deleteUserById
};

function getUser(){
	return dispatch => {
		let apiEndPoint = "";
		userService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeUsersList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getUserById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		userService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editUserDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getUserByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		userService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeUsersList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getUserCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		userService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeUserProps("countUser", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createUser(payload){
	return dispatch => {
		userService.post(payload)
		.then((response)=>{
			dispatch(createUserInfo());
			history.push('/admin/user');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeUserProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeUserProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeUserProps(props, event));
		}else{
			dispatch(handleOnChangeUserProps(props, event.target.value));
		}
	}
}

function editUserInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		userService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedUserInfo());
			history.push('/admin/user');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteUserById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		userService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteUserDetails());
			dispatch(userActions.getUser());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeUsersList(user){
	return{
		type: "GET_ALL_USER",
		user: user
	}
}

export function handleOnChangeUserProps(props, value){
	return{
		type: "HANDLE_USER_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editUserDetails(user){
	return{
		type: "USER_DETAIL",
		id: user.id,
		id_level: user.id_level,
		username: user.username,
		password: user.password,
		created_at: user.created_at,
		updated_at: user.updated_at,
		enabled: user.enabled,
	}
}

export function updatedUserInfo(){
	return{
		type: "USER_UPDATED"
	}
}

export function createUserInfo(){
	return{
		type: "USER_CREATED_SUCCESSFULLY"
	}
}

export function deleteUserDetails(){
	return{
		type: "DELETED_USER_DETAILS"
	}
}

