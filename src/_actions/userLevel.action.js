import {userLevelService} from '../_services';
import {history} from '../helpers';

export const userLevelActions = {
	getUserLevel,
	getUserLevelById,
	getUserLevelByMenuId,
	getUserLevelCount,
	onChangeUserLevelProps,
	createUserLevel,
	editUserLevelInfo,
	deleteUserLevelById
};

function getUserLevel(){
	return dispatch => {
		let apiEndPoint = "";
		userLevelService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeUserLevelsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getUserLevelById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		userLevelService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editUserLevelDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getUserLevelByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		userLevelService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeUserLevelsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getUserLevelCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		userLevelService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeUserLevelProps("countUserLevel", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createUserLevel(payload){
	return dispatch => {
		userLevelService.post(payload)
		.then((response)=>{
			dispatch(createUserLevelInfo());
			history.push('/admin/userlevel');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeUserLevelProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeUserLevelProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeUserLevelProps(props, event));
		}else{
			dispatch(handleOnChangeUserLevelProps(props, event.target.value));
		}
	}
}

function editUserLevelInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		userLevelService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedUserLevelInfo());
			history.push('/admin/userlevel');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteUserLevelById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		userLevelService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteUserLevelDetails());
			dispatch(userLevelActions.getUserLevel());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeUserLevelsList(userLevel){
	return{
		type: "GET_ALL_USERLEVEL",
		userLevel: userLevel
	}
}

export function handleOnChangeUserLevelProps(props, value){
	return{
		type: "HANDLE_USERLEVEL_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editUserLevelDetails(userLevel){
	return{
		type: "USERLEVEL_DETAIL",
		id: userLevel.id,
		level_name: userLevel.level_name,
		created_at: userLevel.created_at,
		updated_at: userLevel.updated_at,
	}
}

export function updatedUserLevelInfo(){
	return{
		type: "USERLEVEL_UPDATED"
	}
}

export function createUserLevelInfo(){
	return{
		type: "USERLEVEL_CREATED_SUCCESSFULLY"
	}
}

export function deleteUserLevelDetails(){
	return{
		type: "DELETED_USERLEVEL_DETAILS"
	}
}

