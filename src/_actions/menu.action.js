import {menuService} from '../_services';
import {history} from '../helpers';

export const menuActions = {
	getMenu,
	getMenuById,
	getMenuByMenuId,
	getMenuCount,
	onChangeMenuProps,
	createMenu,
	editMenuInfo,
	deleteMenuById
};

function getMenu(){
	return dispatch => {
		let apiEndPoint = "";
		menuService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeMenusList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getMenuById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		menuService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editMenuDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getMenuByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		menuService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeMenusList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getMenuCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		menuService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeMenuProps("countMenu", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createMenu(payload){
	return dispatch => {
		menuService.post(payload)
		.then((response)=>{
			dispatch(createMenuInfo());
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeMenuProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeMenuProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeMenuProps(props, event));
		}else{
			dispatch(handleOnChangeMenuProps(props, event.target.value));
		}
	}
}

function editMenuInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		menuService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedMenuInfo());
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteMenuById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		menuService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteMenuDetails());
			dispatch(menuActions.getMenu());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeMenusList(menu){
	return{
		type: "GET_ALL_MENU",
		menu: menu
	}
}

export function handleOnChangeMenuProps(props, value){
	return{
		type: "HANDLE_MENU_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editMenuDetails(menu){
	return{
		type: "MENU_DETAIL",
		id: menu.id,
		id_parent: menu.id_parent,
		nama_menu: menu.nama_menu,
		tipe: menu.tipe,
		link_icon: menu.link_icon,
		enabled: menu.enabled,
		created_at: menu.created_at,
		updated_at: menu.updated_at,
	}
}

export function updatedMenuInfo(){
	return{
		type: "MENU_UPDATED"
	}
}

export function createMenuInfo(){
	return{
		type: "MENU_CREATED_SUCCESSFULLY"
	}
}

export function deleteMenuDetails(){
	return{
		type: "DELETED_MENU_DETAILS"
	}
}

