import {laplayananService} from '../_services';
import {history} from '../helpers';

export const laplayananActions = {
	getLaplayanan,
	getLaplayananById,
	getLaplayananByMenuId,
	getLaplayananCount,
	onChangeLaplayananProps,
	createLaplayanan,
	editLaplayananInfo,
	deleteLaplayananById
};

function getLaplayanan(){
	return dispatch => {
		let apiEndPoint = "";
		laplayananService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeLaplayanansList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getLaplayananById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		laplayananService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editLaplayananDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getLaplayananByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		laplayananService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeLaplayanansList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getLaplayananCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		laplayananService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeLaplayananProps("countLaplayanan", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createLaplayanan(payload){
	return dispatch => {
		laplayananService.post(payload)
		.then((response)=>{
			dispatch(createLaplayananInfo());
			history.push('/admin/laplayanan');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeLaplayananProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeLaplayananProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeLaplayananProps(props, event));
		}else{
			dispatch(handleOnChangeLaplayananProps(props, event.target.value));
		}
	}
}

function editLaplayananInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		laplayananService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedLaplayananInfo());
			history.push('/admin/laplayanan');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteLaplayananById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		laplayananService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteLaplayananDetails());
			dispatch(laplayananActions.getLaplayanan());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeLaplayanansList(laplayanan){
	return{
		type: "GET_ALL_LAPLAYANAN",
		laplayanan: laplayanan
	}
}

export function handleOnChangeLaplayananProps(props, value){
	return{
		type: "HANDLE_LAPLAYANAN_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editLaplayananDetails(laplayanan){
	return{
		type: "LAPLAYANAN_DETAIL",
		id: laplayanan.id,
		tahun: laplayanan.tahun,
		link: laplayanan.link,
		enabled: laplayanan.enabled,
		created_at: laplayanan.created_at,
		updated_at: laplayanan.updated_at,
	}
}

export function updatedLaplayananInfo(){
	return{
		type: "LAPLAYANAN_UPDATED"
	}
}

export function createLaplayananInfo(){
	return{
		type: "LAPLAYANAN_CREATED_SUCCESSFULLY"
	}
}

export function deleteLaplayananDetails(){
	return{
		type: "DELETED_LAPLAYANAN_DETAILS"
	}
}

