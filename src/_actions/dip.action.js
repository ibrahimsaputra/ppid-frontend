import {dipService} from '../_services';
import {history} from '../helpers';

export const dipActions = {
	getDip,
	getDipById,
	getDipByMenuId,
	getDipCount,
	onChangeDipProps,
	createDip,
	editDipInfo,
	deleteDipById
};

function getDip(){
	return dispatch => {
		let apiEndPoint = "";
		dipService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeDipsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getDipById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		dipService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editDipDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getDipByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		dipService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeDipsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getDipCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		dipService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeDipProps("countDip", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createDip(payload){
	return dispatch => {
		dipService.post(payload)
		.then((response)=>{
			dispatch(createDipInfo());
			history.push('/admin/dip');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeDipProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeDipProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeDipProps(props, event));
		}else{
			dispatch(handleOnChangeDipProps(props, event.target.value));
		}
	}
}

function editDipInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		dipService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedDipInfo());
			history.push('/admin/dip');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteDipById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		dipService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteDipDetails());
			dispatch(dipActions.getDip());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeDipsList(dip){
	return{
		type: "GET_ALL_DIP",
		dip: dip
	}
}

export function handleOnChangeDipProps(props, value){
	return{
		type: "HANDLE_DIP_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editDipDetails(dip){
	return{
		type: "DIP_DETAIL",
		id: dip.id,
		rincian: dip.rincian,
		pejabat: dip.pejabat,
		pngjwb: dip.pngjwb,
		wktbuatinfo: dip.wktbuatinfo,
		tmptbuatinfo: dip.tmptbuatinfo,
		bfi_soft: dip.bfi_soft,
		bfi_hard: dip.bfi_hard,
		ji_bk: dip.ji_bk,
		ji_ts: dip.ji_ts,
		ji_sm: dip.ji_sm,
		jk_wkt_simpan: dip.jk_wkt_simpan,
		keterangan: dip.keterangan,
		created_at: dip.created_at,
		updated_at: dip.updated_at,
		enabled: dip.enabled,
	}
}

export function updatedDipInfo(){
	return{
		type: "DIP_UPDATED"
	}
}

export function createDipInfo(){
	return{
		type: "DIP_CREATED_SUCCESSFULLY"
	}
}

export function deleteDipDetails(){
	return{
		type: "DELETED_DIP_DETAILS"
	}
}

