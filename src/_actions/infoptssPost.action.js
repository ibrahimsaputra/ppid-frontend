import {infoptssPostService} from '../_services';
import {history} from '../helpers';

export const infoptssPostActions = {
	getInfoptssPost,
	getInfoptssPostById,
	getInfoptssPostByMenuId,
	getInfoptssPostCount,
	onChangeInfoptssPostProps,
	createInfoptssPost,
	editInfoptssPostInfo,
	deleteInfoptssPostById
};

function getInfoptssPost(){
	return dispatch => {
		let apiEndPoint = "";
		infoptssPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeInfoptssPostsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getInfoptssPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infoptssPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editInfoptssPostDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getInfoptssPostByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		infoptssPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeInfoptssPostsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getInfoptssPostCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		infoptssPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeInfoptssPostProps("countInfoptssPost", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createInfoptssPost(payload){
	return dispatch => {
		infoptssPostService.post(payload)
		.then((response)=>{
			dispatch(createInfoptssPostInfo());
			history.push('/admin/infoptsspost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeInfoptssPostProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeInfoptssPostProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeInfoptssPostProps(props, event));
		}else{
			dispatch(handleOnChangeInfoptssPostProps(props, event.target.value));
		}
	}
}

function editInfoptssPostInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infoptssPostService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedInfoptssPostInfo());
			history.push('/admin/infoptsspost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteInfoptssPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		infoptssPostService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteInfoptssPostDetails());
			dispatch(infoptssPostActions.getInfoptssPost());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeInfoptssPostsList(infoptssPost){
	return{
		type: "GET_ALL_INFOPTSSPOST",
		infoptssPost: infoptssPost
	}
}

export function handleOnChangeInfoptssPostProps(props, value){
	return{
		type: "HANDLE_INFOPTSSPOST_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editInfoptssPostDetails(infoptssPost){
	return{
		type: "INFOPTSSPOST_DETAIL",
		id: infoptssPost.id,
		id_menu: infoptssPost.id_menu,
		judul: infoptssPost.judul,
		body: infoptssPost.body,
		thumbnail: infoptssPost.thumbnail,
		link_pdf: infoptssPost.link_pdf,
		created_at: infoptssPost.created_at,
		updated_at: infoptssPost.updated_at,
		enabled: infoptssPost.enabled,
	}
}

export function updatedInfoptssPostInfo(){
	return{
		type: "INFOPTSSPOST_UPDATED"
	}
}

export function createInfoptssPostInfo(){
	return{
		type: "INFOPTSSPOST_CREATED_SUCCESSFULLY"
	}
}

export function deleteInfoptssPostDetails(){
	return{
		type: "DELETED_INFOPTSSPOST_DETAILS"
	}
}

