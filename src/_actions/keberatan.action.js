import {keberatanService} from '../_services';
import {history} from '../helpers';

export const keberatanActions = {
	getKeberatan,
	getKeberatanById,
	getKeberatanByMenuId,
	getKeberatanCount,
	onChangeKeberatanProps,
	createKeberatan,
	editKeberatanInfo,
	deleteKeberatanById
};

function getKeberatan(){
	return dispatch => {
		let apiEndPoint = "";
		keberatanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeKeberatansList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getKeberatanById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		keberatanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editKeberatanDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getKeberatanByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		keberatanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeKeberatansList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getKeberatanCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		keberatanService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeKeberatanProps("countKeberatan", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createKeberatan(payload){
	return dispatch => {
		keberatanService.post(payload)
		.then((response)=>{
			dispatch(createKeberatanInfo());
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeKeberatanProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeKeberatanProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeKeberatanProps(props, event));
		}else{
			dispatch(handleOnChangeKeberatanProps(props, event.target.value));
		}
	}
}

function editKeberatanInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		keberatanService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedKeberatanInfo());
			history.push('/admin/keberatan');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteKeberatanById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		keberatanService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteKeberatanDetails());
			dispatch(keberatanActions.getKeberatan());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeKeberatansList(keberatan){
	return{
		type: "GET_ALL_KEBERATAN",
		keberatan: keberatan
	}
}

export function handleOnChangeKeberatanProps(props, value){
	return{
		type: "HANDLE_KEBERATAN_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editKeberatanDetails(keberatan){
	return{
		type: "KEBERATAN_DETAIL",
		id: keberatan.id,
		nama_pemohon: keberatan.nama_pemohon,
		email: keberatan.email,
		alamat: keberatan.alamat,
		nohp: keberatan.nohp,
		pil_keberatan: keberatan.pil_keberatan,
		als_keberatan: keberatan.als_keberatan,
		als_lain: keberatan.als_lain,
		status: keberatan.status,
		created_at: keberatan.created_at,
		updated_at: keberatan.updated_at,
		enabled: keberatan.enabled,
	}
}

export function updatedKeberatanInfo(){
	return{
		type: "KEBERATAN_UPDATED"
	}
}

export function createKeberatanInfo(){
	return{
		type: "KEBERATAN_CREATED_SUCCESSFULLY"
	}
}

export function deleteKeberatanDetails(){
	return{
		type: "DELETED_KEBERATAN_DETAILS"
	}
}

