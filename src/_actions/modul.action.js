import {modulService} from '../_services';
import {history} from '../helpers';

export const modulActions = {
	getModul,
	getModulById,
	getModulByMenuId,
	getModulCount,
	onChangeModulProps,
	createModul,
	editModulInfo,
	deleteModulById
};

function getModul(){
	return dispatch => {
		let apiEndPoint = "";
		modulService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeModulsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getModulById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		modulService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editModulDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getModulByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		modulService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeModulsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getModulCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		modulService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeModulProps("countModul", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createModul(payload){
	return dispatch => {
		modulService.post(payload)
		.then((response)=>{
			dispatch(createModulInfo());
			history.push('/admin/modul');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeModulProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeModulProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeModulProps(props, event));
		}else{
			dispatch(handleOnChangeModulProps(props, event.target.value));
		}
	}
}

function editModulInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		modulService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedModulInfo());
			history.push('/admin/modul');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteModulById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		modulService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteModulDetails());
			dispatch(modulActions.getModul());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeModulsList(modul){
	return{
		type: "GET_ALL_MODUL",
		modul: modul
	}
}

export function handleOnChangeModulProps(props, value){
	return{
		type: "HANDLE_MODUL_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editModulDetails(modul){
	return{
		type: "MODUL_DETAIL",
		id: modul.id,
		kode: modul.kode,
		nama: modul.nama,
		created_at: modul.created_at,
		enabled: modul.enabled,
	}
}

export function updatedModulInfo(){
	return{
		type: "MODUL_UPDATED"
	}
}

export function createModulInfo(){
	return{
		type: "MODUL_CREATED_SUCCESSFULLY"
	}
}

export function deleteModulDetails(){
	return{
		type: "DELETED_MODUL_DETAILS"
	}
}

