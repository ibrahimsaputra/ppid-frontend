import {regulasiPostService} from '../_services';
import {history} from '../helpers';

export const regulasiPostActions = {
	getRegulasiPost,
	getRegulasiPostById,
	getRegulasiPostByMenuId,
	getRegulasiPostCount,
	onChangeRegulasiPostProps,
	createRegulasiPost,
	editRegulasiPostInfo,
	deleteRegulasiPostById
};

function getRegulasiPost(){
	return dispatch => {
		let apiEndPoint = "";
		regulasiPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeRegulasiPostsList(response.data));
		}
		).catch((err)=>{
			console.log(err);
		})
	}
}

function getRegulasiPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		regulasiPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(editRegulasiPostDetails(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getRegulasiPostByMenuId(id){
	return dispatch => {
		let apiEndPoint = "/bymenu/"+id;
		regulasiPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(changeRegulasiPostsList(response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function getRegulasiPostCount(){
	return dispatch => {
		let apiEndPoint = "/count";
		regulasiPostService.get(apiEndPoint)
		.then((response)=>{
			dispatch(handleOnChangeRegulasiPostProps("countRegulasiPost", response.data));
		}).catch((err)=>{
			console.log(err);
		})
	};
}

function createRegulasiPost(payload){
	return dispatch => {
		regulasiPostService.post(payload)
		.then((response)=>{
			dispatch(createRegulasiPostInfo());
			history.push('/admin/regulasipost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function onChangeRegulasiPostProps(props, event){
	return dispatch =>{
		if(props === "upload_ktp" || props === "thumbnail" || props === "link_pdf" || props === "link" || props==="photo"){
			dispatch(handleOnChangeRegulasiPostProps(props,event.target.files[0]));
		}else if(!event.target){//untuk react quill
			dispatch(handleOnChangeRegulasiPostProps(props, event));
		}else{
			dispatch(handleOnChangeRegulasiPostProps(props, event.target.value));
		}
	}
}

function editRegulasiPostInfo(id,payload){
	return dispatch => {
		let apiEndPoint = "/"+id;
		regulasiPostService.put(apiEndPoint,payload)
		.then((response)=>{
			dispatch(updatedRegulasiPostInfo());
			history.push('/admin/regulasipost');
		}).catch((err)=>{
			console.log(err);
		})
	}
}

function deleteRegulasiPostById(id){
	return dispatch => {
		let apiEndPoint = "/"+id;
		regulasiPostService.deleteById(apiEndPoint)
		.then((response)=>{
			dispatch(deleteRegulasiPostDetails());
			dispatch(regulasiPostActions.getRegulasiPost());
		}).catch((err)=>{
			console.log(err);
		})
	};
}

export function changeRegulasiPostsList(regulasiPost){
	return{
		type: "GET_ALL_REGULASIPOST",
		regulasiPost: regulasiPost
	}
}

export function handleOnChangeRegulasiPostProps(props, value){
	return{
		type: "HANDLE_REGULASIPOST_ON_CHANGE",
		props: props,
		value: value
	}
}

export function editRegulasiPostDetails(regulasiPost){
	return{
		type: "REGULASIPOST_DETAIL",
		id: regulasiPost.id,
		id_menu: regulasiPost.id_menu,
		judul: regulasiPost.judul,
		body: regulasiPost.body,
		thumbnail: regulasiPost.thumbnail,
		created_at: regulasiPost.created_at,
		updated_at: regulasiPost.updated_at,
		enabled: regulasiPost.enabled,
	}
}

export function updatedRegulasiPostInfo(){
	return{
		type: "REGULASIPOST_UPDATED"
	}
}

export function createRegulasiPostInfo(){
	return{
		type: "REGULASIPOST_CREATED_SUCCESSFULLY"
	}
}

export function deleteRegulasiPostDetails(){
	return{
		type: "DELETED_REGULASIPOST_DETAILS"
	}
}

