import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';
import { menu } from '../_reducers/menu.reducer';

function MegaSubmenu ({submenus,subcolumn,link}){
    const menus = submenus;
    let column = subcolumn;
    // console.log(menus);
    return ( 
        <ul className={"vertical-line-left megasubmenu-column"+column}>
            {menus.map((menu,i)=>{
                let subsub;
                if (menu.subMenus.length > 0){
                    subsub = <MegaSubmenu submenus={menu.subMenus} link={link+"/"+menu.id} subcolumn={column+1}/>;
                }
                return (<li key={i}>
                    <a href={`${link}/${menu.id}`}>
                        {(column===1)?<i className="fa"><FontAwesomeIcon icon={faAngleRight}/></i>:''}{menu.nama_menu.trim()}   
                    </a>
                    {subsub}
                </li>);
            })}
        </ul>
    );
}


export default MegaSubmenu;