import React,{useState}from 'react';
import Carousel from 'react-bootstrap/Carousel';

function CarouselText({text,key}) {
    const [index, setIndex] = useState(0);
  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };
    
    return (
      <Carousel activeIndex={index} onSelect={handleSelect} 
        controls={false} indicators={false} interval={10000}>
        {text.map((data,i)=>{
            return <Carousel.Item key={i}>
                <div className="infocontent">{data}</div>
            </Carousel.Item>;
        })}
      </Carousel>
    );
  }
  

  export default CarouselText;