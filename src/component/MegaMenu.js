import React from 'react';
import MegaSubmenu from './MegaSubmenu';
import {Link} from 'react-router-dom';


function MegaMenu ({menu}){ 
    let submenus;
    const link = "/"+menu.id;
   

    if (menu.subMenus.length > 0){
        submenus = <MegaSubmenu submenus={menu.subMenus} link={link} subcolumn={1}/>;
    }

    
    return (
        <li className="megamenu">
            <a href={link} >
            {menu.nama_menu}
            <span></span>
            </a>
            {console.log()}
            { menu.subMenus.length > 0 && <div className="megamenu-content topbar">
                <div className="row">
                <div className="megamenu-content-name">
                    <Link to={link}>
                        {menu.nama_menu.trim()}
                    </Link>
                </div>
                {submenus}   
                </div>
            </div>}
            
        </li>   
    );
}


export default MegaMenu;