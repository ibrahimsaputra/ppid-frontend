import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import logo from '../img/logo-ppid-kkp-white.png';
import Clock from '../component/Clock';
import { authActions } from '../_actions';

class AdminHeader extends Component{
    
    showNav = (navId)=>(event)=>{
      const nav = document.getElementById(navId);
      if(event.target.checked === true){
        nav.classList.add("side-nav-open")
      }else{
        nav.classList.remove("side-nav-open")
      }
      return;
    }

    render(){
        const {dispatch} = this.props;
        return (
            <header className="admin-header">
                <input type="checkbox" id="nav-toggle" onChange={this.showNav(this.props.navId)} className="nav-toggle"/>
                <label htmlFor="nav-toggle" className="nav-toggle-label">
                    <span></span>
                </label>
                <div className="admin-logo"><a href="/"><img className="admin-logo" src={logo} alt=""/></a></div>
                <Clock />
                <button className="btn btn-sm btn-flat btn-secondary mx-1" onClick={()=> dispatch(authActions.logout())}>Logout</button>
                
            </header>
        );
    }
}

AdminHeader.propTypes = {
};

function mapStateToProps(state){
    const {isAdmin} = state.authentication;
    return {isAdmin};
}
const connectedAdminHeader= withRouter(connect(mapStateToProps,
    null, null, {pure:false})(AdminHeader));

export {connectedAdminHeader as AdminHeader};