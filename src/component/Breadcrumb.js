import React from 'react';

function Breadcrumb({links}){
    // console.log(links)
    const crumbs = [...links];
    
    // console.log(0<crumbs.length-1);
    return (
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
                {crumbs.map((crumb,i)=>{
                    if (i<crumbs.length-1){
                        return <li className="breadcrumb-item" key={i}><a href={crumb.link}>{crumb.nama}</a></li>
                    }else{
                        return <li className="breadcrumb-item active" key={i}><a href={crumb.link}>{crumb.nama}</a></li>
                    }
                })}
            </ol>
        </nav>
    );
}
export default Breadcrumb;