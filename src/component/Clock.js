import React from 'react';

class Clock extends React.Component {
    
    constructor(props) {
      super(props);
      const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        let dateObj = new Date();
        let month = monthNames[dateObj.getMonth()];
        let day = String(dateObj.getDate()).padStart(2,'0');
        let year = dateObj.getFullYear();
        let time = dateObj.getTime;
        
        let output = day +'-'+ month+'-'+year+' '+time;
        this.state = {
                        time: output
                      };
        // console.log(this.props);
    }
    componentDidMount() {
      this.intervalID = setInterval(
        () => this.tick(),
        1000
      );
    }
    componentWillUnmount() {
      clearInterval(this.intervalID);
    }
    tick() {
        const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        let dateObj = new Date();
        let month = monthNames[dateObj.getMonth()];
        let day = String(dateObj.getDate()).padStart(2,'0');
        let year = dateObj.getFullYear();
        let hour = ("0"+dateObj.getHours()).slice(-2);
        let minute = ("0"+dateObj.getMinutes()).slice(-2);
        let second = ("0"+dateObj.getSeconds()).slice(-2);
        
        let time = hour+':'+minute+':'+second+' '
        let output = day +' - '+ month+' - '+year+' '+time;
      this.setState({
        time: output
      });
    }
    render() {
      return (
        <div className="app-clock">
           {this.state.time}
        </div>
      );
    }
}
export default Clock;