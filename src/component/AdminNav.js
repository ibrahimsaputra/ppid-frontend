import React, { Component } from 'react';
import { withRouter,Link} from 'react-router-dom';
import { connect } from 'react-redux';

const drawerWidth = 240;

class AdminNav extends Component{

    render(){
        if (document.getElementById("nav-toggle")){
          document.getElementById("nav-toggle").checked=false;
        }
        return (
          <nav id="admin-side-nav" className="admin-side-nav">
                    <ul>
                        <li><Link to='/admin/dashboard' className="active"><span>Dashboard</span></Link></li>
                        <li><Link to='/admin/permohonan'><span>Permohonan Info</span></Link></li>
                        <li><Link to='/admin/keberatan'><span>Keberatan</span></Link></li>
                        <li><Link to='/admin/dip'><span>e-DIP</span></Link></li>
                        <li><Link to='/admin/profil'><span>Profil PPID</span></Link></li>
                        <li >
                            <input type="checkbox" id="ul-toggle" className="ul-toggle"/> 
                            <label htmlFor="ul-toggle">Regulasi</label>
                            <ul>
                            <li><Link to='/admin/regulasi/sub'><span>Sub Regulasi</span></Link></li>
                            <li><Link to='/admin/regulasi'><span>Post Regulasi</span></Link></li>
                            </ul>
                        </li>
                        <li>
                            <input type="checkbox" id="infopb-toggle" className="ul-toggle"/>
                            <label htmlFor="infopb-toggle">Informasi Publik Berkala</label>
                            <ul>
                            <li><Link to='/admin/infopb/sub'><span>Sub Informasi Publik Berkala</span></Link></li>
                            <li><Link to='/admin/infopb'><span>Post Informasi Publik Berkala</span></Link></li>
                            </ul>
                        </li>
                        <li>
                            <input type="checkbox" id="infosm-toggle" className="ul-toggle"/>
                            <label htmlFor="infosm-toggle">Informasi Serta Merta</label>
                            <ul>
                            <li><Link to='/admin/infopsm/sub'><span>Sub Informasi Serta Merta</span></Link></li>
                            <li><Link to='/admin/infopsm'><span>Post Informasi Serta Merta</span></Link></li>
                            </ul>
                        </li>
                        <li>
                            <input type="checkbox" id="infoss-toggle" className="ul-toggle"/>
                            <label htmlFor="infoss-toggle">Informasi Setiap Saat</label>
                            <ul>
                              <li><Link to='/admin/infoptss/sub'><span>Sub Informasi Setiap Saat</span></Link></li>
                              <li><Link to='/admin/infoptss'><span>Post Informasi Setiap Saat</span></Link></li>
                            </ul>
                        </li>
                        <li>
                            <input type="checkbox" id="sl-toggle" className="ul-toggle"/>
                            <label htmlFor="sl-toggle">Standar Layanan</label>
                            <span></span>
                            <ul>
                            <li><Link to='/admin/stlayanan/sub'><span>Sub Standar Layanan</span></Link></li>
                            <li><Link to='/admin/stlayanan'><span>Post Standar Layanan</span></Link></li>
                            </ul>
                        </li>
                        <li><Link to='/admin/laplayanan'><span>Laporan Layanan</span></Link></li>
                    </ul>
                </nav>
        );
    }
}

AdminNav.propTypes = {
};

function mapStateToProps(state){
    const {isAdmin} = state.authentication;

    return {isAdmin};
}

const connectedAdminNav= withRouter(connect(mapStateToProps,
    null, null, {pure:false})(AdminNav));

export {connectedAdminNav as AdminNav};