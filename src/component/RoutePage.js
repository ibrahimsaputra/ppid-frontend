import React from 'react';
import {Route, Redirect, Router} from 'react-router-dom';
import {RegulasiPostPage, LapLayananPage, ProfilPage, InfopbPostPage, InfoptssPostPage, InfopsmPostPage, StlayananPostPage} from '../pages';


export const RoutePage =  ({component:Component, path,...rest})=>
(
    <Route {...rest} exact path={path} render ={(routerProps) =>{
        
        // return <RegulasiPage />
        const links = path.split('/');
        const id_menu = links[1];
        const id_post = links[links.length-1];
        let link = [];
        // console.log(path);
        switch(id_menu){
            case "1": 
                return <ProfilPage id_menu={id_post} link={[{nama:"Profil PPID", link:"/1"}]}/>
            case "2":
                return <RegulasiPostPage {...routerProps} id_menu={id_post} link={[{nama:"Regulasi", link:"/2"}]}/>
            case "3":
                return <div>
                        <Route  path = {path} render = {routerProps => 
                            <InfopbPostPage {...routerProps} id_menu={3} link={[{nama:"Informasi Publik Berkala", link:"/3"}]}/>
                        }/>
                        <Route  path = {path+"/4"} render = {routerProps => 
                            <InfopbPostPage {...routerProps} id_menu={4} link={[{nama:"Informasi Publik Berkala", link:"/3/4"}]}/>
                        }/>
                        <Route path = {path+"/5"} render = {routerProps => 
                            <InfoptssPostPage {...routerProps} id_menu={5} link={[{nama:"Informasi Publik Berkala", link:"/3/5"}]}/>
                        }/>
                        <Route path = {path+"/6"} render = {routerProps => 
                            <InfopsmPostPage {...routerProps} id_menu={6} link={[{nama:"Informasi Publik Serta Merta", link:"/3/6"}]}/>
                        }/>
                     </div>
            case "7":
                return <StlayananPostPage {...routerProps} id_menu={id_post} link={[{nama:"Standar Layanan", link:"/7"}]}/>
            case "8":
                return <LapLayananPage {...routerProps} id_menu={id_post} link={[{nama:"Laporan Layanan", link:"/8"}]}/>
            default:
                return <Redirect to="/"/>;
        }
    }}/>);