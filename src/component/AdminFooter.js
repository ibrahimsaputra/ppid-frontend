import React from 'react';

function AdminFooter(){
    return (
        <div className="admin-footer">
            <div className="copyrightbar">
                <p> © 2020 Kantor Kesehatan Pelabuhan Kelas II Padang   Supported by PAM-TECHNO</p>
            </div>
        </div>
    );
}
export {AdminFooter};