import React,{useState}from 'react';
import Carousel from 'react-bootstrap/Carousel';

function CarouselImage({contents}) {
    const [index, setIndex] = useState(0);
  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };
    
    return (
      <Carousel activeIndex={index} onSelect={handleSelect} 
        indicators={false} interval={5000}>
        {contents.map((content,i)=>{
            return <Carousel.Item key={i}>
                <img className="carousel-image"
                  src={content.url}
                  alt="First slide"
                />
                <Carousel.Caption>
                  <div className="carousel-image-caption">
                    <h3 className="carousel-image-title">{content.title}</h3>
                    <div className="carousel-image-body">{content.body}</div>
                  </div>
                </Carousel.Caption>
            </Carousel.Item>;
        })}
      </Carousel>
    );
  }
  

  export default CarouselImage;