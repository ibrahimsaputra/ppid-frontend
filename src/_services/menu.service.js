import http, { Authorization } from "../http-common";
const API_URL = "/menu";

export const menuService = {
	get,
	post,
	put,
	deleteById
}

function get(apiEndPoint){
	return http.get(API_URL+apiEndPoint);
}
function post(payload){
	const formData = new FormData();
		for (const key in payload) {
			if (payload.hasOwnProperty(key)) {
				formData.append(key,payload[key]);
			}
			console.log(key);
		}
	return http.post(API_URL,formData,{
		headers:{
			"Content-Type": "multipart/form-data",
			"Authorization" : Authorization,
		}
	})
}
function put(apiEndPoint,payload){
	const formData = new FormData();
		for (const key in payload) {
			if (payload.hasOwnProperty(key)) {
				formData.append(key,payload[key]);
			}
			console.log(key);
		}
	return http.put(API_URL+apiEndPoint,formData,{
		headers:{
			"Content-Type": "multipart/form-data",
			"Authorization" : Authorization,
		}
	})
}
function deleteById(apiEndPoint){
	return http.delete(API_URL+apiEndPoint);
}

