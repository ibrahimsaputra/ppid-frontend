import http from "../http-common";
import {authAction} from '../_actions'
const API_URL = "/api";


export const authService = {
  login,
  logout,
  register,
  getCurrentUser
}
  function login(username, password) {
    return http
      .post("/login", {
        username,
        password
      })
      .then(response => {
        if (response.data.status === "success"){
          console.log("login success")
          localStorage.setItem("user", JSON.stringify(response.data.data));
        }
        // if (response.data.data.accessToken) {
        //   localStorage.setItem("user", JSON.stringify(response.data));
        // }else if (response.data.data.username){
        //   localStorage.setItem("user", JSON.stringify(response.data));
        // }

        return response.data;
      });
  }

  function logout() {
    localStorage.removeItem("user");
  }

  function register(username, email, password) {
    return http.post(API_URL + "/signup", {
      username,
      email,
      password
    });
  }

  function getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }

