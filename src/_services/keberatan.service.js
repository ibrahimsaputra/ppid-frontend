import http from "../http-common";
const API_URL = "/keberatan";

export const keberatanService = {
	get,
	post,
	put,
	deleteById
}

function get(apiEndPoint){
	return http.get(API_URL+apiEndPoint);
}
function post(payload){
	const formData = new FormData();
		for (const key in payload) {
			if (payload.hasOwnProperty(key)) {
				formData.append(key,payload[key]);
			}
		}
	return http.post(API_URL,formData,{
		headers:{
			"Content-Type": "multipart/form-data",
		}
	})
}
function put(apiEndPoint,payload){
	const formData = new FormData();
		for (const key in payload) {
			if (payload.hasOwnProperty(key)) {
				formData.append(key,payload[key]);
			}
		}
	return http.put(API_URL+apiEndPoint,formData,{
		headers:{
			"Content-Type": "multipart/form-data",
		}
	})
}
function deleteById(apiEndPoint){
	return http.delete(API_URL+apiEndPoint);
}

