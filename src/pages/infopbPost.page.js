import React, { Component } from 'react';
import Breadcrumb from '../component/Breadcrumb';
import {web,infopbPostActions} from '../_actions';
import {withRouter, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {faFilePdf} from '@fortawesome/free-solid-svg-icons/faFilePdf';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Markup} from 'interweave';
import {dttodate} from '../helpers';
import {MDBDataTable} from 'mdbreact';

class InfopbPostPage extends Component {

  constructor(props){
      super(props);
      this.state = {
        post:{}
      };
      const {match} = this.props;
      const {dispatch} = this.props;
      if (this.props.isAdmin) dispatch(web());
      if (match.isExact && window.location.pathname === match.url){
        const link = match.url.split("/");
        dispatch(infopbPostActions.getInfopbPostByMenuId(parseInt(link[link.length-1])));
        if(!this.props.infopbPost.length){
            // this.props.history.push("/");
        }
      }
  }
  
  render(){
       const {match} = this.props;
       let data =[];
       let posts=[];
       let tipe="";
       let nama_menu="";
       let menu={};//check
       const link = match.url.split("/");
        
        if (this.props.id_menu){
          menu = this.props.menus.find(element=>element.id === 3).subMenus.find(element=>element.id === parseInt(this.props.id_menu));
        }else if(this.props.menu) {
          menu = this.props.menu.find(element=>element.id === parseInt(link[link.length-1]))
        }
        
        if (match.isExact){
          posts = this.props.infopbPost;
          if (menu){
            tipe = menu.tipe;
            nama_menu = menu.nama_menu;
          }
          if (tipe === "list pdf"){
            let rows=[];
            posts.forEach((element,i) => {
              rows.push({
                deskripsi:element.judul,
                link: <a href ={element.thumbnail} download><FontAwesomeIcon icon={faFilePdf} />  Download File Terkait</a>
              })            
            });
            data = {
              columns: [
                {
                  label: 'Deskripsi',
                  field: 'deskripsi',
                  sort: 'asc',
                  width: 270
                },
                {
                  label: 'Link',
                  field: 'link',
                  sort: 'asc',
                  width: 200
                }
              ],
              rows: rows
            }
          }
        }
      // console.log(tipe)
       return (
      <div className="bodier">
        {match.isExact && 
          (
            (tipe === "post" && 
              (
                ((this.state.post.hasOwnProperty("id")) && 
                  <div className="bodier" key={this.state.post.id}>
                    {this.state.post.thumbnail.trim()?<img className="post-thumbnail" src={this.state.post.thumbnail} alt="thumbnail"/>:''}
                    <Breadcrumb links={this.props.link}/>
                    <div className="post-container">
                        
                        <div className="post-head">
                          <div className="post-title">{this.state.post.judul}</div>
                          <div className="post-time">
                              {dttodate(this.state.post.created_at)}
                          </div>
                        </div>
                        
                        <div className="post-body">
                          <Markup content={this.state.post.body}/>
                        </div>
                        {/* {this.state.post.link_pdf.trim()?
                          <div className="post-pdf">
                              <FontAwesomeIcon icon={faFilePdf} /> <a href ={this.state.post.link_pdf} download>Download File Terkait</a>
                          </div>
                          :''} */}
                    </div>         
                </div>
                ) 
              || (posts.length>0 &&  // show card list of post
                  <div className="bodier">
                  <Breadcrumb links={this.props.link}/>
                  <div className="post-container">
                      <div className="post-head">
                        <div className="post-title">{nama_menu}</div>
                      </div>
                      <div className="post-card-container ">{posts.map(post=>
                        <div className="card post-card" style={{width: 18+'rem'}} key={post.id}>
                          <img src={post.thumbnail} className="card-img-top" alt=""/>
                          <div className="card-body">
                            <h5 className="card-title">{post.judul}</h5>
                            <div className="card-text"><Markup content={post.body}/></div>
                            <button onClick={() => this.setState({post:post})} className="btn btn-primary">Baca Lebih Lanjut</button>
                          </div>
                        </div>)}
                      </div>
                  </div>
              </div>
                )
              )
            )
            ||(tipe === "list pdf"  && 
              <div className="bodier" >
              <Breadcrumb links={this.props.link}/>
              <div className="post-container">
                  <div className="post-head">
                    <div className="post-title">{nama_menu}</div>
                  </div>
                  
                  <div className="post-body">
                  <MDBDataTable btn
                    striped
                    bordered
                    small
                    data={data}
                    sorting={"true"}
                  />
  
                  </div>
                  {/* {posts[0].link_pdf.trim()?
                    <div className="post-pdf">
                        <FontAwesomeIcon icon={faFilePdf} /> <a href ={posts[0].link_pdf} download>Download File Terkait</a>
                    </div>
                    :''} */}
              </div>         
          </div>
            )
          )
        }
        <Route path={`${match.url}/:id_menu`} render={withRouter(connect(mapStateToProps, //check
          null, null, {pure:false})((routerProps)=><InfopbPostPage {...routerProps} 
          menu={menu.subMenus} link={[...this.props.link,{nama:nama_menu,link:match.url}]}/>))}/>
      </div>
  );
  }
}

// InfopbPostPage.propTypes = {
//     classes: PropTypes.object.isRequired
// };

function mapStateToProps(state){
    const {isAdmin} = state.authentication;
    const {infopbPost} = state.infopbPost;
    const menus = state.menu.menu;
    return {isAdmin,infopbPost,menus};
}

const connectedInfopbPostPage= withRouter(connect(mapStateToProps,
    null, null, {pure:false})(InfopbPostPage));

export {connectedInfopbPostPage as InfopbPostPage};
