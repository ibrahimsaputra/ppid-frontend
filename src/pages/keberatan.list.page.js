import React,{Component} from 'react';
import {admin,keberatanActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';
import {dttodate} from '../helpers';

class KeberatanListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(keberatanActions.getKeberatan());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(keberatanActions.deleteKeberatanById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let keberatans=this.props.keberatan || [];

                      // get data for list laporan layanan
                      let rows=[];
                      keberatans.forEach((keberatan,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													nama_pemohon:keberatan.nama_pemohon,
													email:keberatan.email,
													alamat:keberatan.alamat,
													nohp:keberatan.nohp,
													pil_keberatan:keberatan.pil_keberatan,
													als_keberatan:keberatan.als_keberatan,
													als_lain:keberatan.als_lain,
													status:keberatan.status || "belum dibaca",
													created_at:dttodate(keberatan.created_at),
				action:
                          <>
                            <a href ={`${match.url}/ubah/${keberatan.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(keberatan.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Nama Pemohon',
                            field: 'nama_pemohon',
                            sort: 'asc'
                          },
													{
                            label: 'Email',
                            field: 'email',
                            sort: 'asc'
                          },
													{
                            label: 'Nohp',
                            field: 'nohp',
                            sort: 'asc'
                          },
													{
                            label: 'Pilihan Keberatan',
                            field: 'pil_keberatan',
                            sort: 'asc'
                          },
													{
                            label: 'Alasan Keberatan',
                            field: 'als_keberatan',
                            sort: 'asc'
                          },
													{
                            label: 'Status',
                            field: 'status',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Keberatan </div>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {keberatan} = state.keberatan;
                const {menu} = state.menu;
                return {isAdmin,menu,keberatan};
            }

            const connectedKeberatanListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(KeberatanListPage));

            export {connectedKeberatanListPage as KeberatanListPage};

