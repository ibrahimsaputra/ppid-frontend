import React,{Component} from "react";
import {permohonanActions,admin, web} from "../_actions";
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types'; 
import {tipePemohon,tujuanPermohonan,kategoriInformasi,detailWNI,bentukFormat, caraPeroleh, statusSuratMasuk} from '../_utility/appstaticdata';
import noimage from '../img/no_image.jpg';
import { AdminNav } from "../component";
class PermohonanAddPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            loading:false,
            resMessage:{}
        };
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        const {match:{params},dispatch,isAdmin} = this.props;
        if (isAdmin) dispatch(web());
        console.log(isAdmin)
        if(params.id){
            dispatch(admin())
            dispatch(permohonanActions.getPermohonanById(params.id))
        }
    }
    readURL(input,foto) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
              foto.setAttribute('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }

    ValidateSingleInput(oInput) {
        const _validFileExtensions = [".jpg", ".jpeg", ".pdf", ".png"];    
        if (oInput.type === "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    return false;
                }
            }
        }
        return true;
    }

    handleChange = prop => event => {
        const {dispatch} = this.props;
        // console.log(prop,event.target.value);
        if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
            if (prop === "upload_ktp" || prop === "thumbnail"|| prop==="photo"){
                const image = document.getElementById(prop);
                this.readURL(event.target,image);
            }
            if (this.ValidateSingleInput(event.target)) dispatch(permohonanActions.onChangePermohonanProps(prop,event));            

        }else{
            dispatch(permohonanActions.onChangePermohonanProps(prop, event));
        } //change
    };

    handleValidation (){//add
        let isValid = true;
        let errors = {};
        /*
        Beda setiap page
        Untuk field yang tidak wajib isi di deconstruct dari object;
        */
        const {permohonan,id,created_at,updated_at,status,countPermohonan,currentFile,selectedFiles,...fields} = this.props.permohonan; 
        
        for (const key in fields) {
            if (fields.hasOwnProperty(key)) {
                if((!fields[key] || fields[key] === undefined) && !(this.props.match.params.id && key === "upload_ktp")){
                    
                    isValid=false;
                    errors[key]="Wajib Isi";
                    
                }
               
                /**
                * Untuk Field dengan Spesifikasi Khusus
                */
                if(typeof fields["nama_pemohon"] !== "undefined"){
                    if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                    isValid = false;
                    errors["nama_pemohon"] = "Hanya Huruf";
                    }        
                }

                if(typeof fields["email"] !== "undefined"){
                    let lastAtPos = fields["email"].lastIndexOf('@');
                    let lastDotPos = fields["email"].lastIndexOf('.');
     
                    if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                        isValid = false;
                        errors["email"] = "Email Tidak valid";
                        }
                }
               
            }
        }
        console.log(errors);
        this.setState({
            resMessage: errors
        })
        
        return isValid;
    }

    handleFormSubmit = event =>{
        event.preventDefault();
        event.stopPropagation();
        this.setState({
            loading:true
        });

        const {match:{params}}=this.props;
        const {dispatch} =this.props;
        const {permohonan}=this.props;
        if (this.handleValidation()){
            let payload={
                nama_pemohon: permohonan.nama_pemohon,
				email: permohonan.email,
				noktp: permohonan.noktp,
				alamat: permohonan.alamat,
				nohp: permohonan.nohp,
				tujuan: permohonan.tujuan,
				tipe_pemohon: permohonan.tipe_pemohon,
				detail_wni: permohonan.detail_wni,
				info_kategori: permohonan.info_kategori,
				info_diminta: permohonan.info_diminta,
				cara: permohonan.cara,
				bentuk: permohonan.bentuk,
				alasan: permohonan.alasan,
				kegunaan: permohonan.kegunaan,
                status: permohonan.status,
                upload_ktp: permohonan.upload_ktp,
            }
            if(params.id){
                dispatch(permohonanActions.editPermohonanInfo(params.id,payload))
                alert("Form Submitted");
                this.props.history.push("/admin/permohonan");
            }else{
                dispatch(permohonanActions.createPermohonan(payload))
                alert("Form Data Telah Dikirim Kepada "+permohonan.tujuan);
                this.props.history.push("/");
            }
        }else{
            this.setState({
                loading:false
            })
            alert("Form has error");
        }
    }

    render(){
        const {permohonan} = this.props;
        console.log(permohonan);
        const {match:{params}} = this.props;
        return(
            <div className="bodier content">
                 <div className="post-container content-container">
                        <div className="post-head">
                          <div className="post-title">{this.props.match.params.id ? "Update" : ""} Permohonan Informasi</div>
                        </div>
                        <div className="post-body">
                            Silahkan isi Form berikut untuk melakukan permohonan informasi
                            <form action="" encType="multipart/form-data">
                                
                                <fieldset>
                                    <legend>Identitas Pemohon</legend>
                                    <div className="form-group">
                                        <label htmlFor="nama_pemohon">Nama</label>
                                        <input id='nama_pemohon' name="nama_pemohon" type="text" className={`form-control ${(this.state.resMessage['nama_pemohon'])?'is-invalid':''}`} value={permohonan['nama_pemohon']} onChange={this.handleChange('nama_pemohon')} required/>
                                        <div className={`feedback ${(this.state.resMessage['nama_pemohon'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input id='email' name="email" type="email" className={`form-control ${(this.state.resMessage['email'])?'is-invalid':''}`} value={permohonan['email']} onChange={this.handleChange('email')} required/>
                                        <div className={`feedback ${(this.state.resMessage['email'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="noktp">No KTP</label>
                                        <input id='noktp' name="noktp" type="text" className={`form-control ${(this.state.resMessage['noktp'])?'is-invalid':''}`} value={permohonan['noktp']} onChange={this.handleChange('noktp')} required/>
                                        <div className={`feedback ${(this.state.resMessage['noktp'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="alamat">Alamat</label>
                                        <textarea id='alamat' name="alamat"  className={`form-control ${(this.state.resMessage['alamat'])?'is-invalid':''}`} value={permohonan['alamat']} onChange={this.handleChange('alamat')} required/>
                                        <div className={`feedback ${(this.state.resMessage['alamat'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="nohp">No HP</label>
                                        <input id='nohp' name="nohp" type="number" className={`form-control ${(this.state.resMessage['nohp'])?'is-invalid':''}`} value={permohonan['nohp']} onChange={this.handleChange('nohp')} required/>
                                        <div className={`feedback ${(this.state.resMessage['nohp'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Data Informasi Diminta</legend>
                                    <div className="form-group">
                                        <label htmlFor="tujuan">Tujuan</label>
                                        <select id='tujuan' name="tujuan" type="text" 
                                        className={`form-control ${(this.state.resMessage['tujuan'])?'is-invalid':''}`} 
                                        placeholder={tujuanPermohonan(0)} value={permohonan['tujuan']} 
                                        onChange={this.handleChange('tujuan')} required disabled={params.id?true:false}>
                                                            <option value="">-Pilih-</option> 
                                            {tujuanPermohonan().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                        </select>
                                        <div className={`feedback ${(this.state.resMessage['tujuan'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="tipe_pemohon">Tipe Pemohon</label>
                                        <select id='tipe_pemohon' name="tipe_pemohon" type="text" 
                                        className={`form-control ${(this.state.resMessage['tipe_pemohon'])?'is-invalid':''}`} 
                                        placeholder={tipePemohon(0)} value={permohonan['tipe_pemohon']} 
                                        onChange={this.handleChange('tipe_pemohon')} required disabled={params.id?true:false}>
                                                            <option value="">-Pilih-</option> 
                                            {tipePemohon().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                        </select>
                                        <div className={`feedback ${(this.state.resMessage['tipe_pemohon'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="detail_wni">Detail WNI</label>
                                        <select id='detail_wni' name="detail_wni" type="text" 
                                        className={`form-control ${(this.state.resMessage['detail_wni'])?'is-invalid':''}`} 
                                        placeholder={detailWNI(0)} value={permohonan['detail_wni']} 
                                        onChange={this.handleChange('detail_wni')} required disabled={params.id?true:false}
                                        >
                                                            <option value="">-Pilih-</option> 
                                            {detailWNI().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                        </select>
                                        <div className={`feedback ${(this.state.resMessage['detail_wni'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="upload_ktp">Upload KTP</label>
                                        <img id="upload_ktp" src={permohonan['upload_ktp']?permohonan['upload_ktp']:noimage} className="mb-2" width={200+"px"} alt="upload_ktp"/>
                                        <input type="file" id='upload_ktp' name="upload_ktp" 
                                        className={`form-control ${(this.state.resMessage['upload_ktp'])?'is-invalid':''}`} 
                                        onChange={this.handleChange('upload_ktp')} required
                                        disabled={params.id?true:false}
                                        />
                                        <div className={` ${(this.state.resMessage['upload_ktp'])?'invalid-':''}feedback`}>
                                            *Wajib isi *.pdf .jpeg .png ; *Max:2MB
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="info_kategori">Kategori Informasi</label>
                                        <select id='info_kategori' name="info_kategori" 
                                        className={`form-control ${(this.state.resMessage['info_kategori'])?'is-invalid':''}`} 
                                        placeholder={kategoriInformasi(0)} value={permohonan['info_kategori']} 
                                        onChange={this.handleChange('info_kategori')} required disabled={params.id?true:false}>
                                                            <option value="">-Pilih-</option> 
                                            {kategoriInformasi().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                        </select>
                                        <div className={`feedback ${(this.state.resMessage['info_kategori'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="info_diminta">Informasi Yang  Diminta</label>
                                        <textarea id='info_diminta' name="info_diminta" 
                                        className={`form-control ${(this.state.resMessage['info_diminta'])?'is-invalid':''}`} 
                                        value={permohonan['info_diminta']} onChange={this.handleChange('info_diminta')} required
                                        disabled={params.id?true:false}
                                        />
                                        <div className={`feedback ${(this.state.resMessage['info_diminta'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="cara">Cara Memperoleh</label>
                                        <select id='cara' name="cara"  
                                        className={`form-control ${(this.state.resMessage['cara'])?'is-invalid':''}`} 
                                        placeholder={caraPeroleh(0)} value={permohonan['cara']} 
                                        onChange={this.handleChange('cara')} required disabled={params.id?true:false}
                                        >
                                            <option value="">-Pilih-</option>
                                            {caraPeroleh().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                        </select>
                                        <div className={`feedback ${(this.state.resMessage['cara'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="bentuk">Bentuk Informasi</label>
                                        <select id='bentuk' name="bentuk" type="text" 
                                        className={`form-control ${(this.state.resMessage['bentuk'])?'is-invalid':''}`} 
                                        placeholder={bentukFormat(0)} value={permohonan['bentuk']} 
                                        onChange={this.handleChange('bentuk')} required disabled={params.id?true:false}
                                        >
                                                            <option value="">-Pilih-</option> 
                                            {bentukFormat().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                        </select>
                                        <div className={`feedback ${(this.state.resMessage['bentuk'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="alasan">Alasan Permintaan Informasi</label>
                                        <textarea id='alasan' name="alasan" type="text" 
                                        className={`form-control ${(this.state.resMessage['alasan'])?'is-invalid':''}`} 
                                        value={permohonan['alasan']} onChange={this.handleChange('alasan')} required disabled={params.id?true:false}
                                        />
                                        <div className={`feedback ${(this.state.resMessage['alasan'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="kegunaan">Tujuan/Kegunaan Permintaan Informasi</label>
                                        <textarea id='kegunaan' name="kegunaan" type="text" 
                                        className={`form-control ${(this.state.resMessage['kegunaan'])?'is-invalid':''}`} 
                                        value={permohonan['kegunaan']} onChange={this.handleChange('kegunaan')} require disabled={params.id?true:false}
                                        />
                                        <div className={`feedback ${(this.state.resMessage['kegunaan'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                </fieldset>
                                {params.id && <div className="form-group">
                                    <label htmlFor="status">Status</label>
                                    <select id='status' name="status" type="text" 
                                        className={`form-control ${(this.state.resMessage['status'])?'is-invalid':''}`} 
                                        placeholder={statusSuratMasuk[0]} value={permohonan['status']} 
                                        onChange={this.handleChange('status')} required
                                    >
                                                            <option value="">-Pilih-</option> 
                                        {statusSuratMasuk().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                    </select>
                                    <div className={`feedback ${(this.state.resMessage['status'])?'invalid-feedback':''}`}>
                                        *Wajib isi
                                    </div>
                                </div>}
                                <button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                            </form>
                            
                        </div>
                    </div>
            {params.id &&<AdminNav/>}
            </div>
        );
    }

}

PermohonanAddPage.propTypes = {
    permohonan: PropTypes.object.isRequired,
};
const mapStateToProps = (state) =>{
    const { loggingIn,isAdmin } = state.authentication;
    const permohonan = state.permohonan;

    return {
       loggingIn,isAdmin,permohonan
    };
}
const connectedPermohonanAddPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(PermohonanAddPage));

export {connectedPermohonanAddPage as PermohonanAddPage};

