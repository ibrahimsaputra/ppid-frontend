import React,{Component} from 'react';
import {modulActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { AdminNav } from '../component';

class ModulAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{}
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(modulActions.getModulById(params.id))
                }
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
                    dispatch(modulActions.onChangeModulProps('selectedFile',event));
                }else{
                    dispatch(modulActions.onChangeModulProps(prop, event));
                } //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {modul,id,created_at,updated_at,...fields} = this.props.modul;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {modul} =this.props;
                if (this.handleValidate()){

                    const payload={
				id:modul.id,
				kode:modul.kode,
				nama:modul.nama,
				created_at:modul.created_at,
				enabled:modul.enabled,
			}
                        if(params.id){
                            dispatch(modulActions.editModulInfo(params.id,payload))
                        }else{
                            dispatch(modulActions.createModul(payload))
                        }
                        alert("Form Submitted")
                    }else{
                        this.setState({
                            loading:false
                        })
                        alert("Form has error")
                    }
                }

                render(){
                    const {modul} = this.props;
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">Modul</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					<div className="form-group">
                                                    <label htmlFor="id">Id</label>
                                                    <input id='id' name="id" type="text" className={`form-control ${(this.state.resMessage['id'])?'is-invalid':''}`} value={modul['id']} onChange={this.handleChange('id')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="kode">Kode</label>
                                                    <input id='kode' name="kode" type="text" className={`form-control ${(this.state.resMessage['kode'])?'is-invalid':''}`} value={modul['kode']} onChange={this.handleChange('kode')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['kode'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="nama">Nama</label>
                                                    <input id='nama' name="nama" type="text" className={`form-control ${(this.state.resMessage['nama'])?'is-invalid':''}`} value={modul['nama']} onChange={this.handleChange('nama')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['nama'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="created_at">Created At</label>
                                                    <input id='created_at' name="created_at" type="text" className={`form-control ${(this.state.resMessage['created_at'])?'is-invalid':''}`} value={modul['created_at']} onChange={this.handleChange('created_at')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['created_at'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="enabled">Enabled</label>
                                                    <input id='enabled' name="enabled" type="text" className={`form-control ${(this.state.resMessage['enabled'])?'is-invalid':''}`} value={modul['enabled']} onChange={this.handleChange('enabled')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['enabled'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            ModulAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const modul = state.modul;

                return {
                isAdmin,modul
                };
            }
            const connectedModulAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(ModulAddPage));

            export { connectedModulAddPage as ModulAddPage };

