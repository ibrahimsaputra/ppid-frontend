import React,{Component} from 'react';
import {admin,menuActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';

class MenuListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(menuActions.getMenu());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(menuActions.deleteMenuById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let menus=this.props.menu;

                      // get data for list laporan layanan
                      let rows=[];
                      menus.forEach((menu,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													id_parent:menu.id_parent,
													nama_menu:menu.nama_menu,
													tipe:menu.tipe,
													link_icon:menu.link_icon,
													enabled:menu.enabled,
													created_at:menu.created_at,
													updated_at:menu.updated_at,
				action:
                          <>
                            <a href ={`${match.url}/${menu.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(menu.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Id Parent',
                            field: 'id_parent',
                            sort: 'asc'
                          },
													{
                            label: 'Nama Menu',
                            field: 'nama_menu',
                            sort: 'asc'
                          },
													{
                            label: 'Tipe',
                            field: 'tipe',
                            sort: 'asc'
                          },
													{
                            label: 'Link Icon',
                            field: 'link_icon',
                            sort: 'asc'
                          },
													{
                            label: 'Enabled',
                            field: 'enabled',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
													{
                            label: 'Updated At',
                            field: 'updated_at',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Menu </div>
                            <a href="/admin/menu/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {menu} = state.menu;
                return {isAdmin,menu};
            }

            const connectedMenuListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(MenuListPage));

            export {connectedMenuListPage as MenuListPage};

