import React,{Component} from 'react';
import {admin,modulAksesActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';

class ModulAksesListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(modulAksesActions.getModulAkses());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(modulAksesActions.deleteModulAksesById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let modulAksess=this.props.modulAkses;

                      // get data for list laporan layanan
                      let rows=[];
                      modulAksess.forEach((modulAkses,i) => {
                        // console.log(i);
                        rows.push({
                          no:i,
													id_level:modulAkses.id_level,
													id_modul:modulAkses.id_modul,
													l:modulAkses.l,
													d:modulAkses.d,
													t:modulAkses.t,
													u:modulAkses.u,
													h:modulAkses.h,
				action:
                          <>
                            <a href ={`${match.url}/${modulAkses.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(modulAkses.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Id Level',
                            field: 'id_level',
                            sort: 'asc'
                          },
													{
                            label: 'Id Modul',
                            field: 'id_modul',
                            sort: 'asc'
                          },
													{
                            label: 'L',
                            field: 'l',
                            sort: 'asc'
                          },
													{
                            label: 'D',
                            field: 'd',
                            sort: 'asc'
                          },
													{
                            label: 'T',
                            field: 't',
                            sort: 'asc'
                          },
													{
                            label: 'U',
                            field: 'u',
                            sort: 'asc'
                          },
													{
                            label: 'H',
                            field: 'h',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data ModulAkses </div>
                            <a href="/admin/modulakses/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {modulAkses} = state.modulAkses;
                const {menu} = state.menu;
                return {isAdmin,menu,modulAkses};
            }

            const connectedModulAksesListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(ModulAksesListPage));

            export {connectedModulAksesListPage as ModulAksesListPage};

