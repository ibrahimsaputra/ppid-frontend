import React,{Component} from 'react';
import {userActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { AdminNav } from '../component';

class UserAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{}
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(userActions.getUserById(params.id))
                }
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
                    dispatch(userActions.onChangeUserProps('selectedFile',event));
                }else{
                    dispatch(userActions.onChangeUserProps(prop, event));
                } //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {user,id,created_at,updated_at,...fields} = this.props.user;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {user} =this.props;
                if (this.handleValidate()){

                    const payload={
				id:user.id,
				id_level:user.id_level,
				username:user.username,
				password:user.password,
				created_at:user.created_at,
				updated_at:user.updated_at,
				enabled:user.enabled,
			}
                        if(params.id){
                            dispatch(userActions.editUserInfo(params.id,payload))
                        }else{
                            dispatch(userActions.createUser(payload))
                        }
                        alert("Form Submitted")
                    }else{
                        this.setState({
                            loading:false
                        })
                        alert("Form has error")
                    }
                }

                render(){
                    const {user} = this.props;
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">User</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					<div className="form-group">
                                                    <label htmlFor="id">Id</label>
                                                    <input id='id' name="id" type="text" className={`form-control ${(this.state.resMessage['id'])?'is-invalid':''}`} value={user['id']} onChange={this.handleChange('id')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="id_level">Id Level</label>
                                                    <input id='id_level' name="id_level" type="text" className={`form-control ${(this.state.resMessage['id_level'])?'is-invalid':''}`} value={user['id_level']} onChange={this.handleChange('id_level')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id_level'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="username">Username</label>
                                                    <input id='username' name="username" type="text" className={`form-control ${(this.state.resMessage['username'])?'is-invalid':''}`} value={user['username']} onChange={this.handleChange('username')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['username'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="password">Password</label>
                                                    <input id='password' name="password" type="text" className={`form-control ${(this.state.resMessage['password'])?'is-invalid':''}`} value={user['password']} onChange={this.handleChange('password')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['password'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="created_at">Created At</label>
                                                    <input id='created_at' name="created_at" type="text" className={`form-control ${(this.state.resMessage['created_at'])?'is-invalid':''}`} value={user['created_at']} onChange={this.handleChange('created_at')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['created_at'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="updated_at">Updated At</label>
                                                    <input id='updated_at' name="updated_at" type="text" className={`form-control ${(this.state.resMessage['updated_at'])?'is-invalid':''}`} value={user['updated_at']} onChange={this.handleChange('updated_at')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['updated_at'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="enabled">Enabled</label>
                                                    <input id='enabled' name="enabled" type="text" className={`form-control ${(this.state.resMessage['enabled'])?'is-invalid':''}`} value={user['enabled']} onChange={this.handleChange('enabled')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['enabled'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            UserAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const user = state.user;

                return {
                isAdmin,user
                };
            }
            const connectedUserAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(UserAddPage));

            export { connectedUserAddPage as UserAddPage };

