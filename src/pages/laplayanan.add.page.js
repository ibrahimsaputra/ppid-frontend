import React,{Component} from 'react';
import {laplayananActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { AdminNav } from '../component';

class LaplayananAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{}
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(laplayananActions.getLaplayananById(params.id))
                }
            }

           ValidateSingleInput(oInput) {
                const _validFileExtensions = [".jpg", ".jpeg", ".pdf", ".png"];    
                if (oInput.type === "file") {
                    var sFileName = oInput.value;
                    
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() === sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        
                        if (!blnValid) {
                            return false;
                        }
                    }
                }
                return true;
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop === "link" || prop==="photo"){
                    if (this.ValidateSingleInput(event.target)) dispatch(laplayananActions.onChangeLaplayananProps(prop,event));
                }else{
                    dispatch(laplayananActions.onChangeLaplayananProps(prop, event));
                } //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {laplayanan,id,created_at,updated_at,link_pdf,enabled,currentFile,selectedFiles,countLaplayanan,...fields} = this.props.laplayanan;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {laplayanan} =this.props;
                if (this.handleValidate()){

                    const payload={
				tahun:laplayanan.tahun,
				link:laplayanan.link,
			}
                        if(params.id){
                            dispatch(laplayananActions.editLaplayananInfo(params.id,payload))
                        }else{
                            dispatch(laplayananActions.createLaplayanan(payload))
                        }
                        alert("Form Submitted")
                        this.props.history.push("/admin/laplayanan");
                    }else{
                        this.setState({
                            loading:false
                        })
                        alert("Form has error")
                    }
                }

                render(){
                    const {laplayanan,match:{params}} = this.props;
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">{this.props.match.params.id ? "Ubah" : "Tambah"} Laplayanan</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					
                         					<div className="form-group">
                                                    <label htmlFor="tahun">Tahun</label>
                                                    <input id='tahun' name="tahun" type="text" className={`form-control ${(this.state.resMessage['tahun'])?'is-invalid':''}`} value={laplayanan['tahun']} onChange={this.handleChange('tahun')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['tahun'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="link">Link File </label>
                                                        <input id='link' name="link" type="file" className={`form-control ${(this.state.resMessage['link'])?'is-invalid':''}`} onChange={this.handleChange('link')} required/>
                                                    <div className={` ${(this.state.resMessage['link'])?'invalid-':''}feedback`}>
                                                        {this.props.match.params.id ?"":"*Wajib isi"} *.pdf .jpeg .png ; *Max:2MB
                                                    </div>
                                                </div>
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            LaplayananAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const laplayanan = state.laplayanan;

                return {
                isAdmin,laplayanan
                };
            }
            const connectedLaplayananAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(LaplayananAddPage));

            export { connectedLaplayananAddPage as LaplayananAddPage };

