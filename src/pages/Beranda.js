import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CarouselImage from '../component/CarouselImage';
import image2 from '../img/pelabuhan2.jpg';
import image3 from '../img/logo-ppid-kkp-white.png';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {web, infopsmPostActions} from '../_actions';
import { Markup } from 'interweave';

const styles = (theme) => ({
    root:{
        flexGrow:1,
    }
});
class Beranda extends Component{

    constructor(props){
        super(props)
        const {dispatch} = this.props;
        dispatch(web());
        dispatch(infopsmPostActions.getInfopsmPost());
        
    }
    render(){
        const {classes} = this.props;
        const latestPost = this.props.infopsmPost;
        let count = 0;
        let contents = []
        for (let index = latestPost.length-1 ; index >= 0; index--) {
            contents = [...contents,{
                url:(latestPost[index].thumbnail)?latestPost[index].thumbnail:image3,
                title:latestPost[index].judul,
                body:<Markup content={latestPost[index].body}/>
            }]
            if (count === 3){
                break;
            }
            count++;
        }
        if (!contents.length){
        contents = [
            {   url:image2,
                title:"WEBSITE PEJABAT PENGELOLA INFORMASI DAN DOKUMENTASI KANTOR KESEHATAN PELABUHAN PADANG",
                body:""},
            ];
        }
        return (
            <div className={classes.root}>
                <CarouselImage contents={contents}/>
                <nav className="toolbar">
                    <a href="/permohonan">PERMOHONAN INFORMASI</a>
                    <a href="/keberatan">PENGAJUAN KEBERATAN</a>
                    <a href="/e-dip">E-DIP</a>
                </nav>
            </div>
        );
    }
}

Beranda.propTypes = {
    classes: PropTypes.object.isRequired
};

function mapStateToProps(state){
    const {isAdmin} = state.authentication;
    const {infopsmPost} = state.infopsmPost;
    return {isAdmin,infopsmPost};
}

const connectedBerandaPage= withRouter(connect(mapStateToProps,
    null, null, {pure:false})(withStyles(styles)(Beranda)));

export {connectedBerandaPage as Beranda};