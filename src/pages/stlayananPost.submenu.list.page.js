import React, { Component } from "react";
import { admin, menuActions } from "../_actions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { faInfo } from "@fortawesome/free-solid-svg-icons/faInfo";
import { faTrash } from "@fortawesome/free-solid-svg-icons/faTrash";
import { faPlus } from "@fortawesome/free-solid-svg-icons/faPlus";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { MDBDataTable } from "mdbreact";
import { AdminNav } from "../component";
import { menuWajib } from "../_utility/appstaticdata";

class StlayananPostSubmenuListPage extends Component {
  constructor(props){
    super(props)
    const { dispatch } = this.props;
    if (!this.props.isAdmin) dispatch(admin());
    dispatch(menuActions.getMenu());
  }

  delete(id) {
    const { dispatch } = this.props;
    dispatch(menuActions.deleteMenuById(id));
  }

  getSelectMenu = (idMenuUtama) => {
    let selectMenu = [];
    const menus = this.props.menu.find(
      (element) => element.id === parseInt(idMenuUtama)
    );
    if (menus) {
      menus.id_parent = null;
      selectMenu = [...selectMenu, menus];

      const subMenus = (subMen, id_parent) => {
        subMen.forEach((element) => {
          element.id_parent = id_parent;
          selectMenu = [...selectMenu, element];
          if (element.subMenus) {
            subMenus(element.subMenus, element.id);
          }
        });
      };

      subMenus(menus.subMenus, menus.id);
    }
    return selectMenu;
  };

  render() {
    const { match } = this.props;
    //  const link = match.url.split("/");
    let datatable = [];

    const wajib = menuWajib();

    // get data for list laporan layanan
    let rows = [];
    const menus = this.getSelectMenu(7);
    menus.forEach((menu, i) => {
      // console.log(i);
      const parent = menus.find(
        (element) => element.id === parseInt(menu.id_parent)
      );
      let namaParent = "Menu Utama";

      if (parent) namaParent = parent.nama_menu;
      rows.push({
        no:i+1,
        parent_menu: namaParent,
        nama_menu: menu.nama_menu,
        tipe: menu.tipe,
        action: (
          <>
            <a
              href={`${match.url}/ubah/${menu.id}`}
              className="btn btn-sm btn-info mr-2"
            >
              <FontAwesomeIcon icon={faInfo} className="mr-1" />
              Detail
            </a>
            {/* menu yang wajib tidak bolah hapus */}
            {wajib.findIndex((e) => e === parseInt(menu.id)) < 0 && (
              <button
                className="btn btn-sm btn-flat btn-danger"
                onClick={() => this.delete(menu.id)}
              >
                <FontAwesomeIcon icon={faTrash} className="mr-1" />
                Delete
              </button>
            )}
          </>
        ),
      });
    });

    datatable = {
      columns: [
        {
          label: "No",
          field: "no",
          sort: "asc",
          width: 10,
        },
        {
          label: "Parent Menu",
          field: "parent_menu",
          sort: "asc",
        },
        {
          label: "Nama Menu",
          field: "nama_menu",
          sort: "asc",
        },
        {
          label: "Tipe",
          field: "tipe",
          sort: "asc",
        },
        {
          label: "Action",
          field: "action",
          sort: "asc",
        },
      ],
      rows: rows,
    };

    return (
      <div className="content">
        {/* <Breadcrumb links={link}/> */}
        <div className="content-container">
          <div className="post-head">
            <div className="post-title">Data SubMenu </div>
            <a
              href="/admin/stlayanan/sub/tambah"
              className="btn btn-flat btn-sm btn-success"
            >
              {" "}
              <FontAwesomeIcon icon={faPlus} /> Tambah
            </a>
          </div>
          <MDBDataTable
            btn
            striped
            bordered
            small
            data={datatable}
            sorting={"true"}
          />
        </div>
        <AdminNav />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { isAdmin } = state.authentication;
  const { menu } = state.menu;
  return { isAdmin, menu };
}

const connectedStlayananPostSubmenuListPage = withRouter(
  connect(mapStateToProps, null, null, { pure: false })(
    StlayananPostSubmenuListPage
  )
);

export { connectedStlayananPostSubmenuListPage as StlayananPostSubmenuListPage };
