import React,{Component} from 'react';
import {dipActions, web} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {MDBDataTable} from 'mdbreact';

class DIPPage extends Component{
            componentDidMount(){
                const {dispatch} = this.props;
                if (this.props.isAdmin) dispatch(web());
                dispatch(dipActions.getDip());
              }

              render(){
                  //  const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let dips=this.props.dip || [];

                      // get data for list laporan layanan
                      let rows=[];
                      dips.forEach((dip,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													rincian:dip.rincian,
													pejabat:dip.pejabat,
													pngjwb:dip.pngjwb,
													wktbuatinfo:dip.wktbuatinfo,
													tmptbuatinfo:dip.tmptbuatinfo,
													bfi_soft:dip.bfi_soft,
													bfi_hard:dip.bfi_hard,
													ji_bk:dip.ji_bk,
													ji_ts:dip.ji_ts,
													ji_sm:dip.ji_sm,
													jk_wkt_simpan:dip.jk_wkt_simpan,
													keterangan:dip.keterangan,
													created_at:dip.created_at,
													updated_at:dip.updated_at,
													enabled:dip.enabled
			
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Rincian Isi Informasi',
                            field: 'rincian',
                            sort: 'asc',
                          },
													{
                            label: 'Pejabat atau Unit/Satuan',
                            field: 'pejabat',
                            sort: 'asc'
                          },
													{
                            label: 'Penanggungjawab',
                            field: 'pngjwb',
                            sort: 'asc'
                          },
													{
                            label: 'Waktu Pembuatan Informasi',
                            field: 'wktbuatinfo',
                            sort: 'asc'
                          },
													{
                            label: 'Tempat Pembuatan Informasi',
                            field: 'tmptbuatinfo',
                            sort: 'asc'
                          },
													{
                            label: 'Bentuk Format Informasi Softcopy',
                            field: 'bfi_soft',
                            sort: 'asc'
                          },
													{
                            label: 'Bentuk Format Informasi Hardcopy',
                            field: 'bfi_hard',
                            sort: 'asc'
                          },
													{
                            label: 'Jenis Informasi BK',
                            field: 'ji_bk',
                            sort: 'asc'
                          },
													{
                            label: 'Jenis Informasi TS',
                            field: 'ji_ts',
                            sort: 'asc'
                          },
													{
                            label: 'Jenis Informasi SM',
                            field: 'ji_sm',
                            sort: 'asc'
                          },
													{
                            label: 'Jangka Waktu Penyimpanan',
                            field: 'jk_wkt_simpan',
                            sort: 'asc'
                          },
													{
                            label: 'Keterangan',
                            field: 'keterangan',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Informasi Publik </div>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting="true"
                          />
                        </div>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {dip} = state.dip;
                const {menu} = state.menu;
                return {isAdmin,menu,dip};
            }

            const connectedDIPPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(DIPPage));

            export {connectedDIPPage as DIPPage};

