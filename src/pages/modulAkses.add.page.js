import React,{Component} from 'react';
import {modulAksesActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { AdminNav } from '../component';

class ModulAksesAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{}
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(modulAksesActions.getModulAksesById(params.id))
                }
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
                    dispatch(modulAksesActions.onChangeModulAksesProps('selectedFile',event));
                }else{
                    dispatch(modulAksesActions.onChangeModulAksesProps(prop, event));
                } //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {modulAkses,id,created_at,updated_at,...fields} = this.props.modulAkses;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {modulAkses} =this.props;
                if (this.handleValidate()){

                    const payload={
				id_level:modulAkses.id_level,
				id_modul:modulAkses.id_modul,
				l:modulAkses.l,
				d:modulAkses.d,
				t:modulAkses.t,
				u:modulAkses.u,
				h:modulAkses.h,
			}
                        if(params.id){
                            dispatch(modulAksesActions.editModulAksesInfo(params.id,payload))
                        }else{
                            dispatch(modulAksesActions.createModulAkses(payload))
                        }
                        alert("Form Submitted")
                    }else{
                        this.setState({
                            loading:false
                        })
                        alert("Form has error")
                    }
                }

                render(){
                    const {modulAkses} = this.props;
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">ModulAkses</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					<div className="form-group">
                                                    <label htmlFor="id_level">Id Level</label>
                                                    <input id='id_level' name="id_level" type="text" className={`form-control ${(this.state.resMessage['id_level'])?'is-invalid':''}`} value={modulAkses['id_level']} onChange={this.handleChange('id_level')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id_level'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="id_modul">Id Modul</label>
                                                    <input id='id_modul' name="id_modul" type="text" className={`form-control ${(this.state.resMessage['id_modul'])?'is-invalid':''}`} value={modulAkses['id_modul']} onChange={this.handleChange('id_modul')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id_modul'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="l">L</label>
                                                    <input id='l' name="l" type="text" className={`form-control ${(this.state.resMessage['l'])?'is-invalid':''}`} value={modulAkses['l']} onChange={this.handleChange('l')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['l'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="d">D</label>
                                                    <input id='d' name="d" type="text" className={`form-control ${(this.state.resMessage['d'])?'is-invalid':''}`} value={modulAkses['d']} onChange={this.handleChange('d')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['d'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="t">T</label>
                                                    <input id='t' name="t" type="text" className={`form-control ${(this.state.resMessage['t'])?'is-invalid':''}`} value={modulAkses['t']} onChange={this.handleChange('t')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['t'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="u">U</label>
                                                    <input id='u' name="u" type="text" className={`form-control ${(this.state.resMessage['u'])?'is-invalid':''}`} value={modulAkses['u']} onChange={this.handleChange('u')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['u'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="h">H</label>
                                                    <input id='h' name="h" type="text" className={`form-control ${(this.state.resMessage['h'])?'is-invalid':''}`} value={modulAkses['h']} onChange={this.handleChange('h')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['h'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            ModulAksesAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const modulAkses = state.modulAkses;

                return {
                isAdmin,modulAkses
                };
            }
            const connectedModulAksesAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(ModulAksesAddPage));

            export { connectedModulAksesAddPage as ModulAksesAddPage };

