import React,{Component} from 'react';
import {admin,dipActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';
import {dttodate}from '../helpers/';
import {bentukInformasi,jenisInformasi}from '../_utility/appstaticdata'

class DipListPage extends Component{

            constructor(props){
              super(props)
              const {dispatch} = this.props;
              if (!this.props.isAdmin) dispatch(admin());
              dispatch(dipActions.getDip())
            }
            componentDidMount(){
                
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(dipActions.deleteDipById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let dips=this.props.dip || [];

                      // get data for list laporan layanan
                      let rows=[];
                      dips.forEach((dip,i) => {
                        console.log(i);
                        rows.push({
                          no:i+1,
													rincian:dip.rincian,
													pejabat:dip.pejabat,
													pngjwb:dip.pngjwb,
													wktbuatinfo:dip.wktbuatinfo,
													tmptbuatinfo:dip.tmptbuatinfo,
													bfi_soft:bentukInformasi(dip.bfi_soft),
													bfi_hard:bentukInformasi(dip.bfi_hard),
													ji_bk:jenisInformasi(dip.ji_bk),
													ji_ts:jenisInformasi(dip.ji_ts),
													ji_sm:jenisInformasi(dip.ji_sm),
													jk_wkt_simpan:dip.jk_wkt_simpan,
													keterangan:dip.keterangan,
													created_at:dttodate(dip.created_at),
				action:
                          <>
                            <a href ={`${match.url}/ubah/${dip.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(dip.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Rincian Isi Informasi',
                            field: 'rincian',
                            sort: 'asc',
                          },
													{
                            label: 'Pejabat atau Unit/Satuan',
                            field: 'pejabat',
                            sort: 'asc'
                          },
													{
                            label: 'Penanggungjawab',
                            field: 'pngjwb',
                            sort: 'asc'
                          },
													{
                            label: 'Waktu Pembuatan Informasi',
                            field: 'wktbuatinfo',
                            sort: 'asc'
                          },
													{
                            label: 'Tempat Pembuatan Informasi',
                            field: 'tmptbuatinfo',
                            sort: 'asc'
                          },
													{
                            label: 'Bentuk Format Informasi Softcopy',
                            field: 'bfi_soft',
                            sort: 'asc'
                          },
													{
                            label: 'Bentuk Format Informasi Hardcopy',
                            field: 'bfi_hard',
                            sort: 'asc'
                          },
													{
                            label: 'Jenis Informasi BK',
                            field: 'ji_bk',
                            sort: 'asc'
                          },
													{
                            label: 'Jenis Informasi TS',
                            field: 'ji_ts',
                            sort: 'asc'
                          },
													{
                            label: 'Jenis Informasi SM',
                            field: 'ji_sm',
                            sort: 'asc'
                          },
													{
                            label: 'Jangka Waktu Penyimpanan',
                            field: 'jk_wkt_simpan',
                            sort: 'asc'
                          },
													{
                            label: 'Keterangan',
                            field: 'keterangan',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Informasi Publik </div>
                            <a href="/admin/dip/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting="true"
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {dip} = state.dip;
                const {menu} = state.menu;
                return {isAdmin,menu,dip};
            }

            const connectedDipListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(DipListPage));

            export {connectedDipListPage as DipListPage};

