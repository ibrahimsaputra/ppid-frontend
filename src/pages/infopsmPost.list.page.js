import React,{Component} from 'react';
import {admin,infopsmPostActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';
import {dttodate} from '../helpers';

class InfopsmPostListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(infopsmPostActions.getInfopsmPost());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(infopsmPostActions.deleteInfopsmPostById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let infopsmPosts=this.props.infopsmPost || [];

                      // get data for list laporan layanan
                      let rows=[];
                      infopsmPosts.forEach((infopsmPost,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													judul:infopsmPost.judul,
													created_at:dttodate(infopsmPost.created_at),
				action:
                          <>
                            <a href ={`${match.url}/ubah/${infopsmPost.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(infopsmPost.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Judul',
                            field: 'judul',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title mr-1">Data Informasi Publik Serta Merta Post</div>
                            <a href="/admin/infopsm/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {infopsmPost} = state.infopsmPost;
                const {menu} = state.menu;
                return {isAdmin,menu,infopsmPost};
            }

            const connectedInfopsmPostListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(InfopsmPostListPage));

            export {connectedInfopsmPostListPage as InfopsmPostListPage};

