import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {web} from '../_actions';
// import PageNotFound from '../assets/images/PageNotFound';

class NotFoundPage extends React.Component{

    componentDidMount(){
      const {isAdmin,dispatch} = this.props;
      if(isAdmin) dispatch(web()); 
    }
    render(){
      console.log('not found');
    return <div className="d-flex flex-column align-items-center justify-content-center"> 
              {/* <img src={PageNotFound}  /> */}
              <h1>404 Page Not Found</h1>
              <p style={{textAlign:"center"}}>
                <Link to="/">Go to Home </Link>
              </p>
          </div>;
    }

}

function mapStateToProps(state){
  const {isAdmin} = state.authentication;
  return {isAdmin};
}

const connectNotFoundPage = withRouter(connect(mapStateToProps,null,null,{pure:false})(NotFoundPage))


export {connectNotFoundPage as NotFoundPage};