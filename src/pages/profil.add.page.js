import React,{Component} from 'react';
import {profilActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { AdminNav } from '../component';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import noimage from '../img/no_image.jpg';

class ProfilAddPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            loading:false,
            resMessage:{},
            value:'',
            modules: {
                toolbar: [
                  [{ 'header': [1, 2, false] }],
                  ['bold', 'italic', 'underline','strike', 'blockquote'],
                  [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                  ['link', 'image'],
                  ['clean']
                ],
            }
        };
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        const {match:{params},dispatch,isAdmin} = this.props;

        if(!isAdmin)dispatch(admin());
        dispatch(profilActions.getProfilById(1))
    }

    componentDidMount(){
        
    }

    onEditorChange = (value, delta, source, editor) => {
        
        this.setState({
          value: editor.getContents()
        });
    }
    readURL(input,foto) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
              foto.setAttribute('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }

    handleChange = prop => event => {
        const {dispatch} = this.props;
        // console.log(prop,event.target.value);
        if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo" || prop==="link_struktur"){
            const image = document.getElementById(prop);
            
            this.readURL(event.target,image);
            dispatch(profilActions.onChangeProfilProps(prop,event));
        }else{
            dispatch(profilActions.onChangeProfilProps(prop, event));
        } //change
    };

    handleValidate (){
        let isValid = true;
        let errors = {};
        /*
        Beda setiap page
        Untuk field yang tidak wajib isi di deconstruct dari object;
        */
        const {profil,id,created_at,updated_at,link_struktur,thumbnail,link_pdf,enabled,currentFile,selectedFiles,countProfil,...fields} = this.props.profil;

        for (const key in fields) {
            if (fields.hasOwnProperty(key)) {
                if(!fields[key]){
                    isValid=false;
                    errors[key]="Wajib Isi"
                }
            }
        }
        this.setState({
            resMessage: errors
        })
        return isValid;
    }

    handleFormSubmit = event =>{
        event.preventDefault();
        event.stopPropagation();
        this.setState({
            loading:true
        });

        const {match:{params}}=this.props;
        const {dispatch} =this.props;
        const {profil} =this.props;
        if (this.handleValidate()){

            const payload={
                profil_singkat:profil.profil_singkat,
                tugas_fungsi:profil.tugas_fungsi,
                link_struktur:profil.link_struktur,
                visi_misi:profil.visi_misi,
            }
            
            dispatch(profilActions.editProfilInfo(1,payload))
            
            alert("Form Submitted")
        }else{
            this.setState({
                loading:false
            })
            alert("Form has error")
        }
    }

    render(){
        const {profil} = this.props;
       
        const formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
        ]

        return(
            <div className="bodier content">
                <div className="post-container content-container">
                        <div className="post-head">
                        <div className="post-title">Profil</div>
                        </div>
                        <div className="post-body">
                            <form action="" >
                                <fieldset>
                                    <legend></legend>
                                    <div className="form-group">
                                        <label htmlFor="profil_singkat">Profil Singkat</label>
                                        <div className="">
                                        <ReactQuill theme="snow" 
                                        value={profil['profil_singkat']} onChange={this.handleChange('profil_singkat')}
                                        modules={this.state.modules}  required/>
                                        </div>
                                        <div className={`feedback ${(this.state.resMessage['profil_singkat'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="tugas_fungsi">Tugas Fungsi</label>
                                        <ReactQuill theme="snow" id='tugas_fungsi' name="tugas_fungsi" 
                                        className="" 
                                        value={profil['tugas_fungsi']} onChange={this.handleChange('tugas_fungsi')} 
                                        modules={this.state.modules} formats={formats} required/>
                                        <div className={`feedback ${(this.state.resMessage['tugas_fungsi'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="link_struktur">Struktur Organisasi</label>
                                        <img id="link_struktur" src={profil.link_struktur?profil.link_struktur:noimage} className="mb-2" width={200+"px"} alt="link_struktur"/>
                                        <input id='link_struktur' name="link_struktur" type="file" className={`form-control ${(this.state.resMessage['link_struktur'])?'is-invalid':''}`} onChange={this.handleChange('link_struktur')} required/>
                                        <div className={`feedback ${(this.state.resMessage['link_struktur'])?'invalid-feedback':''}`}>
                                            *.jpeg .png ; *Max:2MB
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="visi_misi">Visi Misi</label>
                                        <ReactQuill id='visi_misi' name="visi_misi" type="text" 
                                        className={`${(this.state.resMessage['visi_misi'])?'is-invalid':''}`} 
                                        value={profil['visi_misi']} onChange={this.handleChange('visi_misi')} 
                                        modules={this.state.modules} formats={formats} required/>
                                        <div className={`feedback ${(this.state.resMessage['visi_misi'])?'invalid-feedback':''}`}>
                                            *Wajib isi
                                        </div>
                                    </div>
                                
                                <button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                </fieldset>
                            </form>
                        </div>
                </div>
                <AdminNav/>
            </div>
        );
    }

}

ProfilAddPage.propTypes = {

};
const mapStateToProps = (state) =>{
    const { isAdmin } = state.authentication;
    const profil = state.profil;

    return {
    isAdmin,profil
    };
}
const connectedProfilAddPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(ProfilAddPage));

export { connectedProfilAddPage as ProfilAddPage };

