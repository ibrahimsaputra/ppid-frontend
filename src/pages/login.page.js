import React,{Component} from "react";
import {authActions, web} from "../_actions";
import { withStyles } from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types'; 




const styles = theme => ({
    root: {
       display: 'flex',
       flexWrap: 'wrap'
    },    
    container: {
       display: 'flex',
       flexWrap: 'wrap',
    },    
    margin: {
       margin: theme.spacing(),
    },    
    withoutLabel: {
       marginTop: theme.spacing(3),
    },    
    textField: {
       marginLeft: theme.spacing(),
       marginRight: theme.spacing(),
       width: 200,
    },    
    paper: {
       padding: theme.spacing(2),
       textAlign: 'center',
       color: theme.palette.text.secondary,
    },    
    button: {
       
    },    
    input: {
       display: 'none',
    },
});

class Login extends Component{
    constructor(props){
        super(props);

        this.state ={
            username:"",
            password:"",
            loading:false,
            showPassword:false,
            resMessage:{}
        };
        this.handleLogin = this.handleLogin.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        const {dispatch,isAdmin,history} = this.props;
        if(!isAdmin)dispatch(web());
        // console.log(localStorage.getItem('user'));
        if(localStorage.getItem('user')){
            history.push('/admin/dashboard');
        }
    }

    handleChange = prop => event => {
        this.setState({[prop]:event.target.value});
    };

    handleValidate (){
        let isValid = true;
        let errors = {};
        /*
        Beda setiap page
        Untuk field yang tidak wajib isi di deconstruct dari object;
        */
        const {loading,resMessage,showPassword,...fields} = this.state;

        for (const key in fields) {
            if (fields.hasOwnProperty(key)) {
                if(!fields[key]){
                    isValid=false;
                    errors[key]="Wajib Isi"
                }

                /**
                * Untuk Field dengan Spesifikasi Khusus
                */
                if(typeof fields["nama_pemohon"] !== "undefined"){
                    if(!fields["nama_pemohon"].match(/^[a-z A-Z]+$/)){
                    isValid = false;
                    errors["nama_pemohon"] = "Hanya Huruf";
                    }
                }

                if(typeof fields["email"] !== "undefined"){
                    let lastAtPos = fields["email"].lastIndexOf('@');
                    let lastDotPos = fields["email"].lastIndexOf('.');

                    if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                        isValid = false;
                        errors["email"] = "Email Tidak valid";
                        }
                }

            }
        }
        this.setState({
            resMessage: errors
        })
        return isValid;
    }

    handleLogin = event =>{
      
        this.setState({
            loading:true
        });

        const {dispatch} =this.props;
        if (this.handleValidate()){
            dispatch(authActions.login(this.state.username,this.state.password))
            this.setState({
                loading:false
            })
            alert("Form Submitted")
        }else{
            this.setState({
                loading:false
            })
            alert("Form has error")
        }
    }

    render(){
        const {classes} = this.props;
        const {resMessage,history} = this.props;
        // console.log(localStorage.getItem('user'));
        if(localStorage.getItem('user')){
            history.push('/admin/dashboard');
        }
        return(
            <div className="login">
                <div className ="card card-container" > 
                    <img
                        src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
                        alt="profile-img"
                        className="profile-img-card"
                    />
                   
                    <form style={{padding:"0 20px"}}>
                        {resMessage && (
                            <div className="form-group">
                                <div className="alert alert-danger text-center" style={{fontSize:"0.9em"}} role="alert">
                                    {resMessage}
                                </div>
                            </div>
                        )}
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input username='username' name="username" type="text" className={`form-control ${(this.state.resMessage['username'])?'is-invalid':''}`} value={this.state.username} onChange={this.handleChange('username')} required/>
                            <div className={`feedback ${(this.state.resMessage['username'])?'invalid-feedback':''}`}>
                                *Wajib isi
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input password='password' name="password" type="password" className={`form-control ${(this.state.resMessage['password'])?'is-invalid':''}`} value={this.state.password} onChange={this.handleChange('password')} required/>
                            <div className={`feedback ${(this.state.resMessage['password'])?'invalid-feedback':''}`}>
                                *Wajib isi
                            </div>
                        </div>

                        <div className="form-group">
                            
                            <button
                                className={classes.button+" btn btn-primary btn-block"} 
                                onClick={this.handleLogin}
                                type="button"
                            >
                                <span>Login</span>
                            </button>
                            {this.state.loading &&(
                                <span className="spinner-border spinner-border-sm"></span>
                            )}
                        </div>
                        
                    </form>
                
                </div>
            </div>
        );
    }
    
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = (state) =>{
    const { loggingIn } = state.authentication;
    const { resMessage } = state.authentication;
    
    return {
       loggingIn,resMessage
    };
}
const connectedLoginPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Login)));

export { connectedLoginPage as Login };

