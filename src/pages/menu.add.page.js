import React,{Component} from 'react';
import {menuActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { AdminNav } from '../component';

class MenuAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{}
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(menuActions.getMenuById(params.id))
                }
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
                    dispatch(menuActions.onChangeMenuProps(prop,event));
                }else{
                    dispatch(menuActions.onChangeMenuProps(prop, event));
                } //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {menu,id,created_at,updated_at,link_icon,...fields} = this.props.menu;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {menu} =this.props;
                if (this.handleValidate()){

                    const payload={
                        id_parent:menu.id_parent,
                        nama_menu:menu.nama_menu,
                        tipe:menu.tipe,
                        link_icon:menu.link_icon
                    }
                        if(params.id){
                            dispatch(menuActions.editMenuInfo(params.id,payload))
                        }else{
                            dispatch(menuActions.createMenu(payload))
                        }
                        alert("Form Submitted")
                }else{
                    this.setState({
                        loading:false
                    })
                    alert("Form has error")
                }
            }

                render(){
                    const {menu} = this.props;
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">Menu</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					<div className="form-group">
                                                    <label htmlFor="id">Id</label>
                                                    <input id='id' name="id" type="text" className={`form-control ${(this.state.resMessage['id'])?'is-invalid':''}`} value={menu['id']} onChange={this.handleChange('id')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="id_parent">Id Parent</label>
                                                    <input id='id_parent' name="id_parent" type="text" className={`form-control ${(this.state.resMessage['id_parent'])?'is-invalid':''}`} value={menu['id_parent']} onChange={this.handleChange('id_parent')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id_parent'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="nama_menu">Nama Menu</label>
                                                    <input id='nama_menu' name="nama_menu" type="text" className={`form-control ${(this.state.resMessage['nama_menu'])?'is-invalid':''}`} value={menu['nama_menu']} onChange={this.handleChange('nama_menu')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['nama_menu'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="tipe">Tipe</label>
                                                    <input id='tipe' name="tipe" type="text" className={`form-control ${(this.state.resMessage['tipe'])?'is-invalid':''}`} value={menu['tipe']} onChange={this.handleChange('tipe')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['tipe'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="link_icon">Link Icon</label>
                                                    <input id='link_icon' name="link_icon" type="text" className={`form-control ${(this.state.resMessage['link_icon'])?'is-invalid':''}`} value={menu['link_icon']} onChange={this.handleChange('link_icon')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['link_icon'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="enabled">Enabled</label>
                                                    <input id='enabled' name="enabled" type="text" className={`form-control ${(this.state.resMessage['enabled'])?'is-invalid':''}`} value={menu['enabled']} onChange={this.handleChange('enabled')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['enabled'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="created_at">Created At</label>
                                                    <input id='created_at' name="created_at" type="text" className={`form-control ${(this.state.resMessage['created_at'])?'is-invalid':''}`} value={menu['created_at']} onChange={this.handleChange('created_at')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['created_at'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="updated_at">Updated At</label>
                                                    <input id='updated_at' name="updated_at" type="text" className={`form-control ${(this.state.resMessage['updated_at'])?'is-invalid':''}`} value={menu['updated_at']} onChange={this.handleChange('updated_at')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['updated_at'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            MenuAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const menu = state.menu;

                return {
                isAdmin,menu
                };
            }
            const connectedMenuAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(MenuAddPage));

            export { connectedMenuAddPage as MenuAddPage };

