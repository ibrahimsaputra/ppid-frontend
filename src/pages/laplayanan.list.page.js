import React,{Component} from 'react';
import {admin,laplayananActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';
import {dttodate} from '../helpers';

class LaplayananListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                
                dispatch(laplayananActions.getLaplayanan());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(laplayananActions.deleteLaplayananById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let laplayanans=this.props.laplayanan || [];

                      // get data for list laporan layanan
                      let rows=[];
                      laplayanans.forEach((laplayanan,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													tahun:laplayanan.tahun,
													link:<a href={laplayanan.link}>{laplayanan.link.split("/")[laplayanan.link.split("/").length-1]}</a>,
													created_at:dttodate(laplayanan.created_at),
													
				action:
                          <>
                            <a href ={`${match.url}/ubah/${laplayanan.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(laplayanan.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Tahun',
                            field: 'tahun',
                            sort: 'asc'
                          },
													{
                            label: 'Link',
                            field: 'link',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Laporan Layanan </div>
                            <a href="/admin/laplayanan/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {laplayanan} = state.laplayanan;
                const {menu} = state.menu;
                return {isAdmin,menu,laplayanan};
            }

            const connectedLaplayananListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(LaplayananListPage));

            export {connectedLaplayananListPage as LaplayananListPage};

