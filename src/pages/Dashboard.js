import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {admin,infopbPostActions,infopsmPostActions,
    infoptssPostActions,regulasiPostActions,stlayananPostActions,
    laplayananActions,dipActions, keberatanActions, permohonanActions} from '../_actions';
import {AdminNav} from '../component';
import Breadcrumb from '../component/Breadcrumb';

class Dashboard extends Component{

    componentDidMount(){
        const {dispatch} = this.props;
        dispatch(admin());
        dispatch(keberatanActions.getKeberatan());
        dispatch(permohonanActions.getPermohonan());
        dispatch(infopbPostActions.getInfopbPost());
        dispatch(infopsmPostActions.getInfopsmPost());
        dispatch(infoptssPostActions.getInfoptssPost());
        dispatch(regulasiPostActions.getRegulasiPost());
        dispatch(stlayananPostActions.getStlayananPost());
        dispatch(laplayananActions.getLaplayanan());
        dispatch(dipActions.getDip());
    }
    render(){
        
        return (
            <div className="content dashboard">
                <div className="content-container">
                    <div className="content-title">
                        <h1>Dashboard</h1>
                        <Breadcrumb links={[{nama:"Dashboard",link:"/admin/dashboard"}]}/>
                    </div>
                    <div className="content-body">
                        <div className="post-card-container ">
                            <a href="/admin/keberatan" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Keberatan</h5>
                                        <div className="counter">{this.props.keberatan.length || 0}</div>
                                    <div className="card-text">Laporan</div>
                                </div>
                            </a>
                            <a href="/admin/permohonan" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Permohonan</h5>
                                        <div className="counter">{this.props.permohonan.length || 0}</div>
                                    <div className="card-text">Laporan</div>
                                </div>
                            </a>
                            <a href="/admin/infopb" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Informasi Publik Berkala</h5>
                                        <div className="counter">{this.props.infopbPost.length || 0}</div>
                                    <div className="card-text">Postingan</div>
                                </div>
                            </a>
                            <a href="/admin/infoptss" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Informasi Publik Tersedia Setiap Saat</h5>
                                    <div className="counter">{this.props.infoptssPost.length || 0}</div>
                                    <div className="card-text">Postingan</div>
                                </div>
                            </a>
                            <a href="/admin/infoptsm" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Informasi Publik Serta Merta</h5>
                                    <div className="counter">{this.props.infopsmPost.length || 0}</div>
                                    <div className="card-text">Postingan</div>
                                </div>
                            </a>
                            <a href="/admin/regulasi" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Regulasi</h5>
                                    <div className="counter">{this.props.regulasiPost.length || 0}</div>
                                    <div className="card-text">Postingan</div>
                                </div>
                            </a>
                            <a href="/admin/stlayanan" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Standar Layanan</h5>
                                    <div className="counter">{this.props.stlayananPost.length || 0}</div>
                                    <div className="card-text">Postingan</div>
                                </div>
                            </a>
                            <a href="/admin/laplayanan" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Laporan Layanan</h5>
                                    <div className="counter">{this.props.laplayanan.length || 0}</div>
                                    <div className="card-text">Postingan</div>
                                </div>
                            </a>
                            <a href="/admin/dip" className="card" style={{width: 18+'rem'}}>
                                <div className="card-body">
                                    <h5 className="card-title">Data Informasi Publik</h5>
                                    <div className="counter">{this.props.dip.length || 0}</div>
                                    <div className="card-text">Postingan</div>
                                </div>
                            </a>
                      </div>
                    </div>
                </div>
                <AdminNav/>
            </div>
        );
    }
}

Dashboard.propTypes = {
};

function mapStateToProps(state){
    const {isAdmin} = state.authentication;
    const {keberatan} = state.keberatan;
    const {infopbPost} = state.infopbPost;
    const {infoptssPost} = state.infoptssPost;
    const {infopsmPost} = state.infopsmPost;
    const {regulasiPost} = state.regulasiPost;
    const {permohonan} = state.permohonan;
    const {stlayananPost} = state.stlayananPost;
    const {laplayanan} = state.laplayanan;
    const {dip} = state.dip;
    return {isAdmin, keberatan, infopbPost,infopsmPost,infoptssPost,
        regulasiPost,permohonan,stlayananPost,laplayanan,
        dip};
}

const connectedDashBoardPage= withRouter(connect(mapStateToProps,
    null, null, {pure:false})(Dashboard));

export {connectedDashBoardPage as Dashboard};