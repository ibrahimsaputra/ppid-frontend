import React,{Component} from 'react';
import {keberatanActions,admin, web} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { AdminNav } from '../component';
import { kategoriInformasi,alasanKeberatan, statusSuratMasuk } from '../_utility/appstaticdata';

class KeberatanAddPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            loading:false,
            resMessage:{}
        };
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        const {match:{params},dispatch,isAdmin} = this.props;
        if (isAdmin)dispatch(web());
        if(params.id){
            dispatch(admin())
            dispatch(keberatanActions.getKeberatanById(params.id))
        }
    }

    componentDidMount(){
        
    }
    readURL(input,foto) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
              foto.setAttribute('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }

    handleChange = prop => event => {
        const {dispatch} = this.props;
        // console.log(prop,event.target.value);
        if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
            const image = document.getElementById(prop);
            
            this.readURL(event.target,image);
            dispatch(keberatanActions.onChangeKeberatanProps(prop,event));
        }else{
            dispatch(keberatanActions.onChangeKeberatanProps(prop, event));
        } //change
    };

    handleValidate (){
        let isValid = true;
        let errors = {};
        /*
        Beda setiap page
        Untuk field yang tidak wajib isi di deconstruct dari object;
        */
        
        const {keberatan,id,created_at,updated_at,enabled,selectedFiles,currentFile,status,countKeberatan,...fields} = this.props.keberatan;
        

        for (const key in fields) {
            if (fields.hasOwnProperty(key)) {
                if(!fields[key]){
                    isValid=false;
                    errors[key]="Wajib Isi"
                }

                /**
                * Untuk Field dengan Spesifikasi Khusus
                */
                if(typeof fields["nama_pemohon"] !== "undefined"){
                    if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                    isValid = false;
                    errors["nama_pemohon"] = "Hanya Huruf";
                    }
                }

                if(typeof fields["email"] !== "undefined"){
                    let lastAtPos = fields["email"].lastIndexOf('@');
                    let lastDotPos = fields["email"].lastIndexOf('.');

                    if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                        isValid = false;
                        errors["email"] = "Email Tidak valid";
                        }
                }

            }
        }

        this.setState({
            resMessage: errors
        })
        // console.log(errors);
        return isValid;
    }

    handleFormSubmit = event =>{
        event.preventDefault();
        event.stopPropagation();
        this.setState({
            loading:true
        });

        const {match:{params}}=this.props;
        const {dispatch} =this.props;
        const {keberatan} =this.props;
        if (this.handleValidate()){

            const payload={
                nama_pemohon:keberatan.nama_pemohon,
                email:keberatan.email,
                alamat:keberatan.alamat,
                nohp:keberatan.nohp,
                pil_keberatan:keberatan.pil_keberatan,
                als_keberatan:keberatan.als_keberatan,
                als_lain:keberatan.als_lain,
                status:keberatan.status,
            }
                if(params.id){
                    dispatch(keberatanActions.editKeberatanInfo(params.id,payload))
                    this.props.history.push("/admin/keberatan");
                }else{
                    dispatch(keberatanActions.createKeberatan(payload))
                    // this.props.history.push("/");
                }
                this.setState({
                    loading:false
                })

                alert("Form Submitted")
                
            }else{
                this.setState({
                    loading:false
                })
                alert("Form has error")
            }
        }

        render(){
            const {keberatan} = this.props;
            return(
                <div className="bodier content">
                    <div className="post-container content-container">
                            <div className="post-head">
                            <div className="post-title">{this.props.match.params.id ? "Update" : "Lapor"} Keberatan</div>
                            </div>
                            <div className="post-body">
                                <form action="" >
                                    <fieldset>
                                        <legend></legend>
                                    
                                    <div className="form-group">
                                            <label htmlFor="nama_pemohon">Nama Pemohon</label>
                                            <input id='nama_pemohon' name="nama_pemohon" type="text" className={`form-control ${(this.state.resMessage['nama_pemohon'])?'is-invalid':''}`} value={keberatan['nama_pemohon']} onChange={this.handleChange('nama_pemohon')} required/>
                                            <div className={`${(this.state.resMessage['nama_pemohon'])?'invalid-':''}feedback`}>
                                                *Wajib isi
                                            </div>
                                        </div>
                                    <div className="form-group">
                                            <label htmlFor="email">Email</label>
                                            <input id='email' name="email" type="text" className={`form-control ${(this.state.resMessage['email'])?'is-invalid':''}`} value={keberatan['email']} onChange={this.handleChange('email')} required/>
                                            <div className={` ${(this.state.resMessage['email'])?'invalid-':''}feedback`}>
                                                *Wajib isi
                                            </div>
                                        </div>
                                    <div className="form-group">
                                            <label htmlFor="alamat">Alamat</label>
                                            <input id='alamat' name="alamat" type="text" className={`form-control ${(this.state.resMessage['alamat'])?'is-invalid':''}`} value={keberatan['alamat']} onChange={this.handleChange('alamat')} required/>
                                            <div className={`${(this.state.resMessage['alamat'])?'invalid-':''}feedback`}>
                                                *Wajib isi
                                            </div>
                                        </div>
                                    <div className="form-group">
                                            <label htmlFor="nohp">No HP</label>
                                            <input id='nohp' name="nohp" type="text" className={`form-control ${(this.state.resMessage['nohp'])?'is-invalid':''}`} value={keberatan['nohp']} onChange={this.handleChange('nohp')} required/>
                                            <div className={`feedback ${(this.state.resMessage['nohp'])?'invalid-feedback':''}`}>
                                                *Wajib isi
                                            </div>
                                        </div>
                                    <div className="form-group">
                                            <label htmlFor="pil_keberatan">Permohonan Yang DiBeratkan</label>
                                            <select id='pil_keberatan' name="pil_keberatan" 
                                            className={`form-control ${(this.state.resMessage['pil_keberatan'])?'is-invalid':''}`} 
                                            value={keberatan['pil_keberatan']} onChange={this.handleChange('pil_keberatan')} placeholder="-Pilih-" required>
                                                <option value="" disabled>-Pilih-</option>
                                                {kategoriInformasi().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                            </select>
                                            <div className={`${(this.state.resMessage['pil_keberatan'])?'invalid-':''}feedback`}>
                                                *Wajib isi
                                            </div>
                                        </div>
                                    <div className="form-group">
                                            <label htmlFor="als_keberatan">Alasan Keberatan</label>
                                            <select id='als_keberatan' name="als_keberatan" className={`form-control ${(this.state.resMessage['als_keberatan'])?'is-invalid':''}`} value={keberatan['als_keberatan']} onChange={this.handleChange('als_keberatan')} required>
                                                <option value="" disabled>-Pilih-</option>
                                                {alasanKeberatan().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                            </select>
                                            <div className={`${(this.state.resMessage['als_keberatan'])?'invalid-':''}feedback`}>
                                                *Wajib isi
                                            </div>
                                        </div>
                                    <div className="form-group">
                                            <label htmlFor="als_lain">Alasan Lain</label>
                                            <textarea id='als_lain' name="als_lain" type="text" className={`form-control ${(this.state.resMessage['als_lain'])?'is-invalid':''}`} value={keberatan['als_lain']} onChange={this.handleChange('als_lain')} required/>
                                        </div>
                                    {(this.props.match.params.id) && <div className="form-group">
                                            <label htmlFor="status">Status</label>
                                            <select id='status' name="status" type="text" className={`form-control ${(this.state.resMessage['status'])?'is-invalid':''}`} value={keberatan['status']} onChange={this.handleChange('status')} required>
                                                <option value="" disabled>-Pilih-</option>
                                                {statusSuratMasuk().map((element,i)=><option key={i} value={element} >{element} </option> )}
                                            </select>
                                            <div className={`${(this.state.resMessage['status'])?'invalid-':''}feedback`}>
                                                *Wajib isi
                                            </div>
                                        </div>}
                                    
                                    <button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    {this.props.isAdmin && <AdminNav/>}
                </div>
            );
        }

}

KeberatanAddPage.propTypes = {
    
};
const mapStateToProps = (state) =>{
    const { isAdmin } = state.authentication;
    const keberatan = state.keberatan;

    return {
    isAdmin,keberatan
    };
}
const connectedKeberatanAddPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(KeberatanAddPage));

export { connectedKeberatanAddPage as KeberatanAddPage };

