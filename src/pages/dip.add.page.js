import React,{Component} from 'react';
import {dipActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { AdminNav } from '../component';
import { bentukInformasi,jenisInformasi } from '../_utility/appstaticdata';

class DipAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{}
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(dipActions.getDipById(params.id))
                }
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                
                dispatch(dipActions.onChangeDipProps(prop, event));
                 //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {dip,id,created_at,updated_at,enabled,currentFile,selectedFiles,countDip,...fields} = this.props.dip;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {dip} =this.props;
                if (this.handleValidate()){

                    const payload={
				rincian:dip.rincian,
				pejabat:dip.pejabat,
				pngjwb:dip.pngjwb,
				wktbuatinfo:dip.wktbuatinfo,
				tmptbuatinfo:dip.tmptbuatinfo,
				bfi_soft:dip.bfi_soft,
				bfi_hard:dip.bfi_hard,
				ji_bk:dip.ji_bk,
				ji_ts:dip.ji_ts,
				ji_sm:dip.ji_sm,
				jk_wkt_simpan:dip.jk_wkt_simpan,
				keterangan:dip.keterangan,
			}
                        if(params.id){
                            dispatch(dipActions.editDipInfo(params.id,payload))
                        }else{
                            dispatch(dipActions.createDip(payload))
                        }
                        this.props.history.push("/admin/dip");
                        alert("Form Submitted")
                    }else{
                        this.setState({
                            loading:false
                        })
                        alert("Form has error")
                    }
                }

                render(){
                    const {dip} = this.props;
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">Data Informasi Publik</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					
                         					<div className="form-group">
                                                    <label htmlFor="rincian">Rincian</label>
                                                    <input id='rincian' name="rincian" type="text" className={`form-control ${(this.state.resMessage['rincian'])?'is-invalid':''}`} value={dip['rincian']} onChange={this.handleChange('rincian')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['rincian'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="pejabat">Pejabat</label>
                                                    <input id='pejabat' name="pejabat" type="text" className={`form-control ${(this.state.resMessage['pejabat'])?'is-invalid':''}`} value={dip['pejabat']} onChange={this.handleChange('pejabat')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['pejabat'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="pngjwb">Penanggung Jawab</label>
                                                    <input id='pngjwb' name="pngjwb" type="text" className={`form-control ${(this.state.resMessage['pngjwb'])?'is-invalid':''}`} value={dip['pngjwb']} onChange={this.handleChange('pngjwb')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['pngjwb'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="wktbuatinfo">Waktu Pembuatan Informasi</label>
                                                    <input id='wktbuatinfo' name="wktbuatinfo" type="date" className={`form-control ${(this.state.resMessage['wktbuatinfo'])?'is-invalid':''}`} value={dip['wktbuatinfo']} onChange={this.handleChange('wktbuatinfo')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['wktbuatinfo'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="tmptbuatinfo">Tempat Pembuatan Informasi</label>
                                                    <input id='tmptbuatinfo' name="tmptbuatinfo" type="text" className={`form-control ${(this.state.resMessage['tmptbuatinfo'])?'is-invalid':''}`} value={dip['tmptbuatinfo']} onChange={this.handleChange('tmptbuatinfo')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['tmptbuatinfo'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="bfi_soft">Bentuk Format Informasi Softcopy</label>
                                                    <select id='bfi_soft' name="bfi_soft" type="text" className={`form-control ${(this.state.resMessage['bfi_soft'])?'is-invalid':''}`} value={dip['bfi_soft']} onChange={this.handleChange('bfi_soft')} required>
                                                        <option value="" disabled>-Pilih-</option>
                                                        {bentukInformasi().map((element,i)=><option key={i} value={i} >{element} </option> )}
                                                    </select>
                                                    <div className={`feedback ${(this.state.resMessage['bfi_soft'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="bfi_hard">Bentuk Format Informasi Hardcopy</label>
                                                    <select id='bfi_hard' name="bfi_hard" type="text" className={`form-control ${(this.state.resMessage['bfi_hard'])?'is-invalid':''}`} value={dip['bfi_hard']} onChange={this.handleChange('bfi_hard')} required>
                                                        <option value="" disabled>-Pilih-</option>
                                                        {bentukInformasi().map((element,i)=><option key={i} value={i} >{element} </option> )}
                                                    </select>
                                                    <div className={`feedback ${(this.state.resMessage['bfi_hard'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="ji_bk">Jenis Informasi BK</label>
                                                    <select id='ji_bk' name="ji_bk" type="text" className={`form-control ${(this.state.resMessage['ji_bk'])?'is-invalid':''}`} value={dip['ji_bk']} onChange={this.handleChange('ji_bk')} required>
                                                        <option value="" disabled>-Pilih-</option>
                                                        {jenisInformasi().map((element,i)=><option key={i} value={i} >{element} </option> )}
                                                    </select>
                                                    <div className={`feedback ${(this.state.resMessage['ji_bk'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="ji_ts">Jenis Informasi TS</label>
                                                    <select id='ji_ts' name="ji_ts" type="text" className={`form-control ${(this.state.resMessage['ji_ts'])?'is-invalid':''}`} value={dip['ji_ts']} onChange={this.handleChange('ji_ts')} required>
                                                    <option value="" disabled>-Pilih-</option>
                                                        {jenisInformasi().map((element,i)=><option key={i} value={i} >{element} </option> )}
                                                    </select>
                                                    <div className={`feedback ${(this.state.resMessage['ji_ts'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="ji_sm">Jenis Informasi SM</label>
                                                    <select id='ji_sm' name="ji_sm" type="text" className={`form-control ${(this.state.resMessage['ji_sm'])?'is-invalid':''}`} value={dip['ji_sm']} onChange={this.handleChange('ji_sm')} required>
                                                    <option value="" disabled>-Pilih-</option>
                                                        {jenisInformasi().map((element,i)=><option key={i} value={i} >{element} </option> )}
                                                    </select>
                                                    <div className={`feedback ${(this.state.resMessage['ji_sm'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="jk_wkt_simpan">Jangka Waktu Penyimpanan</label>
                                                    <input id='jk_wkt_simpan' name="jk_wkt_simpan" type="text" className={`form-control ${(this.state.resMessage['jk_wkt_simpan'])?'is-invalid':''}`} value={dip['jk_wkt_simpan']} onChange={this.handleChange('jk_wkt_simpan')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['jk_wkt_simpan'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="keterangan">Keterangan</label>
                                                    <textarea id='keterangan' name="keterangan" type="text" className={`form-control ${(this.state.resMessage['keterangan'])?'is-invalid':''}`} value={dip['keterangan']} onChange={this.handleChange('keterangan')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['keterangan'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            DipAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const dip = state.dip;

                return {
                isAdmin,dip
                };
            }
            const connectedDipAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(DipAddPage));

            export { connectedDipAddPage as DipAddPage };

