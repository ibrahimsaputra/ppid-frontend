import React, { Component } from 'react';
import Breadcrumb from '../component/Breadcrumb';
import {web,laplayananActions, permohonanActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faFilePdf} from '@fortawesome/free-solid-svg-icons/faFilePdf';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import {Pie} from 'react-chartjs-2';
import {tipePemohon, kategoriInformasi} from '../_utility/appstaticdata'

class LapLayananPage extends Component {

  constructor(props){
    super(props)
    const {dispatch,match} = this.props;
    if (this.props.isAdmin) dispatch(web());
        console.log(window.location.pathname === match.url)
      if (match.isExact && window.location.pathname === match.url){
          dispatch(laplayananActions.getLaplayanan())
      }
      if (match.isExact && window.location.pathname === match.url){
        dispatch(permohonanActions.getPermohonan())
    }
  }

  render(){
       const {match} = this.props;
      //  const legendOpts = {
      //     display: false,
      //     position: 'top',
      //     fullWidth: true,
      //     reverse: false,
      //     labels: {
      //       fontColor: 'rgb(255, 99, 132)'
      //     }
      //   };
       let data =[];
       let posts=[];
       let dataTipePemohon={
         labels:[
           'WNI','Badan Hukum'
         ],
         datasets:[{
           data:[],
           backgroundColor:[
            '#FF6384',
            '#36A2EB',
           ],
           hoverBackgroundColor:[
            '#FF6384',
            '#36A2EB',
           ]
         }]
       };
       let dataKategoriPemohon={
        labels:[
          'Regulasi','RKA','Lakip','Laporan Keuangan','Perjanjian','ProfilBadanPublik','Kegiatan','LHKPN','RENSTRA','Lainnya'
        ],
        datasets:[{
          data:[],
          backgroundColor:[
           '#2180C1',
           '#248DD4',
           '#2585CC',
           '#267CC4',
           '#276AB3',
           '#2A4792',
           '#2C3682',
           '#2D2471',
           '#2E1260',
           '#2F004F',          
          ],
          hoverBackgroundColor:[
            '#2180C1',
            '#248DD4',
            '#2585CC',
            '#267CC4',
            '#276AB3',
            '#2A4792',
            '#2C3682',
            '#2D2471',
            '#2E1260',
            '#2F004F',  
          ]
        }]
       };

       if (match.isExact){
          
          posts = this.props.laplayanan || [];
          
          // get data permohonan for chart
          const permohonan = this.props.permohonan || [];
          console.log(permohonan)
          dataKategoriPemohon.datasets[0].data = [0,0,0,0,0,0,0,0,0,0];
          dataTipePemohon.datasets[0].data = [0,0];

          permohonan.forEach(e=>{
            const indexKategori = kategoriInformasi().findIndex(d => d === e.info_kategori);
            const indexTipe = tipePemohon().findIndex(d => d === e.tipe_pemohon);
            dataKategoriPemohon.datasets[0].data[indexKategori]++;
            dataTipePemohon.datasets[0].data[indexTipe]++;
          })


          
          // dataKategoriPemohon.datasets[0].data = [1,2,3,4,5,6,7,8,9,10];
          // dataTipePemohon.datasets[0].data = [1,2];

          
          // get data for list laporan layanan
          let rows=[];
          posts.forEach(({tahun,link,...props},i) => {
            // console.log(i);
            rows.push({
              tahun:tahun,
              link: <a href ={link} download><FontAwesomeIcon icon={faFilePdf} />  Download File Terkait</a>
            })            
          });
          data = {
            columns: [
              {
                label: 'Tahun',
                field: 'tahun',
                sort: 'asc',
                width: 270
              },
              {
                label: 'Link',
                field: 'link',
                sort: 'asc',
                width: 200
              }
            ],
            rows: rows
          }
        }
       return (
      <div className="bodier">
        {match.isExact &&
          (            
            (
              <div className="bodier" >
              <Breadcrumb links={this.props.link}/>
              <div className="post-container">
                  <div className="post-head">
                    <div className="post-title">Laporan Layanan</div>
                  </div>
                  
                  <div className="post-body">
                  <h3 className="">Statistik Permohonan Informasi</h3>
                  <div className="d-flex flex-row justify-content-around align-items-end flex-wrap my-5">
                      <div className="chart1">
                        <Pie data={dataKategoriPemohon} height={300} width={400} options={{maintainAspectRatio:false}} redraw/>
                      </div>
                      <div className="chart2">
                        <Pie data={dataTipePemohon} height={300} width={400}  options={{maintainAspectRatio:false}} redraw/>
                      </div>
                  </div>
                  <h3>Laporan Tahunan</h3>
                  <MDBDataTable btn
                    striped
                    bordered
                    small
                    data={data}
                    sorting="true"
                  />
  
                  </div>
              </div>         
          </div>
            )
          )
        }
      </div>
  );
  }
}

// LapLayananPage.propTypes = {
//     classes: PropTypes.object.isRequired
// };

function mapStateToProps(state){
    const {isAdmin} = state.authentication;
    const {laplayanan} = state.laplayanan;
    const {permohonan} = state.permohonan;
    const {menu} = state.menu;
    return {isAdmin,laplayanan,menu,permohonan};
}

const connectedLapLayananPage= withRouter(connect(mapStateToProps,
    null, null, {pure:false})(LapLayananPage));

export {connectedLapLayananPage as LapLayananPage};
