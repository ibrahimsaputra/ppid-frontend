import React, { Component } from 'react';
import {web,profilActions} from '../_actions';
import {withRouter, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {Markup} from 'interweave';
import {noimage} from '../img/str_org.jpeg';

class ProfilPage extends Component {

  constructor(props){
    super(props)
      const {match} = this.props;
      const {dispatch} = this.props;
      if (this.props.isAdmin) dispatch(web());
      console.log(window.location.href)
      if (match.isExact && window.location.pathname === match.url && !this.props.profil.length){
        dispatch(profilActions.getProfil());
      }
  }


  render(){
       const {match} = this.props;
       const link = match.url.split("/");
       const {profil} = this.props;
       const currentLink = link[link.length-1];
       const pages = ["19","20","21","22"];
       let nama_menu= "";
       let body="";
       let link_struktur="";
      if(match.isExact && profil.length > 0){
        // console.log(profil)
       switch (currentLink){
        case "19":
          nama_menu = "Profil Singkat"
          body = profil[0].profil_singkat;
          break;
        case "20":
          nama_menu = "Tugas dan Fungsi"
          body = profil[0].profil_singkat;
          break;
        case "21":
          nama_menu = "Sturuktur Organisasi"
          link_struktur = (profil[0].link_struktur);
          break;
        case "22":
          nama_menu = "Visi Misi"
          body = profil[0].profil_singkat;
          break;
        default:
          break;
       }
      }
      // console.log(tipe)
       return (
        <div className="bodier">
          {match.isExact && (pages.find(element => element === currentLink)) &&
              <div className="post-container">
                <div className="post-head">
                  <div className="post-title">{nama_menu}</div>
                </div>
                <div className="post-body">
                    {body && <Markup content={body}/>}
                    {link_struktur && <img src={link_struktur} style={{width:"100%"}} alt=""/> }
                </div>
            </div>
          }
          <Route path={`${match.url}/:id_menu`} render={withRouter(connect(mapStateToProps, //check
          null, null, {pure:false})((routerProps)=><ProfilPage {...routerProps} 
          link={[...this.props.link,{nama:nama_menu,link:match.url}]}/>))}/>
        </div>
      );
  }
}


function mapStateToProps(state){
    const {isAdmin} = state.authentication;
    const {profil} = state.profil;
    const {menu} = state.menu;
    return {isAdmin,profil,menu};
}

const connectedProfilPage= withRouter(connect(mapStateToProps,
    null, null, {pure:false})(ProfilPage));

export {connectedProfilPage as ProfilPage};
