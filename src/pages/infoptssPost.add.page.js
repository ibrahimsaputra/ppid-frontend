import React,{Component} from 'react';
import {infoptssPostActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { AdminNav } from '../component';
import noimage from '../img/no_image.jpg';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';


class InfoptssPostAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{},
                    value:'',
                    modules: {
                        toolbar: [
                        [{ 'header': [1, 2, false] }],
                        ['bold', 'italic', 'underline','strike', 'blockquote'],
                        [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                        ['link', 'image'],
                        ['clean']
                        ],
                    }
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(infoptssPostActions.getInfoptssPostById(params.id))
                }
            }

            readURL(input,foto) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
            
                    reader.onload = function (e) {
                      foto.setAttribute('src', e.target.result);
                    }
            
                    reader.readAsDataURL(input.files[0]);
                }
            }

            ValidateSingleInput(oInput) {
                const _validFileExtensions = [".jpg", ".jpeg", ".pdf", ".png"];    
                if (oInput.type === "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        
                        if (!blnValid) {
                            return false;
                        }
                    }
                }
                return true;
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
                    if (prop === "upload_ktp" || prop === "thumbnail"|| prop==="photo"){
                        const image = document.getElementById(prop);
                        this.readURL(event.target,image);
                    }
                    if (this.ValidateSingleInput(event.target)) dispatch(infoptssPostActions.onChangeInfoptssPostProps(prop,event));
                }else{
                    dispatch(infoptssPostActions.onChangeInfoptssPostProps(prop, event));
                } //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {infoptssPost,id,created_at,updated_at,body,thumbnail,link_pdf,enabled,currentFile,selectedFiles,countInfoptssPost,...fields} = this.props.infoptssPost;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                console.log(this.state.resMessage);
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {infoptssPost} =this.props;
                if (this.handleValidate()){

                    const payload={
				id_menu:infoptssPost.id_menu,
				judul:infoptssPost.judul,
				body:infoptssPost.body,
				thumbnail:infoptssPost.thumbnail,
			}
                        if(params.id){
                            dispatch(infoptssPostActions.editInfoptssPostInfo(params.id,payload))
                        }else{
                            dispatch(infoptssPostActions.createInfoptssPost(payload))
                        }
                        alert("Form Submitted")
                        this.props.history.push("/admin/infoptss");
                    }else{
                        this.setState({
                            loading:false
                        })
                        alert("Form has error")
                    }
                }

                getSelectMenu = (idMenuUtama)=>{
                    let selectMenu=[];
                    const menus = this.props.menu.find(element=>element.id === 3).subMenus.find(element=>element.id === parseInt(idMenuUtama));
                    if (menus){
                        selectMenu = [...selectMenu,menus];
                        
                        const subMenus = (subMen) => {
                            subMen.forEach((element)=>{
                                selectMenu = [...selectMenu,element];
                                if (element.subMenus){
                                    subMenus(element.subMenus)
                                }
                            })
                        }
                        
                        subMenus(menus.subMenus)
                    }
                    return selectMenu;
                }

                getTipeMenu(id_menu){
                    const menu = this.getSelectMenu(5).find(e => e.id === parseInt(id_menu))
                    return menu?menu.tipe:"";
                }

                render(){
                    const {infoptssPost} = this.props;
                    // const formats = [
                    //     'header',
                    //     'bold', 'italic', 'underline', 'strike', 'blockquote',
                    //     'list', 'bullet', 'indent',
                    //     'link', 'image'
                    // ]
                    
                    // console.log(this.getSelectMenu(2));
                    const tipe = this.getTipeMenu(infoptssPost.id_menu);
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">{this.props.match.params.id ? "Ubah" : "Tambah"} Informasi Publik Setiap Saat Post</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					    <div className="form-group">
                                                    <label htmlFor="id_menu">Sub Menu</label>
                                                    <select id='id_menu' name="id_menu" 
                                                        className={`form-control ${(this.state.resMessage['id_menu'])?'is-invalid':''}`} 
                                                        value={infoptssPost['id_menu']} onChange={this.handleChange('id_menu')} 
                                                        required>
                                                            <option value="">-Pilih-</option> 
                                                            {this.getSelectMenu(5).map((element,i)=><option key={i} value={element.id} >{element.nama_menu} </option> )}
                                                    </select>
                                                    <div className={`feedback ${(this.state.resMessage['id_menu'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					    <div className="form-group">
                                                    <label htmlFor="judul">{ tipe === "list pdf"?"Deskripsi":"Judul"}</label>
                                                    <input id='judul' name="judul" type="text" className={`form-control ${(this.state.resMessage['judul'])?'is-invalid':''}`} value={infoptssPost['judul']} onChange={this.handleChange('judul')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['judul'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                                                { tipe !== "list pdf" &&
                         					    <div className="form-group">
                                                    <label htmlFor="body">Body</label>
                                                    <div className="">
                                                        <ReactQuill theme="snow" id='body' name="body" type="text" 
                                                        value={infoptssPost['body']} onChange={this.handleChange('body')} 
                                                        modules={this.state.modules} required/>
                                                    </div>
                                                    <div className={`feedback ${(this.state.resMessage['body'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>}
                         					    <div className="form-group">
                                                    <label htmlFor="thumbnail">Thumbnail</label>
                                                        { tipe !== "list pdf" && <img id='thumbnail' src={infoptssPost['thumbnail']?infoptssPost['thumbnail']:noimage} className="mb-2" width={200+"px"} alt=""/>}
                                                        { tipe === "list pdf" && <a href={infoptssPost['thumbnail']?infoptssPost['thumbnail']:"#"}>{infoptssPost['thumbnail']?infoptssPost['thumbnail'].split('/')[4]:""} </a>}
                                                    <input  name="thumbnail" type="file" className={`form-control ${(this.state.resMessage['thumbnail'])?'is-invalid':''}`} onChange={this.handleChange('thumbnail')} required/>
                                                        <div className={`${(this.state.resMessage['thumbnail'])?'invalid-':''}feedback `}>
                                                        *Wajib isi *.jpeg .png ; *Max:2MB
                                                    </div>
                                                </div>
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            InfoptssPostAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const infoptssPost = state.infoptssPost;
                const {menu} = state.menu;

                return {
                isAdmin,infoptssPost,menu
                };
            }
            const connectedInfoptssPostAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(InfoptssPostAddPage));

            export { connectedInfoptssPostAddPage as InfoptssPostAddPage };

