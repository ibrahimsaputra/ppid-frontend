import React,{Component} from 'react';
import {admin,infoptssPostActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';
import {dttodate} from '../helpers';

class InfoptssPostListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(infoptssPostActions.getInfoptssPost());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(infoptssPostActions.deleteInfoptssPostById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let infoptssPosts=this.props.infoptssPost || [];

                      // get data for list laporan layanan
                      let rows=[];
                      infoptssPosts.forEach((infoptssPost,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													judul:infoptssPost.judul,
													created_at:dttodate(infoptssPost.created_at),
				action:
                          <>
                            <a href ={`${match.url}/ubah/${infoptssPost.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(infoptssPost.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Judul',
                            field: 'judul',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title mr-1">Data Informasi Publik Setiap Saat Post </div>
                            <a href="/admin/infoptss/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {infoptssPost} = state.infoptssPost;
                const {menu} = state.menu;
                return {isAdmin,menu,infoptssPost};
            }

            const connectedInfoptssPostListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(InfoptssPostListPage));

            export {connectedInfoptssPostListPage as InfoptssPostListPage};

