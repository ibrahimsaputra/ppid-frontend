import React,{Component} from 'react';
import {admin,permohonanActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
// import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';
import {dttodate} from '../helpers';

class PermohonanListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(permohonanActions.getPermohonan());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(permohonanActions.deletePermohonanById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let permohonans=this.props.permohonan || [];

                      // get data for list laporan layanan
                      let rows=[];
                      permohonans.forEach((permohonan,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													nama_pemohon:permohonan.nama_pemohon,
													email:permohonan.email,
													info_kategori:permohonan.info_kategori,
                          status:permohonan.status,
                          tanggal:dttodate(permohonan.created_at),
				                  action:
                          <>
                            <a href ={`${match.url}/ubah/${permohonan.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(permohonan.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Nama Pemohon',
                            field: 'nama_pemohon',
                            sort: 'asc'
                          },
													{
                            label: 'Email',
                            field: 'email',
                            sort: 'asc'
                          },
													{
                            label: 'Info Kategori',
                            field: 'info_kategori',
                            sort: 'asc'
                          },
													{
                            label: 'Status',
                            field: 'status',
                            sort: 'asc'
                          },
                          {
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Permohonan </div>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {permohonan} = state.permohonan;
                const {menu} = state.menu;
                return {isAdmin,menu,permohonan};
            }

            const connectedPermohonanListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(PermohonanListPage));

            export {connectedPermohonanListPage as PermohonanListPage};

