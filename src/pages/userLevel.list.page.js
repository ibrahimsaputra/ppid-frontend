import React,{Component} from 'react';
import {admin,userLevelActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';

class UserLevelListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(userLevelActions.getUserLevel());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(userLevelActions.deleteUserLevelById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let userLevels=this.props.userLevel;

                      // get data for list laporan layanan
                      let rows=[];
                      userLevels.forEach((userLevel,i) => {
                        // console.log(i);
                        rows.push({
                          no:i,
													level_name:userLevel.level_name,
													created_at:userLevel.created_at,
													updated_at:userLevel.updated_at,
				action:
                          <>
                            <a href ={`${match.url}/${userLevel.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(userLevel.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Level Name',
                            field: 'level_name',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
													{
                            label: 'Updated At',
                            field: 'updated_at',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data UserLevel </div>
                            <a href="/admin/userlevel/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {userLevel} = state.userLevel;
                const {menu} = state.menu;
                return {isAdmin,menu,userLevel};
            }

            const connectedUserLevelListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(UserLevelListPage));

            export {connectedUserLevelListPage as UserLevelListPage};

