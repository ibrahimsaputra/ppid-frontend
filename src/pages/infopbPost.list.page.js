import React,{Component} from 'react';
import {admin,infopbPostActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';
import {dttodate} from '../helpers';

class InfopbPostListPage extends Component{
              constructor(props){
                super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(infopbPostActions.getInfopbPost());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(infopbPostActions.deleteInfopbPostById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let infopbPosts = this.props.infopbPost || [];
                      console.log(infopbPosts)
                      // get data for list laporan layanan
                      let rows=[];
                      infopbPosts.forEach((infopbPost,i) => {
                        // console.log(i);
                        rows.push({
                          no:i+1,
													id_menu:infopbPost.id_menu,
													judul:infopbPost.judul,
													body:infopbPost.body,
													created_at:dttodate(infopbPost.created_at),
				action:
                          <>
                            <a href ={`${match.url}/ubah/${infopbPost.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(infopbPost.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Judul',
                            field: 'judul',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Informasi Publik Berkala Post </div>
                            <a href="/admin/infopb/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {infopbPost} = state.infopbPost;
                const {menu} = state.menu;
                return {isAdmin,menu,infopbPost};
            }

            const connectedInfopbPostListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(InfopbPostListPage));

            export {connectedInfopbPostListPage as InfopbPostListPage};

