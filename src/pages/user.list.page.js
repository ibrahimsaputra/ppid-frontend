import React,{Component} from 'react';
import {admin,userActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';

class UserListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(userActions.getUser());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(userActions.deleteUserById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let users=this.props.user || [];

                      // get data for list laporan layanan
                      let rows=[];
                      users.forEach((user,i) => {
                        // console.log(i);
                        rows.push({
                          no:i,
													id_level:user.id_level,
													username:user.username,
													password:user.password,
				action:
                          <>
                            <a href ={`${match.url}/${user.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={()=>this.delete(user.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Id Level',
                            field: 'id_level',
                            sort: 'asc'
                          },
													{
                            label: 'Username',
                            field: 'username',
                            sort: 'asc'
                          },
													{
                            label: 'Password',
                            field: 'password',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
													{
                            label: 'Updated At',
                            field: 'updated_at',
                            sort: 'asc'
                          },
													{
                            label: 'Enabled',
                            field: 'enabled',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data User </div>
                            <a href="/admin/user/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {user} = state.user;
                const {menu} = state.menu;
                return {isAdmin,menu,user};
            }

            const connectedUserListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(UserListPage));

            export {connectedUserListPage as UserListPage};

