import React,{Component} from 'react';
import {userLevelActions,admin} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { AdminNav } from '../component';

class UserLevelAddPage extends Component{
            constructor(props){
                super(props);
                this.state ={
                    loading:false,
                    resMessage:{}
                };
                this.handleFormSubmit = this.handleFormSubmit.bind(this);
                this.handleChange = this.handleChange.bind(this);
            }

            componentDidMount(){
                const {match:{params},dispatch,isAdmin} = this.props;

                if(!isAdmin)dispatch(admin());
                if(params.id){
                    dispatch(userLevelActions.getUserLevelById(params.id))
                }
            }

            handleChange = prop => event => {
                const {dispatch} = this.props;
                // console.log(prop,event.target.value);
                if(prop === "upload_ktp" || prop === "thumbnail" || prop === "link_pdf" || prop==="photo"){
                    dispatch(userLevelActions.onChangeUserLevelProps('selectedFile',event));
                }else{
                    dispatch(userLevelActions.onChangeUserLevelProps(prop, event));
                } //change
            };

            handleValidate (){
                let isValid = true;
                let errors = {};
                /*
                Beda setiap page
                Untuk field yang tidak wajib isi di deconstruct dari object;
                */
                const {userLevel,id,created_at,updated_at,...fields} = this.props.userLevel;

                for (const key in fields) {
                    if (fields.hasOwnProperty(key)) {
                        if(!fields[key]){
                            isValid=false;
                            errors[key]="Wajib Isi"
                        }

                        /**
                        * Untuk Field dengan Spesifikasi Khusus
                        */
                        if(typeof fields["nama_pemohon"] !== "undefined"){
                            if(!fields["nama_pemohon"].match(/^[a-zA-Z]+$/)){
                            isValid = false;
                            errors["nama_pemohon"] = "Hanya Huruf";
                            }
                        }

                        if(typeof fields["email"] !== "undefined"){
                            let lastAtPos = fields["email"].lastIndexOf('@');
                            let lastDotPos = fields["email"].lastIndexOf('.');

                            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                                isValid = false;
                                errors["email"] = "Email Tidak valid";
                                }
                        }

                    }
                }
                this.setState({
                    resMessage: errors
                })
                return isValid;
            }

            handleFormSubmit = event =>{
                event.preventDefault();
                event.stopPropagation();
                this.setState({
                    loading:true
                });

                const {match:{params}}=this.props;
                const {dispatch} =this.props;
                const {userLevel} =this.props;
                if (this.handleValidate()){

                    const payload={
				id:userLevel.id,
				level_name:userLevel.level_name,
				created_at:userLevel.created_at,
				updated_at:userLevel.updated_at,
			}
                        if(params.id){
                            dispatch(userLevelActions.editUserLevelInfo(params.id,payload))
                        }else{
                            dispatch(userLevelActions.createUserLevel(payload))
                        }
                        alert("Form Submitted")
                    }else{
                        this.setState({
                            loading:false
                        })
                        alert("Form has error")
                    }
                }

                render(){
                    const {userLevel} = this.props;
                    return(
                        <div className="bodier content">
                            <div className="post-container content-container">
                                    <div className="post-head">
                                    <div className="post-title">UserLevel</div>
                                    </div>
                                    <div className="post-body">
                                        <form action="" >
                                            <fieldset>
                                                <legend></legend>
                         					<div className="form-group">
                                                    <label htmlFor="id">Id</label>
                                                    <input id='id' name="id" type="text" className={`form-control ${(this.state.resMessage['id'])?'is-invalid':''}`} value={userLevel['id']} onChange={this.handleChange('id')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['id'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="level_name">Level Name</label>
                                                    <input id='level_name' name="level_name" type="text" className={`form-control ${(this.state.resMessage['level_name'])?'is-invalid':''}`} value={userLevel['level_name']} onChange={this.handleChange('level_name')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['level_name'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="created_at">Created At</label>
                                                    <input id='created_at' name="created_at" type="text" className={`form-control ${(this.state.resMessage['created_at'])?'is-invalid':''}`} value={userLevel['created_at']} onChange={this.handleChange('created_at')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['created_at'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                         					<div className="form-group">
                                                    <label htmlFor="updated_at">Updated At</label>
                                                    <input id='updated_at' name="updated_at" type="text" className={`form-control ${(this.state.resMessage['updated_at'])?'is-invalid':''}`} value={userLevel['updated_at']} onChange={this.handleChange('updated_at')} required/>
                                                    <div className={`feedback ${(this.state.resMessage['updated_at'])?'invalid-feedback':''}`}>
                                                        *Wajib isi
                                                    </div>
                                                </div>
                          					<button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Submit</button>
                                            </fieldset>
                                        </form>
                                    </div>
                            </div>
                            <AdminNav/>
                        </div>
                    );
                }

            }

            UserLevelAddPage.propTypes = {

            };
            const mapStateToProps = (state) =>{
                const { isAdmin } = state.authentication;
                const userLevel = state.userLevel;

                return {
                isAdmin,userLevel
                };
            }
            const connectedUserLevelAddPage = withRouter(connect(mapStateToProps, null, null, {
                pure: false
            })(UserLevelAddPage));

            export { connectedUserLevelAddPage as UserLevelAddPage };

