import React,{Component} from 'react';
import {admin,modulActions} from '../_actions';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faTrash} from '@fortawesome/free-solid-svg-icons/faTrash';
import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {MDBDataTable} from 'mdbreact';
import { AdminNav } from '../component';

class ModulListPage extends Component{
  constructor(props){
    super(props)
                const {dispatch} = this.props;
                if (!this.props.isAdmin) dispatch(admin());
                dispatch(modulActions.getModul());
              }

              delete(id){
                const {dispatch} = this.props;
                dispatch(modulActions.deleteModulById(id));
              }

              render(){
                   const {match} = this.props;
                  //  const link = match.url.split("/");
                   let datatable =[];
                   let moduls=this.props.modul;

                      // get data for list laporan layanan
                      let rows=[];
                      moduls.forEach((modul,i) => {
                        // console.log(i);
                        rows.push({
                          no:i,
													kode:modul.kode,
													nama:modul.nama,
													created_at:modul.created_at,
													enabled:modul.enabled,
				action:
                          <>
                            <a href ={`${match.url}/${modul.id}`} className="btn btn-sm btn-info mr-2"><FontAwesomeIcon icon={faInfo} className="mr-1"/>Detail</a>
                            <button className="btn btn-sm btn-flat btn-danger" onClick={this.delete(modul.id)}><FontAwesomeIcon icon={faTrash} className="mr-1" />Delete</button>
                          </>
                        })
                      });

                      datatable = {
                        columns: [
                          {
                            label: 'No',
                            field: 'no',
                            sort: 'asc',
                            width: 10,
                          },
													{
                            label: 'Kode',
                            field: 'kode',
                            sort: 'asc'
                          },
													{
                            label: 'Nama',
                            field: 'nama',
                            sort: 'asc'
                          },
													{
                            label: 'Created At',
                            field: 'created_at',
                            sort: 'asc'
                          },
													{
                            label: 'Enabled',
                            field: 'enabled',
                            sort: 'asc'
                          },
{
                            label: 'Action',
                            field: 'action',
                            sort: 'asc'
                          }
                        ],
                        rows: rows
                      }

                   return (
                      <div className="content">
                      {/* <Breadcrumb links={link}/> */}
                      <div className="content-container">
                          <div className="post-head">
                            <div className="post-title">Data Modul </div>
                            <a href="/admin/modul/tambah" className="btn btn-flat btn-sm btn-success"> <FontAwesomeIcon icon={faPlus}/> Tambah</a>
                          </div>
                          <MDBDataTable btn
                            striped
                            bordered
                            small
                            data={datatable}
                            sorting={"true"}
                          />
                        </div>
                        <AdminNav/>
                      </div>
                    );
              }
            }

            function mapStateToProps(state){
                const {isAdmin} = state.authentication;
                const {modul} = state.modul;
                const {menu} = state.menu;
                return {isAdmin,menu,modul};
            }

            const connectedModulListPage= withRouter(connect(mapStateToProps,
                null, null, {pure:false})(ModulListPage));

            export {connectedModulListPage as ModulListPage};

